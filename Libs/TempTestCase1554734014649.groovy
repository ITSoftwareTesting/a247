import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\GILLIA~1\\AppData\\Local\\Temp\\Katalon\\Test Cases\\QC Test Configuration\\27.4 QC Test Configuration- Edit\\20190408_153334\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
not_run: WebUI.maximizeWindow()

not_run: WebUI.delay(2)

'Enter username'
not_run: WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), 'daveau8@sharklasers.com')

'Enter password '
not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_1'), 'YC0926CP/9Y=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_2'), 'O7ZfLiLQj1U=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_3'), 'AiUR7bYidlY=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_4'), '2j+12BlJgqs=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_5'), 'OrFTmnKUEIE=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_6'), 'TJFShuHF+Ko=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_7'), 'jJYj7iVMMds=')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_8'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

not_run: WebUI.setEncryptedText(findTestObject('Gillian Objects/Log in/input_Use your account details_9'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

'Click "Log in"'
not_run: WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

not_run: WebUI.delay(5)

'Click on the configuration tile '
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

not_run: WebUI.delay(2)

'Click on the QC test tab'
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/a_QC Test'))

not_run: WebUI.delay(5)

'Click on the Add New button'
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/div_Add New'))

not_run: WebUI.delay(3)

'Click on the instrument drop down list '
not_run: WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _2'))

'Select Instrument 1'
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_Instrument 1'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(2)

'Select on the assay drop down list '
not_run: WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configuration/span_Select an item..._k-icon k-i-arrow-60-down'))

not_run: WebUI.delay(2)

'Select CA 125 | Immunoassay | Chemiluminescence | U/ml | Roche'
not_run: WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_CA 125  Immunoassay  Chemil'))

not_run: WebUI.delay(2)

'Select the Lot 1 drop down list '
not_run: WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _1'))

not_run: WebUI.delay(2)

'Select lot 1304EC '
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_1305EC'))

not_run: WebUI.delay(2)

'Click on the save button '
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Save'))

'Check that the success message appears '
not_run: WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), 
    0)

not_run: WebUI.delay(10)

'Double click on the newly created QC Test '
not_run: WebUI.doubleClick(findTestObject('Gillian Objects/QC Test Config/td_CA 125'))

not_run: WebUI.delay(5)

'Click on the Assay drop down button'
not_run: WebUI.click(findTestObject('Gillian Objects/Send keys/Page_Acusera 24.7 - QC Test Configuration/span_CA 125  Immunoassay  Chemiluminescence  Uml  Roche_k-icon k-i-arrow-60-down'))

not_run: WebUI.delay(2)

'Select "Calcium | Clinical Chemistry | Arsenazo | g/l | Roche"'
not_run: WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_Calcium  Clinical Chemistry  Arsenazo  gl  Roche'))

not_run: WebUI.delay(5)

'Click on the Lot 1 dropdown arrow'
not_run: WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _1'))

not_run: WebUI.delay(5)

'Verify that Lot 00125-PL is clickable '
not_run: WebUI.verifyElementClickable(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_00125-PL'))

not_run: WebUI.delay(3)

'Click Lot 00125-PL'
not_run: WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_00125-PL'))

WebUI.delay(2)

'Click on Lot 2 dropdown arrow '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Lot2Dropdown'))

//Call the driverfactory library'
WebDriver driver = DriverFactory.getWebDriver()

//'Save the list within the Analyte dropdown into the analyteList array, finding the list items by their xpath'
List analyteList = driver.findElements(By.xpath('//ul[@id=\\'lot2_listbox\\']/li[6]'))

//'Create a foreach loop to check each item in the list'
for (def item : analyteList) {
    //'create an if statement to check if any of the items in the list equals, in this case "Albumin". You can change "Albumin" to any item on the list that you want.'
    if (item.getText().toString() == '1009UNCM') {
        //'Modify the text property of the Test Object so that it is pointing towards the item you selected from the list'
        analyteObject = WebUI.modifyObjectProperty(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_1009UNCM'), 
            'text', 'equals', item.getText().toString(), true)

        //'Click on modified object'
        WebUI.click(analyteObject)

        //'break out of the loop'
        break //if
    } //foreach
}

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_1009UNCM'), 0)

'Verify that Lot 00125-PL is clickable '
WebUI.verifyElementClickable(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_1009UNCM'))

WebUI.delay(5)

'Select Lot 1009UNCM'
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_1009UNCM'))

WebUI.delay(2)

'Click on the Lot 3 drop down arrow '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Lot3Dropdown'))

WebUI.delay(2)

'Select Lot 1008UN'
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_1010UN'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Save'))

WebUI.delay(10)

'Check that the success message appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), 
    0)

WebUI.delay(5)

WebUI.closeBrowser()

''', 'Test Cases/QC Test Configuration/27.4 QC Test Configuration- Edit', new TestCaseBinding('Test Cases/QC Test Configuration/27.4 QC Test Configuration- Edit',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
