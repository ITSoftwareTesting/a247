
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject


def static "readAndWriteToExcel.ReadFromExcel.readFromExcel"(
    	int iSheet	
     , 	int iRow	
     , 	int iCell	
     , 	String fileLocation	) {
    (new readAndWriteToExcel.ReadFromExcel()).readFromExcel(
        	iSheet
         , 	iRow
         , 	iCell
         , 	fileLocation)
}

def static "autoGenerators.AutoStringGenerator.randomString"(
    	int length	) {
    (new autoGenerators.AutoStringGenerator()).randomString(
        	length)
}

def static "datePickerPackage.DatepickerHandler.handleDatepicker"(
    	TestObject calender	
     , 	String exp_Date	
     , 	String exp_Month	
     , 	String exp_Year	) {
    (new datePickerPackage.DatepickerHandler()).handleDatepicker(
        	calender
         , 	exp_Date
         , 	exp_Month
         , 	exp_Year)
}

def static "readAndWriteToExcel.WriteToExcel.writeToExcel"(
    	int iSheet	
     , 	int iRow	
     , 	int iCell	
     , 	String iText	
     , 	String fileLocation	) {
    (new readAndWriteToExcel.WriteToExcel()).writeToExcel(
        	iSheet
         , 	iRow
         , 	iCell
         , 	iText
         , 	fileLocation)
}
