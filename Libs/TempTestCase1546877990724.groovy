import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\DAVIDM~1.RAN\\AppData\\Local\\Temp\\Katalon\\Test Cases\\Assay Configuration - CRUD\\20190107_161950\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.openBrowser('')

not_run: WebUI.maximizeWindow()

not_run: WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), 'daveau8@sharklasers.com')

not_run: WebUI.setEncryptedText(findTestObject('Acusera247/Login/input_PasswordField'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

'Log in to the application as a participant'
not_run: WebUI.click(findTestObject('Acusera247/Login/button_Login'))

not_run: WebUI.delay(1)

'Click on the Acusera 24.7 application'
not_run: WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

not_run: WebUI.delay(1)

'Click on the "Configuration" tile'
not_run: WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Add New'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Analyte'))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Page_Acusera 24.7 - Assay Configura/input_AnalyteFilter'), 'cal')

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Analyte_Calcium'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_ChemistryType'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_ChemType_ClinChem'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Method'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Method_Arsenazo'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Unit'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Unit_gl'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Reagent Supplier'))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Page_Acusera 24.7 - Assay Configura/input_ReagentSupplier1'), 'ro')

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_ReagentSupplier_Roche'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Add New'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Analyte'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Analyte_CA 125'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_ChemistryType'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_ChemType_Immunoassay'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Method'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Method_CLIA'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Unit'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Unit_Uml'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Reagent Supplier'))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Page_Acusera 24.7 - Assay Configura/input_ReagentSupplier2'), 'roche')

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_ReagentSupplier_Roche - Copy'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

not_run: WebUI.delay(1)

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Acusera 24.7 - Assay Configura/td_CA 125'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Analyte'))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Page_Acusera 24.7 - Assay Configura/input_AnalyteFilter'), 'glu')

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Analyte_Glucose'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_ChemistryType'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/li_Clinical Chemistry'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Method'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Method_Colorimetric'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Unit'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_Unit_mmoll'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/dropdown_Reagent Supplier'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Acusera 24.7 - Assay Configura/list_ReagentSupplier_Roche'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_Acusera 24.7 - Assay Configura/span_Roche_glyphicon glyphicon'))

WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Yes'))

WebUI.click(findTestObject('Object Repository/Page_Acusera 24.7 - Assay Configura/span_Roche_glyphicon glyphicon'))

WebUI.click(findTestObject('Acusera247/Assay Configuration/button_No'))

WebUI.closeBrowser()

''', 'Test Cases/Assay Configuration - CRUD', new TestCaseBinding('Test Cases/Assay Configuration - CRUD',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
