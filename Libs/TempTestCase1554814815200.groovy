import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\RAIMON~1\\AppData\\Local\\Temp\\Katalon\\Test Cases\\Custom\\Custom Instrument\\1. Custom Instrument - Create\\20190409_140015\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

not_run: WebUI.setText(findTestObject('Acusera247/Custom Configuration/Page_Randox QC/input_Use your account details to log in_UserName'), 
    'daveau8@sharklasers.com')

not_run: WebUI.setEncryptedText(findTestObject('Acusera247/Custom Configuration/Page_Randox QC/input_Use your account details to log in_Password'), 
    'ZM/VR/a+KpRW0CGCfSq1yw==')

not_run: WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Randox QC/button_Log in'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Analyte Configuration/a_Instrument'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Add New'))

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/span_Select a manufacturer'))

WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_No data found_k-textbox'), 
    'Automated Custom Manufacturer 1')

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Add new item'))

WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_No data found_k-textbox'), 
    'Automated Custom Manufacturer 1')

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/span_Select a group'))

WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_Add new item_k-textbox'), 
    'Automated Custom Group 1')

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Add new item'))

WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_Add new item_k-textbox'), 
    'Automated Custom Group 1')

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/span_Select a model'))

WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_Add new item_k-textbox_26'), 
    'Automated Custom Model 1')

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Add new item'))

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Save'))

''', 'Test Cases/Custom/Custom Instrument/1. Custom Instrument - Create', new TestCaseBinding('Test Cases/Custom/Custom Instrument/1. Custom Instrument - Create',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
