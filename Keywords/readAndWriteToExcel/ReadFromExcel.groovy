package readAndWriteToExcel

import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.kms.katalon.core.annotation.Keyword

public class ReadFromExcel {

	@Keyword
	def readFromExcel(int iSheet, int iRow, int iCell, String fileLocation){

		'Opens connection to file for reading'
		FileInputStream file = new FileInputStream(new File(fileLocation))

		'Finds the workbook'
		XSSFWorkbook workbook = new XSSFWorkbook(file)

		'Finds the 1st sheet in the workbook'
		XSSFSheet sheet = workbook.getSheetAt(iSheet)

		'Read data from the cell in row 1 column 1'
		def cellValue = sheet.getRow(iRow).getCell(iCell)

		'Closes the file'
		file.close()

		'Formats the cellvalue to a string'
		DataFormatter dataFormatter = new DataFormatter();
		String value = dataFormatter.formatCellValue(cellValue);

		return value
	}//readFromExcel
}//ReadFromExcelClass
