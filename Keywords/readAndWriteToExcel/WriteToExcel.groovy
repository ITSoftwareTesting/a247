package readAndWriteToExcel

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.kms.katalon.core.annotation.Keyword

public class WriteToExcel {

	@Keyword
	def void writeToExcel(int iSheet, int iRow, int iCell, String iText, String fileLocation){

		'Opens connection to file for reading'
		FileInputStream file = new FileInputStream (new File(fileLocation))

		'Finds the workbook'
		XSSFWorkbook workbook = new XSSFWorkbook(file)

		'Finds the 1st sheet in the workbook'
		XSSFSheet sheet = workbook.getSheetAt(iSheet)

		'If the cell you are reading or writing from/too in excel is empty, include the following line, otherwise it can be left out'
		'it creates an empty cell in row 1 column 1, just modify the numbers to the row and cell you want minus 1'
		sheet.createRow(iRow).createCell(iCell)

		'Write data to excel'
		sheet.getRow(iRow).createCell(iCell).setCellValue(iText)

		'Closes the file'
		file.close()

		'Creates a file output stream to write to the file'
		FileOutputStream outFile = new FileOutputStream(new File(fileLocation))

		'Writes the specified byte to this file output stream.'
		workbook.write(outFile)

		'Closes the file'
		outFile.close()
	}//writeToExcel

}//WriteExcel
