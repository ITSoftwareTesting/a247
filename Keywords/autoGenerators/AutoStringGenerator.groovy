package autoGenerators;

import com.kms.katalon.core.annotation.Keyword
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.*;
import java.lang.String;


@Keyword

def randomString(int length) {

	String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	Random rand = new Random();

	StringBuilder sb = new StringBuilder();

	for (int i=0; i<length; i++) {

		sb.append(chars.charAt(rand.nextInt(chars.length())));
	}

	return sb.toString();
}