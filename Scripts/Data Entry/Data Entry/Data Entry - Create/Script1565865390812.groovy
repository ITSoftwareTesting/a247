import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import java.util.concurrent.ThreadLocalRandom as ThreadLocalRandom
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

'Extract users email from the excel file and save it to the email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Extract the users password from the excel file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Variable to set what you want your minimum result value to be'
double numLow = 1

'Variable to set what you want your Maximum result value to be'
double numHigh = 5

'Variable to set how many results you want'
int numOfResults = 21

'call the subclass DecimalFormat which allows us to set the results to how many decimal places we wnat, here it is set to 2dp'
DecimalFormat df = new DecimalFormat('0.00')

'Navigate to stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

'Enter the users email address into the username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Enter the users password into the password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the Data Entry tile'
WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Home/div_Data Entry'))

WebUI.delay(1)

'Click on the Calcium row'
WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/td_Calcium'))

WebUI.delay(1)

'Click on the Next button'
WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/button_Next'))

WebUI.delay(1)

'Click on the Mean box to activate the input field'
WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/input_Mean'))

'Enter a value into the mean field'
WebUI.sendKeys(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/input_MeanInput'), '4.3')

WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/span_SD_k-icon k-i-cancel'))

WebUI.delay(1)

WebUI.verifyTextPresent('Click Add New to configure a result', false)

WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/a_Add new result'))

'Opens connection to file for reading'
FileInputStream file = new FileInputStream(new File('G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\Data Entry Values.xlsx'))

'Finds the workbook'
XSSFWorkbook workbook = new XSSFWorkbook(file)

'Finds the 1st sheet in the workbook'
XSSFSheet sheet = workbook.getSheetAt(0)

for (int i = 0; i < numOfResults; i++) {
    random = ThreadLocalRandom.current().nextDouble(numLow, numHigh)

    println((('Result ' + (i + 1)) + ' = ') + df.format(random))

    WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/input_Mean'))

    WebUI.sendKeys(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/input_MeanInput'), df.format(random))

    WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/span_SD_k-icon k-i-check'))

    WebUI.delay(1)

    'If the cell you are reading or writing from/too in excel is empty, include the following line, otherwise it can be left out'

    'it creates an empty cell in row 1 column 1, just modify the numbers to the row and cell you want minus 1'
    sheet.createRow(i).createCell(0)

    'Write data to excel'
    sheet.getRow(i).createCell(0).setCellValue(df.format(random //for
            ))
}

'Closes the file'
file.close()

'Creates a file output stream to write to the file'
FileOutputStream outFile = new FileOutputStream(new File('G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\Data Entry Values.xlsx'))

'Writes the specified byte to this file output stream.'
workbook.write(outFile)

'Closes the file'
outFile.close()

WebUI.delay(1)

'Click on the "Save" button to save the results'
WebUI.click(findTestObject('Acusera247/Data Entry/Page_Acusera 247 - Data Entry/button_Save'))

WebUI.delay(1)

WebUI.verifyTextPresent(' Results have been created.', false)

WebUI.closeBrowser()

