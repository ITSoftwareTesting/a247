import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click Login button'
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click the Data Entry panel on the Home screen'
WebUI.click(findTestObject('Oran demo objects/Events/Home-Data Entry Panel'))

WebUI.delay(2)

'Click on the Events tab on the Data Entry page'
WebUI.click(findTestObject('Oran demo objects/Events/Data Entry-Events Tab'))

WebUI.delay(2)

'Click on Instrument 1, the first instrument in the grid'
WebUI.click(findTestObject('Oran demo objects/Events/Data Entry-Events-Instrument 1'))

WebUI.delay(2)

'Click on the Next button'
WebUI.click(findTestObject('Oran demo objects/Events/Events-Next'))

WebUI.delay(2)

'Click on the Delete icon that will delete the first record on the grid'
WebUI.click(findTestObject('Oran demo objects/Events/Events-Delete-001'))

WebUI.delay(2)

'Click on the Yes button on the popup box'
WebUI.click(findTestObject('Oran demo objects/Events/Delete-Confirm-Yes'))

WebUI.delay(2)

'End of test, close browser'
WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

