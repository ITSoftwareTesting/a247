import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click Login button'
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

'Click the Data Entry panel on the Home screen'
WebUI.click(findTestObject('Oran demo objects/Events/Home-Data Entry Panel'))

WebUI.delay(3)

'Click on the Events tab on the Data Entry page'
WebUI.click(findTestObject('Oran demo objects/Events/Data Entry-Events Tab'))

WebUI.delay(1)

'Click on Instrument 1, the first instrument in the grid'
WebUI.click(findTestObject('Oran demo objects/Events/Data Entry-Events-Instrument 1'))

WebUI.delay(1)

'Click on the Next button'
WebUI.click(findTestObject('Oran demo objects/Events/Events-Next'))

WebUI.delay(1)

'Click the Create Event button'
WebUI.click(findTestObject('Oran demo objects/Events/Create Event'))

WebUI.delay(1)

'Set the Title of the Event to "Event001"'
WebUI.setText(findTestObject('Oran demo objects/Events/Event Title textbox'), 'Event001')

WebUI.delay(1)

'Set the Description of the Event to "Event 001"'
WebUI.setText(findTestObject('Oran demo objects/Events/Event Description textbox'), 'Event 001')

WebUI.delay(1)

'Click on the Calendar icon in the "Datetime" field to set the date'
WebUI.click(findTestObject('Oran demo objects/Events/Event Calendar icon'))

WebUI.delay(1)

'Set the date to the 14th of August'
WebUI.click(findTestObject('Oran demo objects/Events/14-Aug-2019'))

WebUI.delay(1)

'Click on the Clock icon in the "Datetime" field to set the time'
WebUI.click(findTestObject('Oran demo objects/Events/Event Clock icon'))

WebUI.delay(1)

'Set the time to 0200'
WebUI.click(findTestObject('Oran demo objects/Events/2.00 AM'))

WebUI.delay(1)

'Click the "Save" button'
WebUI.click(findTestObject('Oran demo objects/Events/Events-Save'))

WebUI.delay(1)

'Click on the "Charts" tab on the left hand side of the screen'
WebUI.click(findTestObject('Oran demo objects/Events/Sidebar-Charts'))

WebUI.delay(2)

'Select the analyte "CA 125" by clicking on it'
WebUI.click(findTestObject('Oran demo objects/Events/Charts-Analyte 1'))

WebUI.delay(2)

'Click on the "Next" button'
WebUI.click(findTestObject('Oran demo objects/Page_Acusera 247 - Charts/Page_Acusera 247 - Charts/button_Next'))

WebUI.delay(1)

'Click on the "Generate" button'
WebUI.click(findTestObject('Oran demo objects/Events/LeveyJennings-Generate'))

WebUI.delay(3)

'Short term fix; check to see if the text present in the EVENT LABEL is present on the webpage'
WebUI.verifyTextPresent('Event001', true)

WebUI.delay(1)

WebUI.closeBrowser()

