import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Read the Email from the file and save it to the email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the Password from the file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

'Navigate to the STES url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter the email into the username field'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Input the password into the password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

WebUI.click(findTestObject('Gillian Objects/Change Password/div_Home_toggle_control_panel_visibility'))

WebUI.delay(1)

WebUI.click(findTestObject('Gillian Objects/Change Password/a_Change Password'))

WebUI.delay(2)

WebUI.click(findTestObject('Gillian Objects/Change Password/button_Change'))

WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Password/div_Error Please enter current password'), 0)

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Password/div_Error Please enter current password'), 'Error Please enter current password')

WebUI.delay(1)

WebUI.setEncryptedText(findTestObject('Gillian Objects/Change Password/input_Current password_OldPassword (1)'), 'xZjJueMkUOCAqPTsWkCP1A==')

WebUI.delay(1)

WebUI.click(findTestObject('Gillian Objects/Change Password/button_Change'))

WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Password/div_Error Please enter new password'), 0)

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Password/div_Error Please enter new password'), 'Error Please enter new password')

WebUI.delay(1)

WebUI.setEncryptedText(findTestObject('Gillian Objects/Change Password/input_New password_NewPassword (1)'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

WebUI.delay(1)

WebUI.click(findTestObject('Gillian Objects/Change Password/button_Change'))

WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Password/div_Error Please confirm new password'), 0)

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Password/div_Error Please confirm new password'), 'Error Please confirm new password')

WebUI.delay(1)

WebUI.setEncryptedText(findTestObject('Gillian Objects/Change Password/input_Confirm password_ConfirmPassword (1)'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

