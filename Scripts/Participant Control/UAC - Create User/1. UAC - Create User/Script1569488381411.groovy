import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')
String partialEmail = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 7, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

String userEmail = 'AUuser'+email, partialUserEmail = 'AUuser'+partialEmail

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the user icon on the top right hand of the site '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(3)

'Select User Access Control'
WebUI.click(findTestObject('Gillian Objects/UAC/div_User Access Control'))

WebUI.delay(1)

'Click on the user icon on the top right hand of the site '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(1)

'Click on the Add New button'
WebUI.click(findTestObject('Gillian Objects/UAC/div_Add New'))

WebUI.delay(1)

'Input an email address'
WebUI.setText(findTestObject('Gillian Objects/UAC/input_Email_email'), userEmail)

WebUI.delay(1)

'Input a firstname'
WebUI.setText(findTestObject('Gillian Objects/UAC/input_First Name_fname'), 'Automation')

WebUI.delay(1)

'Input a surname'
WebUI.setText(findTestObject('Gillian Objects/UAC/input_Last Name_lname'), 'User')

WebUI.delay(1)

'Click on the User drop down '
WebUI.click(findTestObject('Gillian Objects/UAC/span_UserRoleDropdown'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Select "Admin" from the list '
WebUI.click(findTestObject('Gillian Objects/UAC/li_Admin'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Gillian Objects/UAC/button_Save'))

WebUI.delay(1)

'Verify the success banner appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/UAC/div_Success User successfully saved'), 0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/div_Success User successfully saved'), 'Success User successfully saved')

WebUI.delay(2)

'Click on the banner so that it disappears'
WebUI.click(findTestObject('Gillian Objects/UAC/div_Success User successfully saved'), FailureHandling.STOP_ON_FAILURE)

'Verify the text within the gridview matches the user just created '
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/span_ParticipantTableRow2Column1'), userEmail)

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_Dave'), 'Automation')

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_Au 111'), 'User')

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_English (United Kingdom)'), 'English (United Kingdom)')

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_Admin'), 'Admin')

WebUI.delay(1)

'Click on the User icon on the top right hand side of the site '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(2)

'Click on the Log out button '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Log out (daveau25sharklaserscom)'))

WebUI.delay(5)

'Navigate to Guerrilla Mail'
WebUI.navigateToUrl('https://www.guerrillamail.com/')

'Click on the Email Input field'
WebUI.click(findTestObject('Gillian Objects/UAC/span_EmailGuerrilla'))

WebUI.delay(3)

'Click on the Email Input field'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/span_bwoaqlwi'))

WebUI.delay(1)

'Set Email in Guerrilla Mail'
WebUI.setText(findTestObject('Acusera247/Guerrilla Mail/input'), partialUserEmail)

'Click on the Set Email button'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/button_Set'))

'Have a long delay to allow the Set Password Email to come through'
WebUI.delay(60)

WebUI.refresh()

'Click on the top email in GM\r\n'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/td_qcoperationsrandox.com'))

WebUI.delay(1)

'Click on the Set Password link'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/a_Click here to set your password'))

WebUI.delay(1)

'Switch to Guerrilla Mail tab'
WebUI.switchToWindowIndex(1)

WebUI.delay(1)

'Get the Url so that we can edit it as GM apends its address to the start of the URL, we need to reomove this'
resetPwFull = WebUI.getUrl()

'This removes the Guerilla Mail address from the start of the url'
resetPwEdit = resetPwFull.replaceAll('https://www.guerrillamail.com/', '')

'Navigate to the Set Password url'
WebUI.navigateToUrl(resetPwEdit)

WebUI.delay(3)

'Enter an inadequate password into the New Password field'
WebUI.setText(findTestObject('Acusera247/Participant Management/Set Password/input_Set your password_newPassword'), password)

WebUI.delay(1)

'Enter an inadequate password into the Confirm Password field, the same that is in the New Password field'
WebUI.setText(findTestObject('Acusera247/Participant Management/Set Password/input_Set your password_confirmPassword'), 
    password)

WebUI.delay(1)

'Click on the "Reset" button'
WebUI.click(findTestObject('Acusera247/Participant Management/Set Password/button_Reset'))

WebUI.delay(1)

'Verify the Success Text'
WebUI.verifyTextPresent('Success Password updated', false)

WebUI.delay(2)

'Verify user is on the login page'
WebUI.verifyElementPresent(findTestObject('Acusera247/Participant Management/Set Password/h2_Log in'), 5)

WebUI.delay(5)

'Input the log in username'
WebUI.setText(findTestObject('Acusera247/Page_Randox QC/input_Use your account details to log in_UserName'), userEmail)

WebUI.delay(1)

'Input the log in password'
WebUI.setMaskedText(findTestObject('Acusera247/Page_Randox QC/input_Use your account details to log in_Password'), password)

WebUI.delay(1)

'Click on the log in button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

'Ensure the EULA title appears '
WebUI.verifyElementPresent(findTestObject('Acusera247/Home Page/h1_Eula Title'), 5)

'Click on the Accept button '
WebUI.click(findTestObject('Acusera247/Home Page/btn_Eula Accept Button'))

WebUI.delay(2)

'Ensure the home page is displayed'
WebUI.verifyElementPresent(findTestObject('Acusera247/Home Page/div_Home Title'), 2)

WebUI.delay(10)

WebUI.closeBrowser()

