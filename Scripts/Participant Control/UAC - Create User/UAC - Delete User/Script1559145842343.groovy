import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the user icon on the top right hand of the site '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(3)

'Select User Access Control'
WebUI.click(findTestObject('Gillian Objects/UAC/div_User Access Control'))

WebUI.delay(1)

'Click on the Bin icon beside the user Daveau111'
WebUI.click(findTestObject('Gillian Objects/UAC/span_Daveau111_Delete'))

WebUI.delay(1)

'When the pop up box asks whether you wish to proceed click on the No button '
WebUI.click(findTestObject('Gillian Objects/UAC/button_No'))

WebUI.delay(1)

'Click on the Bin icon beside the user Daveau111'
WebUI.click(findTestObject('Gillian Objects/UAC/span_Daveau111_Delete'))

WebUI.delay(1)

'When the pop up box asking whether you wish to delete pops up click Yes'
WebUI.click(findTestObject('Gillian Objects/UAC/button_Yes'))

WebUI.delay(1)

'Verify the success banner is displayed'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/UAC/div_Success User successfully deleted'), 0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/div_Success User successfully deleted'), 'Success! User successfully deleted')

WebUI.delay(1)

'Click on the success banner so that it disappears '
WebUI.click(findTestObject('Gillian Objects/UAC/div_Success User successfully deleted'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the User icon on the top right hand side of the site '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(2)

'Click on the Log out button '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Log out (daveau25sharklaserscom)'))

WebUI.delay(2)

'Verify user is on the login page'
WebUI.verifyElementPresent(findTestObject('Acusera247/Participant Management/Set Password/h2_Log in'), 5)

WebUI.delay(2)

'Input the log in username'
WebUI.setText(findTestObject('Acusera247/Page_Randox QC/input_Use your account details to log in_UserName'), 'daveau111@sharklasers.com')

WebUI.delay(1)

'Input the log in password '
WebUI.setEncryptedText(findTestObject('Acusera247/Page_Randox QC/input_Use your account details to log in_Password'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

WebUI.delay(1)

'Click on the log in button '
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Verify the message has appeared to say the account is deactivated '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/UAC/li_Account has been deactivated'), 0)

'Verify the text within the message to say that the account has been deactivated '
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/li_Account has been deactivated'), 'Invalid login attempt.')

WebUI.delay(10)

WebUI.closeBrowser()

