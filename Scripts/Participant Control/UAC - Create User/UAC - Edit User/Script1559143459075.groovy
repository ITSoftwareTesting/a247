import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

String userEditedFirstName = 'User First Name Edit'

String userEditedLastName = 'User Last Name Edit'

String userEmail = 'AUuser' + email

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button'
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the user icon on the top right hand side of the site'
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(3)

'Click on User Access Control '
WebUI.click(findTestObject('Gillian Objects/UAC/div_User Access Control'))

WebUI.delay(5)

'Click on the user icon on the top right hand side of the site'
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(3)

'Double click on the Admin user created in the UAC - Create User test case'
WebUI.doubleClick(findTestObject('Gillian Objects/UAC/span_ParticipantTableRow2Column1'))

WebUI.delay(2)

'Edit the first name'
WebUI.setText(findTestObject('Gillian Objects/UAC/input_First Name_fname'), userEditedFirstName)

WebUI.delay(1)

'Edit the surname'
WebUI.setText(findTestObject('Gillian Objects/UAC/input_Last Name_lname'), userEditedLastName)

WebUI.delay(1)

'Click on the User Role drop down arrow '
WebUI.click(findTestObject('Gillian Objects/UAC/span_UserRoleDropdown'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Select "User" from the drop down list '
WebUI.click(findTestObject('Gillian Objects/UAC/li_User'))

WebUI.delay(1)

'Click on the Deactivate toggle to ensure it is now active'
WebUI.click(findTestObject('Gillian Objects/UAC/span_Deactivated_k-switch-handle km-switch-handle'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Gillian Objects/UAC/button_Save'))

'Verify the success banner appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/UAC/div_Success User successfully deactivated'), 0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/div_Success User successfully deactivated'), 'Success User successfully deactivated')

WebUI.delay(2)

'Click on the banner so that it disappears'
WebUI.click(findTestObject('Gillian Objects/UAC/div_Success User successfully deactivated'), FailureHandling.STOP_ON_FAILURE)

'Verify the text within the gridview matches the user just created '
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/span_ParticipantTableRow2Column1'), userEmail)

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_Dave'), userEditedFirstName)

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_Au 111'), userEditedLastName)

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_English (United Kingdom)'), 'English (United Kingdom)')

WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/td_Admin'), 'User')

'Get the position that the toggle for reject is set at '
value = WebUI.getCSSValue(findTestObject('Gillian Objects/UAC/span_deactivateToggleGV'), 'transform')

println(value)

'Checking that the value is correct and the toggle is switched on '
WebUI.verifyEqual(value, 'matrix(1, 0, 0, 1, 30.4, 0)')

WebUI.delay(1)

'Click on the User icon on the top right hand side of the site '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Home_UserIcon'))

WebUI.delay(2)

'Click on the Log out button '
WebUI.click(findTestObject('Gillian Objects/UAC/span_Log out (daveau25sharklaserscom)'))

WebUI.delay(2)

'Verify user is on the login page'
WebUI.verifyElementPresent(findTestObject('Acusera247/Participant Management/Set Password/h2_Log in'), 5)

WebUI.delay(2)

'Input the log in username'
WebUI.setText(findTestObject('Acusera247/Page_Randox QC/input_Use your account details to log in_UserName'), userEmail)

WebUI.delay(1)

'Input the log in password '
WebUI.setEncryptedText(findTestObject('Acusera247/Page_Randox QC/input_Use your account details to log in_Password'), password)

WebUI.delay(1)

'Click on the log in button '
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Verify the message has appeared to say the account is deactivated '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/UAC/li_Account has been deactivated'), 0)

'Verify the text within the message to say that the account has been deactivated '
WebUI.verifyElementText(findTestObject('Gillian Objects/UAC/li_Account has been deactivated'), 'Account has been deactivated.')

WebUI.delay(10)

WebUI.closeBrowser()

