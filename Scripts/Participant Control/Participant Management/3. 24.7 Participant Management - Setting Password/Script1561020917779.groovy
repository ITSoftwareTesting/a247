import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Read the users "Email" from the excel file and save it to the "email" variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users "password" from the excel file and save it to the "password" variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users "password" from the excel file and save it to the "password" variable'
String partialEmail = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 7, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.maximizeWindow()

'Navigate to Guerrilla Mail'
WebUI.navigateToUrl('https://www.guerrillamail.com/')

WebUI.delay(2)

'Click on the Email Input field'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/span_bwoaqlwi'))

WebUI.delay(1)

'Set Email in Guerrilla Mail'
WebUI.setText(findTestObject('Acusera247/Guerrilla Mail/input'), partialEmail)

'Click on the Set Email button'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/button_Set'))

'Have a long delay to allow the Set Password Email to come through'
WebUI.delay(60)

WebUI.refresh()

'Click on the top email in GM\r\n'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/td_qcoperationsrandox.com'))

WebUI.delay(1)

'Click on the Set Password link'
WebUI.click(findTestObject('Acusera247/Guerrilla Mail/a_Click here to set your password'))

WebUI.delay(1)

'Switch to Guerrilla Mail tab'
WebUI.switchToWindowIndex(1)

WebUI.delay(1)

'Get the Url so that we can edit it as GM apends its address to the start of the URL, we need to reomove this'
resetPwFull = WebUI.getUrl()

'This removes the Guerilla Mail address from the start of the url'
resetPwEdit = resetPwFull.replaceAll('https://www.guerrillamail.com/', '')

'Navigate to the Set Password url'
WebUI.navigateToUrl(resetPwEdit)

WebUI.delay(1)

'Click on the "Reset" button'
WebUI.click(findTestObject('Acusera247/Participant Management/Set Password/button_Reset'))

'Verify the alert message'
WebUI.verifyTextPresent('Error Please enter new password', false)

'Enter an inadequate password into the New Password field'
WebUI.setText(findTestObject('Acusera247/Participant Management/Set Password/input_Set your password_newPassword'), 'qwerty')

'Click on "Reset" button'
WebUI.click(findTestObject('Acusera247/Participant Management/Set Password/button_Reset'))

'Verify the alert message'
WebUI.verifyTextPresent('Error Please confirm new password', false)

'Enter an inadequate password into the Confirm Password field, the same that is in the New Password field'
WebUI.setText(findTestObject('Acusera247/Participant Management/Set Password/input_Set your password_confirmPassword'), 
    'qwerty')

'Click in the "Reset" button'
WebUI.click(findTestObject('Acusera247/Participant Management/Set Password/button_Reset'))

WebUI.delay(1)

'Verify the Alert message'
WebUI.verifyTextPresent('Error Password is not accepted. Please ensure your password is at least six characters long with one number, one lower and uppercase letter and one symbol', 
    false)

'Enter the adequate password into the New PAssword field'
WebUI.setText(findTestObject('Acusera247/Participant Management/Set Password/input_Set your password_newPassword'), password)

'Click on the "Reset" button'
WebUI.click(findTestObject('Acusera247/Participant Management/Set Password/button_Reset'))

WebUI.delay(1)

'Verify the Alert message'
WebUI.verifyTextPresent('Error Please ensure new passwords match', false)

WebUI.delay(1)

'Set Confirm password field to the correct password'
WebUI.setText(findTestObject('Acusera247/Participant Management/Set Password/input_Set your password_confirmPassword'), 
    password)

'Click on the "Reset" button'
WebUI.click(findTestObject('Acusera247/Participant Management/Set Password/button_Reset'))

WebUI.delay(1)

'Verify the Success Text'
WebUI.verifyTextPresent('Success Password updated', false)

WebUI.delay(2)

'Verify user is on the login page'
WebUI.verifyElementPresent(findTestObject('Acusera247/Participant Management/Set Password/h2_Log in'), 5)

WebUI.closeBrowser()

