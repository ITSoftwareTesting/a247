import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'PREREQS: Manually Register a participant via the Registration Form, and put the Participants Name within the User Login Data.elsx file, under Manually Registered User'
WebUI.openBrowser('')

'Read the Participants name from the excel file and save it to the "participantName" variable'
String participantName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 6, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the  users password from the excel file and save it to the "password" variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the Operations email address from the excel file and save it to the "opsUserName" variable'
String opsUserName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 5, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.maximizeWindow()

'Navigate to Stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/')

WebUI.delay(1)

'Enter in Operations username'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), opsUserName)

'Enter in Operations PW'
WebUI.setText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 App button within the side panel'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(3)

'Click on the Global Admin tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_GlobalAdmin'))

WebUI.delay(4)

'Click on the Participant Filter dropdown'
WebUI.click(findTestObject('Acusera247/Participant Management/dropdown_ParticipantFilter'))

WebUI.delay(1)

'Select "Name" in the filter list'
WebUI.click(findTestObject('Acusera247/Participant Management/list_ParticipantFilterDropdown_Name'))

'Enter the Participants Name in the search field, \r\nthis is to single out the participant so that the can be selected in the next few steps'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_ParticipantFilterSearch'), participantName)

'Double click on the filtered out Participant'
WebUI.doubleClick(findTestObject('Acusera247/Participant Management/usersRowParticipantsGridView'))

/*
 *  THIS CAN BE ADDED IN TO VERIFY THAT THE CORRECT DETAILS HAVE BEEN SAVED
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_LaboratoryName'),
	'value', newLabName, 0)

WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_ContactName'),
	'value', newContactName, 0)

WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_ContactSurname'),
	'value', newContactSurname, 0)

WebUI.verifyElementText(findTestObject('Acusera247/Participant Management/dropdown_Membership'),
	'Platinum')

WebUI.verifyElementText(findTestObject('Acusera247/Participant Management/dropdown_Country'),
	'United Kingdom')

WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_ParticipantEmail'),
	'value', newParticipantEmail, 0)

WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_SalesEmail'),
	'value', newSalesEmail, 0)

WebUI.verifyElementChecked(findTestObject('Acusera247/Participant Management/checkbox_IQC'),
	0)

WebUI.verifyElementChecked(findTestObject('Acusera247/Participant Management/checkbox_Active'),
	0)

*/
WebUI.delay(1)

'Check the Active Checkbox'
WebUI.check(findTestObject('Acusera247/Participant Management/checkbox_Active'))

'Uncheck the EQA checkbox as we are only activating the participant for IQC'
WebUI.uncheck(findTestObject('Acusera247/Participant Management/checkbox_EQA'))

'Check the IQC checkbox'
WebUI.check(findTestObject('Acusera247/Participant Management/checkbox_IQC'))

WebUI.delay(2)

'Open Calendar Widget'
WebUI.click(findTestObject('Acusera247/Participant Management/calendarIconIQC'))

WebUI.delay(2)

'Select the Next month to the current month the calendar opens on'
WebUI.click(findTestObject('Acusera247/Participant Management/calendarMonthOver'))

WebUI.delay(1)

'Selects the date on the 4th row and 5th column of the calendar'
WebUI.click(findTestObject('Acusera247/Participant Management/dateOn4thRow5thColumn'))

WebUI.delay(1)

'Click on the Save button'
WebUI.click(findTestObject('Acusera247/Participant Management/button_Save'))

WebUI.delay(1)

WebUI.refresh()

WebUI.delay(4)

'Click on the Participant Filter dropdown'
WebUI.click(findTestObject('Acusera247/Participant Management/dropdown_ParticipantFilter'))

WebUI.delay(1)

'Select "Name" in the filter list'
WebUI.click(findTestObject('Acusera247/Participant Management/list_ParticipantFilterDropdown_Name'))

'Enter the Participants Name in the search field, \r\nthis is to single out the participant so that the can be selected in the next few steps'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_ParticipantFilterSearch'), participantName)

'Verify that the Active checkbox appears checked within the Participant grid view'
WebUI.verifyElementChecked(findTestObject('Acusera247/Participant Management/usersRowParticipantsGridViewActive'), 4)

WebUI.closeBrowser()

