import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Read the users "First Name" from the excel file and save it to the "firstName" variable'
String firstName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 1, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users "Last Name" from the excel file and save it to the "lastName" variable'
String lastName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 2, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

String fullName = (firstName + ' ') + lastName

'Read the users "Email" from the excel file and save it to the "email" variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users "password" from the excel file and save it to the "password" variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the Acusera Operations user name from the excel file and save it to the "opsUserName" variable'
String opsUserName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 5, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the Participants name from the excel file and save it to the "participantName" variable'
String participantName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 0, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.maximizeWindow()

'Navigate to "Stes"'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/')

WebUI.delay(1)

'Enter the Operations email to the username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), opsUserName)

'enter the Operations Password into the Password field'
WebUI.setText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

'Click on the Acusera 24.7 App button within the Side Panel'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(2)

'Select the Global Admin Tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_GlobalAdmin'))

WebUI.delay(1)

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Participant Management/button_AddNew'))

WebUI.delay(1)

'Click the Save button to show validation message'
WebUI.click(findTestObject('Acusera247/Participant Management/button_Save'))

'Verify the Validation message'
WebUI.verifyElementText(findTestObject('Acusera247/Participant Management/message_ValidationMessage_Membership'), 'Information Please complete all the required fields.')

WebUI.click(findTestObject('Acusera247/Participant Management/message_ValidationMessage_Membership'))

'Enter Laboratory name'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_LaboratoryName'), participantName)

'Enter a First Name'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_OwnerName'), firstName)

'Enter a Surname'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_OwnerSurname'), lastName)

'Click on the Membership Dropdown'
WebUI.click(findTestObject('Acusera247/Participant Management/dropdown_Membership'))

WebUI.delay(1)

'Select a Membership Level ("Platinum")'
WebUI.click(findTestObject('Acusera247/Participant Management/list_ParticipantMembership_Platinum'))

WebUI.delay(1)

'Click on the Country Dropdown'
WebUI.click(findTestObject('Acusera247/Participant Management/dropdown_Country'))

WebUI.delay(1)

'Filter out the countries ("United")'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_Country'), 'united')

WebUI.delay(1)

'Select a country from the filtered list ("United Kingdom")'
WebUI.click(findTestObject('Acusera247/Participant Management/list_CountryDropdown_ United Kingdom'))

'Enter in an Email Address'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_OwnerEmail'), email)

'Enter in a Sales Email Address'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_SalesEmail'), email)

'Click on the IQC checkbox'
WebUI.check(findTestObject('Acusera247/Participant Management/checkbox_IQC'))

WebUI.delay(1)

'Open Calendar Widget'
WebUI.click(findTestObject('Acusera247/Participant Management/calendarIconIQC'))

WebUI.delay(1)

'Select the Next month to the current month the calendar opens on'
WebUI.click(findTestObject('Acusera247/Participant Management/calendarMonthOver'))

WebUI.delay(1)

'Selects the date on the 4th row and 5th column of the calendar'
WebUI.click(findTestObject('Acusera247/Participant Management/dateOn4thRow5thColumn'))

WebUI.delay(1)

'Click the Save button so that the Active validation message appears'
WebUI.click(findTestObject('Acusera247/Participant Management/button_Save'))

WebUI.delay(2)

'Verify the validation message text'
WebUI.verifyTextPresent('Information Active must be selected', false)

WebUI.delay(1)

'Click on the Active checkbox'
WebUI.check(findTestObject('Acusera247/Participant Management/checkbox_Active'))

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Participant Management/button_Save'))

WebUI.delay(4)

'Click on the Participant Filter dropdown'
WebUI.click(findTestObject('Acusera247/Participant Management/dropdown_ParticipantFilter'))

WebUI.delay(1)

'Select "Name" in the filter list'
WebUI.click(findTestObject('Acusera247/Participant Management/list_ParticipantFilterDropdown_Name'))

WebUI.delay(2)

'Enter the Participants Name in the search field, \r\nthis is to single out the participant so that the can be selected in the next few steps'
WebUI.setText(findTestObject('Acusera247/Participant Management/inputField_ParticipantFilterSearch'), participantName)

'Verify that the Active checkbox appears checked within the Participant grid view'
WebUI.verifyElementChecked(findTestObject('Acusera247/Participant Management/usersRowParticipantsGridViewActive'), 4)

'Double click on the filtered out Participant'
WebUI.doubleClick(findTestObject('Acusera247/Participant Management/usersRowParticipantsGridView'))

WebUI.delay(1)

'Verify the Laboratory Name field'
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_LaboratoryName'), 'value', 
    participantName, 0)

'Verify the Membership field'
WebUI.verifyElementText(findTestObject('Acusera247/Participant Management/dropdown_Membership'), 'Platinum')

'Verify the Country field'
WebUI.verifyElementText(findTestObject('Acusera247/Participant Management/dropdown_Country'), 'United Kingdom')

'Verify the Sales Email field'
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Participant Management/inputField_SalesEmail'), 'value', email, 
    0)

'Verify the Owner Name field'
WebUI.verifyElementText(findTestObject('Acusera247/Participant Management/span_OwnerDropdown'), fullName)

'Verify IQC is checked'
WebUI.verifyElementChecked(findTestObject('Acusera247/Participant Management/checkbox_IQC'), 3)

'Verify Active is checked'
WebUI.verifyElementChecked(findTestObject('Acusera247/Participant Management/checkbox_Active'), 3)

WebUI.closeBrowser()

