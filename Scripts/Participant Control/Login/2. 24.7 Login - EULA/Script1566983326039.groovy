import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Read the email address from the file and save it to the email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the password from the file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

'Navigate to the STES url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/')

WebUI.delay(2)

'Input the email into the Username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

'Verify that the EULA appears'
WebUI.verifyElementPresent(findTestObject('Acusera247/Home Page/h1_Eula Title'), 5)

'Click on the "Decline" button on the EULA'
WebUI.click(findTestObject('Acusera247/Home Page/btn_Eula Decline'))

WebUI.delay(2)

'Input the email into the Username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

'Verify that the EULA is displayed'
WebUI.verifyElementPresent(findTestObject('Acusera247/Home Page/h1_Eula Title'), 5)

'Click on the "Accept" button in the EULA'
WebUI.click(findTestObject('Acusera247/Home Page/btn_Eula Accept Button'))

WebUI.delay(2)

'Verify the Home page is displayed'
WebUI.verifyElementPresent(findTestObject('Acusera247/Home Page/div_Home Title'), 2)

WebUI.closeBrowser()

