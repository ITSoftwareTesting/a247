import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'Read the users Email address from the file and save it to the email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users password from the file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Master Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

'Navigate tot the STES url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/')

WebUI.delay(2)

'Click on the Login button '
WebUI.click(findTestObject('Object Repository/Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Verify validation message is displayed informing the user they need to enter a User Name'
WebUI.verifyElementText(findTestObject('Acusera247/Login/li_List Item 1'), 'The UserName field is required.')

'Verify validation message is isplayed informing the user they need to enter a password'
WebUI.verifyElementText(findTestObject('Acusera247/Login/li_List Item 2'), 'The Password field is required.')

'Input an invalid email address into the Username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), 'InvalidEmail.com')

'Click on the Login button'
WebUI.click(findTestObject('Object Repository/Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Verify validation message is displayed informing the user they need to enter a valid email address'
WebUI.verifyElementText(findTestObject('Acusera247/Login/li_List Item 1'), 'The UserName field is not a valid e-mail address.')

'Verify validation message is displayed informing the user they need to enter a password'
WebUI.verifyElementText(findTestObject('Acusera247/Login/li_List Item 2'), 'The Password field is required.')

'Input the valid email address into the user name field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Click on the login button'
WebUI.click(findTestObject('Object Repository/Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Verify validation message is displayed informing the user they need to enter a password'
WebUI.verifyElementText(findTestObject('Acusera247/Login/li_List Item 1'), 'The Password field is required.')

'Input text into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), 'RandomPassword1!')

'Click on the login button'
WebUI.click(findTestObject('Object Repository/Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Verify validation message is displayed informing they user they have attempted to log in using invalid user credentials'
WebUI.verifyTextPresent('Invalid login attempt', false)

WebUI.closeBrowser()

