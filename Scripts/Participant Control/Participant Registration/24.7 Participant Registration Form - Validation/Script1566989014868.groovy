import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

'Maximize current window'
WebUI.maximizeWindow()

'Using the Keyword readFromExcel, find the participant name in the excel sheet and save it to a variable'
String participantName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 0, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Using the Keyword readFromExcel, find the first name in the excel sheet and save it to a variable'
String firstName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 1, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Using the Keyword readFromExcel, find the last  name in the excel sheet and save it to a variable'
String lastName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 2, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Navigate to the 24.7 Registration url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Register')

'Select Acusera 24.7 app'
WebUI.click(findTestObject('Acusera247/Registration/appSelectorAcusera247'))

WebUI.delay(1)

'Click on the Next Button\r\n'
WebUI.click(findTestObject('Acusera247/Registration/button_Next'))

WebUI.delay(1)

'Click on the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation appears within the Participant Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Country field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the First Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Last Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field\r\n'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over Participant Name Field to get Tooltip to display\r\n'
WebUI.mouseOver(findTestObject('Acusera247/Registration/participantNameTextField'))

WebUI.delay(1)

'Ensure Participant Name field validation tooltip displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/participantNameField'), 'Enter a valid participant name')

WebUI.delay(1)

'Enter Participant Name'
WebUI.setText(findTestObject('Acusera247/Registration/participantNameTextField'), 'Automation Lab')

'Click on the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Country field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the First Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Last Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over Country dropdown to ensure tooltip is display\r\n'
WebUI.mouseOver(findTestObject('Acusera247/Registration/countryDropdownSelector'))

WebUI.delay(1)

'Ensure Country field validation tooltip displays correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/countrySelectorField'), 'Select a valid country')

'Select a Country'
WebUI.selectOptionByValue(findTestObject('Acusera247/Registration/countryDropdownSelector'), 'United Kingdom', true)

WebUI.delay(1)

'Click on the Register button\r\n'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Country field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the First Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Last Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over the First Name field to ensure tooltip is displayed'
WebUI.mouseOver(findTestObject('Acusera247/Registration/firstNameTextField'))

WebUI.delay(1)

'Ensure First Name validation tooltip displays correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/firstNameFieldToolTip'), 'Enter a valid first name')

'Enter a First Name'
WebUI.setText(findTestObject('Acusera247/Registration/firstNameTextField'), 'Automation')

'Click on the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Country field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the First Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Last Name field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over the Last Name field to ensure tooltip is displayed'
WebUI.mouseOver(findTestObject('Acusera247/Registration/lastNameTextField'))

WebUI.delay(1)

'\r\n'
WebUI.verifyElementPresent(findTestObject('Acusera247/Registration/lastNameFieldToolTip'), 5)

'Ensure Last Name Validation tooltip displays correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/lastNameFieldToolTip'), 'Enter a valid last name')

'Enter a Last Name'
WebUI.setText(findTestObject('Acusera247/Registration/lastNameTextField'), 'User')

'Click on the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Country field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the First Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Last Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over the Email field to ensure tooltip is displayed'
WebUI.mouseOver(findTestObject('Acusera247/Registration/emailAddressTextField'))

WebUI.delay(1)

'Ensure Email Address validation tooltip displays correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/emailAddressFieldToolTip'), 'Enter a valid email address')

'Enter an invalid Email Address\r\n'
WebUI.setText(findTestObject('Acusera247/Registration/emailAddressTextField'), 'email.domain.com')

'Click on the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Country field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the First Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Last Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over the Email field to ensure tooltip is displayed'
WebUI.mouseOver(findTestObject('Acusera247/Registration/emailAddressTextField'))

WebUI.delay(1)

'Ensure Email Address validation tooltip displays correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/emailAddressFieldToolTip'), 'Enter a valid email address')

'Enter an Email Address\r\n'
WebUI.setText(findTestObject('Acusera247/Registration/emailAddressTextField'), 'emailthatdoesntexist@sharklasers.com')

'Click in the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Participant Country field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the First Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Last Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Email Address field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation appears within the Sales Email Address field'
WebUI.verifyElementVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

'Hover over the Email field to ensure tooltip is displayed'
WebUI.mouseOver(findTestObject('Acusera247/Registration/salesEmailAddress'))

'Ensure Sales Validation tooltip displays correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/salesEmailFieldToolTip'), 'Enter a valid sales email address')

'Enter a Sales Email Address'
WebUI.setText(findTestObject('Acusera247/Registration/salesEmailAddress'), 'emailthatdoesntexist@sharklasers.com')

'Click on the Marketing toggle switch'
WebUI.click(findTestObject('Acusera247/Registration/marketingSwitch'))

WebUI.delay(1)

'Check if the marketing button is in the ON position\r\n'
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Registration/marketingSwitch'), 'style', 'transform: translateX(31px) translateY(0px);', 
    5)

'Click on the Marketing toggle switch'
WebUI.click(findTestObject('Acusera247/Registration/marketingSwitch'))

WebUI.delay(1)

'Check if the marketing button is in the OFF position\r\n'
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Registration/marketingSwitch'), 'style', 'transform: translateX(0px) translateY(0px);', 
    5)

WebUI.delay(1)

'Click on the Register button'
WebUI.click(findTestObject('Acusera247/Registration/button_Register'))

'Ensure the general validation message is displayed\r\n'
WebUI.verifyElementText(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 'Information Please complete all required fields and try again')

WebUI.click(findTestObject('Acusera247/Registration/registrationFormValidationMessage'))

'Wait for the general validation message to disappear'
WebUI.waitForElementNotVisible(findTestObject('Acusera247/Registration/registrationFormValidationMessage'), 5)

'Ensure Validation no longer appears within the Participant Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Country field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconParticipantCountry'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the First Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconFirstName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Last Name field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconLastName'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Email Address field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconEmailAddress'), FailureHandling.CONTINUE_ON_FAILURE)

'Ensure Validation no longer appears within the Sales Email Address field'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Registration/validationIconSalesEmail'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.closeBrowser()

