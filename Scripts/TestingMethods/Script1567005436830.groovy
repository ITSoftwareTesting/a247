import java.time.LocalDate
import java.time.format.DateTimeFormatter

LocalDate date = LocalDate.now().minusDays(1);
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/LLLL/yyyy");
println ('Date is: ' +date.format(formatter))

'Read the users "First Name" from the excel file and save it to the "firstName" variable'
String firstName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 1, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users "Last Name" from the excel file and save it to the "lastName" variable'
String lastName = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 2, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String fullName = (firstName + ' ') + lastName

println(fullName)