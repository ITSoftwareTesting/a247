import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

WebUI.maximizeWindow()

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'Opens connection to file for reading'
FileInputStream file = new FileInputStream(new File('G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx'))

'Finds the workbook'
XSSFWorkbook workbook = new XSSFWorkbook(file)

'Finds the 1st sheet in the workbook'
XSSFSheet sheet = workbook.getSheetAt(0)

'Read data from the cell in row 1 column 1'
String email

String password

email = sheet.getRow(3).getCell(1).getStringCellValue()

password = sheet.getRow(4).getCell(1).getStringCellValue()

'Closes the file'
file.close()
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

'Navigate to stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), 'daveau19@sharklasers.com')

WebUI.setEncryptedText(findTestObject('Acusera247/Login/input_PasswordField'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Assay" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

WebUI.delay(1)


String[] celltext = new String[8];
String[] expected1, expected2, expected3

expected1 = ["Calcium", "Clinical Chemistry", "Arsenazo", "g/l", "Roche", "","",""]
expected2 = ["CA 125", "Immunoassay", "Chemiluminescent Immunoassay (CLIA)", "U/ml", "Roche", "","",""]
expected3 = ["Chloride", "Blood Gas", "Colorimetric", "mmol/l", "Randox Laboratories", "","",""]

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop:
for (int row = 0; row < rows_count; row++) {
	'To locate columns(cells) of that specific row'
	List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))
	 
	'To calculate no of columns(cells) In that specific row'
	int columns_count = Columns_row.size()
	 
	println((('Number of cells In Row ' + row) + ' are ') + columns_count)
	 
	'Loop will execute till the last cell of that specific row'
	for (int column = 0; column < columns_count; column++) {
		'It will retrieve text from each cell'
		celltext[column] = Columns_row.get(column).getText()		
		 
	}
	println(celltext)
	
	if(celltext==expected1 || celltext == expected2 || celltext==expected3){
		println('woohoo')
		
		
	}//if
	else if(celltext==expected3){
		println('boohoo')
	}
	
	
}
