import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Select the custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 24.7 - Home/span_Configuration_glyphicon glyphicon-plus'))

WebUI.delay(2)

'Select the Temperature tab '
WebUI.click(findTestObject('Gillian Objects/Custom Temp/a_Temperature'))

WebUI.delay(2)

'Click on the Bin icon beside the newly created Custom Temperature '
WebUI.click(findTestObject('Gillian Objects/Custom Temp/td'))

WebUI.delay(3)

'Click on  the "No" button'
WebUI.click(findTestObject('Gillian Objects/Custom Method/button_No'))

WebUI.delay(2)

'Click on the Bin icon beside the newly created Custom Temperature '
WebUI.click(findTestObject('Gillian Objects/Custom Temp/td'))

WebUI.delay(2)

'Click on the "Yes" button'
WebUI.click(findTestObject('Gillian Objects/Custom Method/button_Yes2'))

WebUI.delay(2)

'Verify the success messsage for deletion appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Method/div_Success Custom Method deleted. (1)'), 0)

'Verify the success messsage for deletion appears '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Method/div_Success Custom Method deleted. (1)'), 'Success! Custom Temperature deleted.')

'Close the browser '
WebUI.closeBrowser()

