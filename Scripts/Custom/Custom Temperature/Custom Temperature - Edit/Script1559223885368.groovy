import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 24.7 - Home/span_Configuration_glyphicon glyphicon-plus'))

WebUI.delay(2)

'Click on the Temperature tab'
WebUI.click(findTestObject('Gillian Objects/Custom Temp/a_Temperature'))

WebUI.delay(2)

WebUI.doubleClick(findTestObject('Gillian Objects/Custom Temp/td_22'))

WebUI.delay(2)

'Click on the Entry box to enter the Temperature'
WebUI.click(findTestObject('Gillian Objects/Custom Temp/Page_Acusera 247 - Custom Temperature Configuration/input_Description_description'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Gillian Objects/Custom Temp/Page_Acusera 247 - Custom Temperature Configuration/input_Description_description'), 
    30)

WebUI.waitForElementClickable(findTestObject('Gillian Objects/Custom Temp/Page_Acusera 247 - Custom Temperature Configuration/input_Description_description'), 
    30)

'Enter value 5'
WebUI.sendKeys(findTestObject('Gillian Objects/Custom TAE/input_Description_descriptionEDIT'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(3)

'Enter 22'
not_run: WebUI.setText(findTestObject('Gillian Objects/Custom Temp/Page_Acusera 247 - Custom Temperature Configuration/input_Description_description'), 
    '18')

WebUI.doubleClick(findTestObject('Gillian Objects/Custom TAE/input_Description_descriptionEDIT'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/Custom Temp/button_Save'))

WebUI.delay(2)

'Verify that the success banner is displayed to say the Custom Temperature has been created '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Temp/div_Success Custom Temperature created.'), 0)

'Verify that the success banner is displayed to say the Custom Temperature has been created '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Temp/div_Success Custom Temperature created.'), 'Success! Custom Temperature created.')

WebUI.delay(2)

'Verify that the success banner is displayed to say the Custom Temperature has been created '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Temp/td_temperature'), '12')

WebUI.delay(5)

WebUI.closeBrowser()

