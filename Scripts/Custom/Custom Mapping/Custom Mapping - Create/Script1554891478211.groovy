import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Select the custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 24.7 - Home/span_Configuration_glyphicon glyphicon-plus'))

WebUI.delay(2)

'Click on the Mapping tab'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/div_Mapping'))

WebUI.delay(2)

'Click on the Save button '
not_run: WebUI.click(findTestObject('Gillian Objects/Custom Mapping/button_Save'))

'Verify that the success message appears to say that the validation message is displayed '
not_run: WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Mapping/div_Information'), 0)

not_run: WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/div_Information'), 'Information You must create at least one configuration before saving. Please use the \'Add new record\' button to do so.')

WebUI.delay(1)

'Click on the "Add new record" button'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/a_Add new record'))

WebUI.delay(2)

'Click on the Analyte Name drop down arrow '
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/span_Instrument_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select ALT (GPT)'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/li_ALT (GPT)'))

WebUI.delay(1)

'Click on the drop down arrow under the Method heading'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/span_ALT (GPT)_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select Colorimetric'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/li_Colorimetric'))

WebUI.delay(1)

'Click on the Unit drop down arrow '
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/span_Colorimetric_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select Unit mU/ml'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/li_mUml'))

WebUI.delay(1)

'Select the Temperature drop down list'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/span_mUml_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select 30'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/li_30'))

WebUI.delay(1)

'Select the Instrument drop down list '
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/a_Mapping'))

WebUI.delay(1)

'Select 1100 Series'
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/li_1100 Series'))

WebUI.delay(1)

'Click on the Tick icon '
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/span_mUml_k-icon k-i-check'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Gillian Objects/Custom Mapping/button_Save'))

'Verify that the success message appears to say that the custom mapping has been saved '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Mapping/div_Success Custom mapping saved'), 0)

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/div_Success Custom mapping saved'), 'Success! Custom mapping saved.')

'Verify that the text in the table cell is correct'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/td_ALT (GPT)'), 'ALT (GPT)')

'Verify that the text in the table cell is correct'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/td_Colorimetric'), 'Colorimetric')

'Verify that the text in the table cell is correct'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/td_mUml'), 'cu2005')

'Verify that the text in the table cell is correct'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/td_30'), '30')

'Verify that the text in the table cell is correct'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Mapping/td_1100 Series'), '1100 Series')

WebUI.delay(5)

WebUI.closeBrowser()

