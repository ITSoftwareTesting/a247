import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the custom Tile'
WebUI.click(findTestObject('Gillian Objects/Custom Method/span_Configuration_glyphicon glyphicon-plus'))

WebUI.delay(1)

'Select the Method tab '
WebUI.click(findTestObject('Gillian Objects/Custom Method/a_Method'))

WebUI.delay(2)

'Click on the Add new button '
WebUI.click(findTestObject('Gillian Objects/Custom Method/button_Add New'))

WebUI.delay(2)

'Click on the save button'
WebUI.click(findTestObject('Gillian Objects/Custom Method/button_Save'))

'Verify that the validation message is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Method/div_Information Please Complete All Fields'), 0)

'Verify the text within the validation message'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Method/div_Information Please Complete All Fields'), 'Information Please Complete All Fields')

WebUI.delay(1)

'Input the name as "Custom Method 1"'
WebUI.setText(findTestObject('Gillian Objects/Custom Method/input_Name_name'), 'Custom Method 1')

WebUI.delay(2)

'Click on the save button'
WebUI.click(findTestObject('Gillian Objects/Custom Method/button_Save'))

'Verify that the success message is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Method/div_Success Custom Method created.'), 0)

'Verify the text within the success message'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Method/div_Success Custom Method created.'), 'Success! Custom Method created.')

WebUI.delay(3)

'Verify the text within the grid view to ensure it displays the newly created custom method'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Method/td_Custom Method 1'), 'Custom Method 1')

WebUI.delay(5)

WebUI.closeBrowser()

