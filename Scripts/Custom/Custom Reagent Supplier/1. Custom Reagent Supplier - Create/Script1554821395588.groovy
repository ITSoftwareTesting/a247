import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

'Click on the custom tile '
WebUI.click(findTestObject('Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the Reagent Supplier tab '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/a_Reagent Supplier'))

WebUI.delay(1)

'Click on the Add New button'
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/button_Add New'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/button_Save'))

WebUI.delay(1)

'Verify the success messsage for creating the Reagent supplier appears'
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/div_Information Please Complete All Fields'), 
    0)

'Verify the text within the success messsage '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/div_Information Please Complete All Fields'), 
    'Information Please Complete All Fields')

WebUI.delay(2)

'Set the text within the name field '
WebUI.setText(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/input_Name_name'), 
    'Automated Custom Reagent Supplier 1')

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/button_Save'))

'Verify the success messsage for creating the reagent supplier '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/div_Success CRS'), 
    0)

'Verify the text within the success messsage'
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/div_Success CRS'), 
    'Success! Custom Reagent Supplier created.')

WebUI.delay(2)

'Verify the text within the gridview to ensure it displays the newly created Custom Reagent Supplier '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Reagent Supplier Configuration/td_RSname'), 
    'Automated Custom Reagent Supplier 1')

WebUI.delay(5)

WebUI.closeBrowser()

