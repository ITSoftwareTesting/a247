import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the Total Allowable Error tab'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/a_Total Allowable Error'))

WebUI.delay(2)

'Click on the Add New button'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/button_Add NewMain'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/button_Save'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Instrument has been deleted'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom TAE/div_Information Please complete all required fields and try again'), 
    0)

'Verify the text within the success banner which is displayed to say the Custom Instrument has been deleted'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/div_Information Please complete all required fields and try again'), 
    'Information Please complete all required fields and try again')

WebUI.delay(1)

'Click on the validation banner to ensure that it disappears '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/div_Information Please complete all required fields and try again'))

WebUI.delay(1)

'Enter a TAE Type Name'
WebUI.setText(findTestObject('Gillian Objects/Custom TAE/input_TAE Type Name_taeTypeName'), 'Custom TAE 1')

WebUI.delay(1)

'Click on the Add New button'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/button_Add New'))

WebUI.delay(1)

'Click on the Analyte drop down list '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/span_AnalyteDropdown'))

WebUI.delay(1)

'Select Albumin'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/li_Albumin'))

WebUI.delay(1)

'Click on the Chemistry Type drop down '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/span_AlbuminCTdropdown'))

WebUI.delay(1)

'Select Specific Proteins'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/li_Specific Proteins'))

WebUI.delay(1)

'Click on the Target Bias entry box'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/td_TargetBias'))

WebUI.delay(3)

'Enter value 5'
WebUI.sendKeys(findTestObject('Gillian Objects/Custom TAE/input_Clinical Chemistry_TargetBias'), Keys.chord(Keys.DELETE, 
        '5', Keys.TAB))

WebUI.delay(3)

'Enter value 8 for Target TAE'
WebUI.sendKeys(findTestObject('Gillian Objects/Custom TAE/input_Specific Proteins_TargetTAE'), Keys.chord(Keys.DELETE, '8', 
        Keys.TAB))

WebUI.delay(1)

'Click on the Save button'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/button_Save'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Instrument has been deleted'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom TAE/div_Success Custom Total Allowable Error Saved'), 
    0)

'Verify the text within the success banner which is displayed to say the Custom Instrument has been deleted'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/div_Success Custom Total Allowable Error Saved'), 'Success! Custom Total Allowable Error Saved!')

WebUI.delay(1)

'Click on the success banner to ensure that it disappears '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/div_Success Custom Total Allowable Error Saved'))

WebUI.delay(1)

'Click on the drop down button beside the Custom TAE'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/td_Stats ComparisonDropdown'))

WebUI.delay(1)

'Verify the text within the gridview to ensure it matches the Custom TAE which has just been created'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/td_Custom TAE 1'), 'Custom TAE 1')

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/td_Albumin'), 'Albumin')

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/td_Specific Proteins'), 'Specific Proteins')

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/td_5'), '5')

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/td_8'), '8')

WebUI.closeBrowser()

