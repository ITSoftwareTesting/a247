import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the Total Allowable Error tab'
WebUI.click(findTestObject('Gillian Objects/Custom TAE/a_Total Allowable Error'))

WebUI.delay(1)

'Click on the delete icon '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/span_Custom TAEdelete'))

WebUI.delay(1)

'Click on the No button when the pop up box appears asking if you wish to delete '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/button_No'))

WebUI.delay(1)

'Click on the delete icon '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/span_Custom TAEdelete'))

WebUI.delay(1)

'Click on the Yes button when the pop up box appears asking if you wish to delete '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/button_Yes'))

WebUI.delay(1)

'Verify the success banner appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom TAE/div_Success Custom Total Allowable Error Deleted'), 
    0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/div_Success Custom Total Allowable Error Deleted'), 'Success! Custom Total Allowable Error Deleted')

WebUI.delay(1)

'Click on the success banner to ensure that it disappears '
WebUI.click(findTestObject('Gillian Objects/Custom TAE/div_Success Custom Total Allowable Error Deleted'))

'Verify that the no data available is displayed to verify there is no Cutom TAEs saved and it has been deleted successsfully'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom TAE/span_No data available'), 0)

'Verify the text within the element '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom TAE/span_No data available'), 'No data available')

WebUI.closeBrowser()

