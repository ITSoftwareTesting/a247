import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the Stats Comparison tab '
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/a_Stats Comparison'))

WebUI.delay(1)

'Double click on the preconfigured Stat Comparison method '
WebUI.doubleClick(findTestObject('Gillian Objects/Custom Stats Comparison/td_Instrument Group'))

WebUI.delay(1)

'Click on the Reagent Supplier Toggle to make it active '
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/span_Reagent SupplierToggle'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/button_Save'))

WebUI.delay(1)

'Verify the success banner is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Stats Comparison/div_Success Custom Comparison Group Saved'), 
    0)

'Verify the text within the success banner '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Stats Comparison/div_Success Custom Comparison Group Saved'), 
    'Success! Custom Comparison Group Saved!')

WebUI.delay(1)

'Verify the text within the grid view matches that which has been just configured '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Stats Comparison/td_Instrument Group'), 'Reagent Supplier/ Instrument Group')

WebUI.closeBrowser()

