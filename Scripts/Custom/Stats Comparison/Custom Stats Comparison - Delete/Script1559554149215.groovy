import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the stats comparison tab'
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/a_Stats Comparison'))

WebUI.delay(1)

'Click on the delete icon beside the configured Stats Comparion method'
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/span_Instrument Group_Delete'))

WebUI.delay(1)

'Click on the No button when the pop up appears to ask are you sure you wish to delete'
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/button_No'))

WebUI.delay(1)

'Click on the Delete button '
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/span_Instrument Group_Delete'))

WebUI.delay(1)

'Click on the Yes button '
WebUI.click(findTestObject('Gillian Objects/Custom Stats Comparison/button_Yes'))

WebUI.delay(1)

'Verify the success banner is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Stats Comparison/div_Success Custom Comparison Group Deleted'), 
    0)

'Verify the text within the success banner '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Stats Comparison/div_Success Custom Comparison Group Deleted'), 
    'Success! Custom Comparison Group Deleted')

WebUI.delay(1)

'Verify the text within the grid view shows "No Data available" '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Stats Comparison/span_No data available'), 'No data available')

WebUI.closeBrowser()

