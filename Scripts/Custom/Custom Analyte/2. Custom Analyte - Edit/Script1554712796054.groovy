import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(2)

'Double click on the custom analyte which is displayed within the gridview '
WebUI.doubleClick(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/td_Custom Automated Analyte 1'))

WebUI.delay(2)

'Input the new analyte name within the entry box '
WebUI.setText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/input_Name_name2'), 
    'Custom Automated Analyte 1 Edited')

WebUI.delay(1)

'Input the new analyte abbreviation in to the entry box '
WebUI.setText(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/input_Abbreviation_abbreviation'), 
    'CAA2')

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/button_Save'))

'Verify that the success banner has been displayed '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/div_Success Custom Analyte created'), 
    0)

'Verify the text within the Success banner '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/div_Success Custom Analyte created'), 
    'Success! Custom Analyte created.')

WebUI.delay(1)

'Verify that the new edited analyte is displayed within the gridview '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/td_Custom Automated Analyte 1 (1)'), 
    'Custom Automated Analyte 1 Edited')

'Verify the new edited analyte abbreviation is displayed within the grid view '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/td_CAA1'), 
    'CAA2')

WebUI.delay(5)

WebUI.closeBrowser()

