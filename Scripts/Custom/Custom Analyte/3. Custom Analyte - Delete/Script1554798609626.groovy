import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(2)

'Click on the Bin icon beside the configured custom analyte '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/span_CAA1_glyphicon glyphicon-trash'))

WebUI.delay(1)

'Click the No button when the display button asks do you wish to delete '
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/button_No'))

WebUI.delay(1)

'Click on the Bin icon beside the configured custom analyte again'
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/span_CAA1_glyphicon glyphicon-trash'))

WebUI.delay(1)

'Click on the Yes button when the display box asks if you wish to delete '
WebUI.click(findTestObject('Object Repository/Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/button_Yes'))

WebUI.delay(1)

'Verify that the success banner is displayed to say that the custom analyte has been deleted '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/div_Success Custom Analyte deleted'), 
    0)

'Verify the text within the success banner '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/div_Success Custom Analyte deleted'), 
    'Success! Custom Analyte deleted.')

WebUI.delay(1)

WebUI.closeBrowser()

