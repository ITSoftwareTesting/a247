import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the Instrument tag '
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/a_Instrument (1)'))

WebUI.delay(1)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

KeywordLogger log = new KeywordLogger()

log.logInfo('Rows = ' + rows_count)

if (rows_count > 0) {
    'Loop will execute for all the rows of the table'
    Loop: for (int row = 0; row < rows_count; row++) {
        'Click on Delete Icon'
        WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/span_CustomInstrumentDelete'))

        WebUI.delay(1)

        'Click "Yes" on the Pop Up modal to delete the Instrument'
        WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

        WebUI.delay(1)

        'Verify that the success banner is displayed to say the Custom Instrument has been deleted'
        WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument deleted'), 
            0)

        'Verify the text within the success banner which is displayed to say the Custom Instrument has been deleted'
        WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument deleted'), 
            'Success! Custom Instrument deleted.')

        WebUI.delay(1)

        WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument deleted'))

        WebUI.delay(1)
    }
}

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Instrument has been deleted'
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/span_No items to display'), 
    0)

'Verify the text within the success banner which is displayed to say the Custom Instrument has been deleted'
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/span_No items to display'), 
    'No items to display')

WebUI.closeBrowser()

