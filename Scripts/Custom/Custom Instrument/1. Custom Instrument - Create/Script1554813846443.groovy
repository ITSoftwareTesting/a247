import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String AutoGenerateManu

String AutoGenerateGroup

String AutoGenerateModel

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

'Click on the Custom tile '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Home/div_Custom'))

WebUI.delay(1)

'Click on the Instrument tag '
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Analyte Configuration/a_Instrument (1)'))

WebUI.delay(1)

'Click on the Add New button '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Add New'))

WebUI.delay(1)

'Click on the Manufacturer drop down '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/span_Select a manufacturer'))

WebUI.delay(1)

AutoGenerateManu = CustomKeywords.'autoGenerators.AutoStringGenerator.randomString'(8)

'Set the text to a random string of generated letters'
WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_No data found_k-textbox'), 
    AutoGenerateManu)

WebUI.delay(3)

'Click on the Add New button'
WebUI.click(findTestObject('Page_Acusera 247 - Custom Instrument Configuration/button_Add new item'))

WebUI.delay(1)

'Click on the yes button within the pop up box '
WebUI.click(findTestObject('Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Manufacturer has been created '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Manufacturer created'), 
    0)

'Verify the text within the success banner  that is displayed to say the Custom Manufacturer has been created '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Manufacturer created'), 
    'Success! Custom Instrument Manufacturer created.')

WebUI.delay(2)

'Click on the success banner to ensure that it disappears'
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Manufacturer created'))

WebUI.delay(1)

'Clcik on the group drop down '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/span_Select a group'))

WebUI.delay(1)

AutoGenerateGroup = CustomKeywords.'autoGenerators.AutoStringGenerator.randomString'(8)

'Set the text to a random string of generated letters'
WebUI.setText(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/input_Add new item_k-textbox'), 
    AutoGenerateGroup)

WebUI.delay(3)

'Click on the Add New button'
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/button_Add new group'))

WebUI.delay(1)

'Click on the yes button within the pop up box '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Group has been created '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Group created'), 
    0)

'Verify the text within the success banner that displayed to say the Custom Group has been created '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Group created'), 
    'Success! Custom Instrument Group created.')

WebUI.delay(2)

'Click on the success banner to ensure that it disappears'
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Group created'))

WebUI.delay(1)

'Click on the Model drop down '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/span_Select a model'))

WebUI.delay(1)

AutoGenerateModel = CustomKeywords.'autoGenerators.AutoStringGenerator.randomString'(8)

'Set the text to a random string of generated letters'
WebUI.setText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/input_Add new item_k-textbox model'), 
    AutoGenerateModel)

WebUI.delay(3)

'Click on the Add New button'
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/button_Add new model'))

WebUI.delay(1)

'Click on the yes button within the pop up box '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Yes'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Model has been created '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Model created'), 
    0)

'Verify the text within the success banner which is displayed to say the Custom Model has been created '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Model created'), 
    'Success! Custom Instrument Model created.')

WebUI.delay(2)

'Click on the success banner to ensure that it disappears'
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument Model created'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Object Repository/Page_Acusera 247 - Custom Instrument Configuration/button_Save'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the Custom Instrument has been created '
WebUI.verifyElementPresent(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument created'), 
    0)

'Verify the text within the success banner which is displayed to say the Custom Instrument has been created '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument created'), 
    'Success! Custom Instrument created.')

WebUI.delay(2)

'Click on the success banner to ensure that it disappears'
WebUI.click(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/div_Success Custom Instrument created'))

'Verify the text within the Manufacturer cell in the gridview matches that random string which was entered '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/td_Custom Manu'), 
    AutoGenerateManu)

'Verify the text within the Group cell in the gridview matches that random string which was entered '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/td_CustomGroup'), 
    AutoGenerateGroup)

'Verify the text within the Modelcell in the gridview matches that random string which was entered '
WebUI.verifyElementText(findTestObject('Acusera247/Custom Configuration/Page_Acusera 247 - Custom Instrument Configuration/td_Custom Model'), 
    AutoGenerateModel)

WebUI.delay(5)

WebUI.closeBrowser()

