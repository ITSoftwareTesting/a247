import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the Custom tile'
WebUI.click(findTestObject('Object Repository/Page_Acusera 24.7 - Home/span_Configuration_glyphicon glyphicon-plus'))

WebUI.delay(2)

'Select the Lot tab '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/div_Lot'))

WebUI.delay(5)

'Click on the Add new button '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/button_Add NewLot'))

WebUI.delay(2)

'Enter 0904GW within the Lot Number entry field'
WebUI.setText(findTestObject('Gillian Objects/Custom Lot/input_Lot Number_lotNumberText'), '0904GW')

'Enter 0904 Custom Lot within the Lot Name entry field '
WebUI.setText(findTestObject('Gillian Objects/Custom Lot/input_Lot Name_LotName'), '0904 Custom Lot')

WebUI.delay(2)

'Click on the calendar icon within the Expiry Date entry box'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/span_Expiry Date_k-icon k-i-calendar (1)'))

WebUI.delay(2)

'Click on the arrow pointing to the next month'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/span_No data found_k-icon k-i-arrow-60-right'))

'Click on the arrow pointing to the next month'
WebUI.doubleClick(findTestObject('Gillian Objects/Custom Lot/span_No data found_k-icon k-i-arrow-60-right'))

'Click on the arrow pointing to the next month'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/span_No data found_k-icon k-i-arrow-60-right'))

'Click on the arrow pointing to the next month'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/span_No data found_k-icon k-i-arrow-60-right'))

'Click on the arrow pointing to the next month'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/span_No data found_k-icon k-i-arrow-60-right'))

'Click on the 28th'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/a_28'))

WebUI.delay(2)

'Click on the chemistry type drop down icon'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/span_Select a chemistry type_k-icon k-i-arrow-60-down'))

WebUI.delay(2)

'Select "Clinical Chemistry"'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/li_Clinical Chemistry_dropdown'))

WebUI.delay(2)

'Select the Analyte drop down box '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/div_Clear All_k-multiselect-wrap k-floatwrap'))

WebUI.delay(2)

'Select ALT (GPT) '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/li_ALT (GPT)'))

WebUI.delay(2)

'Click off the Analyte selection box '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/div_Instrument Model Selection Add All'))

WebUI.delay(2)

'Click on the Instrument Model Selection drop down list '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/div_Clear All_k-multiselect-wrap k-floatwrap_1'))

WebUI.delay(2)

'Select Diestro 103 Series 103'
WebUI.click(findTestObject('Gillian Objects/Custom Lot/li_Diestro  103 Series 103'))

WebUI.delay(2)

'Click off the instrument Model selection drop down '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/div_Instrument Model Selection Add All'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/Custom Lot/button_Save'))

'Verify the Success banner is displayed to say that the Custom Lot has been added '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Custom Lot/div_Success Custom lot has been added'), 0)

'Verify the Success banner is displayed to say that the Custom Lot has been added '
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Lot/div_Success Custom lot has been added'), 'Success! Custom lot has been added')

WebUI.delay(1)

'Verify that the text displayed within the grid view is that which was created'
WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Lot/td_0904GW (1)'), '0904GW')

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Lot/td_0904 Custom Lot'), '0904 Custom Lot')

WebUI.verifyElementText(findTestObject('Gillian Objects/Custom Lot/li_Clinical Chemistry'), 'Clinical Chemistry')

WebUI.delay(5)

WebUI.closeBrowser()

