import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.setText(findTestObject('Oran demo objects/Events/Page_Randox QC/input_Use your account details to log in_UserName'), 
    'katalondemo@sharklasers.com')

WebUI.setEncryptedText(findTestObject('Oran demo objects/Events/Page_Randox QC/input_Use your account details to log in_Password'), 
    'ZM/VR/a+KpRW0CGCfSq1yw==')

WebUI.click(findTestObject('Oran demo objects/Events/Page_Randox QC/button_Log in'))

WebUI.click(findTestObject('Oran demo objects/Events/Page_Acusera 247 - Home/div_Configuration'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/delete_InstrumentRow1'))

WebUI.delay(1)

WebUI.click(findTestObject('Oran demo objects/Events/Page_Acusera 247 - Instrument Configuration/button_Yes_delete_record'))

WebUI.delay(1)

WebUI.closeBrowser()

