import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

'Click on the Utilities tile'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/span_UtilitiesTile'))

WebUI.delay(1)

'Click on the change control set tab'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/a_Change Control Set'))

WebUI.delay(1)

'Double click on the configured QC test '
WebUI.doubleClick(findTestObject('Gillian Objects/Change Control Set/td_CA 125doubleClickQCtest'))

WebUI.delay(1)

'Click on the Lot 1 drop down list '
WebUI.click(findTestObject('Gillian Objects/Change Control Set/span_Select a lot_k-select'))

WebUI.delay(1)

'Select 1306EC'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/li_1306EC'))

WebUI.delay(1)

'Click on the clear icon'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/span_Clear_k-icon k-i-trash'))

WebUI.delay(3)

'Click on the Lot 1 drop down list '
WebUI.click(findTestObject('Gillian Objects/Change Control Set/span_Select a lot_k-select'))

WebUI.delay(2)

'Select 1306EC'
WebUI.doubleClick(findTestObject('Gillian Objects/Change Control Set/li_1306EC'))

WebUI.delay(1)

'Click on the change control set button'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/button_Change Control Set'))

WebUI.delay(1)

'Verify the successful banner is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Control Set/h4_Successful'), 0)

'Verify the text withint the banner says Successful'
not_run: WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/h4_Successful'), 'Successful')

'Verify the success banner is displayed'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Control Set/div_Confirmation Change control set process has been completed Please see the report'), 
    0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/div_Confirmation Change control set process has been completed Please see the report'), 
    'Confirmation Change control set process has been completed. Please see the report grid below for more details.')

'Verify the text within the grid view'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/div_The following tests have been updated successfully and their'), 
    'The following tests have been updated successfully and their control set has been changed.')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_CA 125verify'), 'CA 125')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_CLIA'), 'Chemiluminescent Immunoassay (CLIA)')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_Uml'), 'U/ml')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_Instrument 1'), 'Instrument 1')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/b_1306ECverify'), '1306EC')

WebUI.delay(5)

'Click on the configuration tab'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/div_Configuration'))

WebUI.delay(1)

'Click on the QC tab'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/a_QC Test'))

'Verify the text within the QC test'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_CA 125qc'), 'CA 125')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_Immunoassayqc'), 'Immunoassay')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_Chemiluminescent Immunoassay (CLIA)qc'), 'Chemiluminescent Immunoassay (CLIA)')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_Instrument 1qc'), 'Instrument 1')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_Umlqc'), 'U/ml')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/td_1306ECqc'), '1306EC')

'Click on the bin icon beside the new QC test'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/span_Uml_glyphicon glyphicon-trash'))

WebUI.delay(1)

'Click on the Yes button'
WebUI.click(findTestObject('Gillian Objects/Change Control Set/button_Yes'))

WebUI.delay(1)

'Verify the success banner is displayed'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Control Set/div_Success QC Test configuration deleted'), 
    0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Control Set/div_Success QC Test configuration deleted'), 
    'Success! QC Test configuration deleted.')

WebUI.delay(5)

WebUI.closeBrowser()

