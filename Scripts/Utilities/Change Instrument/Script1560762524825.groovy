import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.SimpleDateFormat as SimpleDateFormat
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(1)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/span_configuration'))

WebUI.delay(1)

'Click on Add new instrument '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/button_Add NewInstrument'))

WebUI.delay(1)

'Enter the name "Instrument 2"'
WebUI.setText(findTestObject('Gillian Objects/Change Instrument/input_Name_InstrumentName'), 'Instrument 2')

WebUI.delay(1)

'Click on the manufacturer drop down arrow '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/span_ManuDownArrow'))

WebUI.delay(1)

'Select Abaxis'
WebUI.click(findTestObject('Gillian Objects/Change Instrument/li_Abaxis'))

WebUI.delay(1)

'Click on the Group drop down arrow '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/span_GroupDropdown'))

WebUI.delay(1)

'Select Piccolo from the list'
WebUI.click(findTestObject('Gillian Objects/Change Instrument/li_PiccoloGroup'))

WebUI.delay(1)

'Click on the Model drop down list '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/span_ModelDropdown'))

WebUI.delay(1)

'Selct Piccolo from the list '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/li_PiccoloModel'))

WebUI.delay(1)

'Click on the Save button'
WebUI.click(findTestObject('Gillian Objects/Change Instrument/button_Save'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the instrument has been successfully created'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Instrument/div_Success Instrument configuration created'), 
    0)

'Verify the text within the success banner '
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/div_Success Instrument configuration created'), 
    'Success! Instrument configuration created')

WebUI.delay(1)

'Verify the text within the Instrument grid view to ensure that the instrument is being displayed '
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_Instrument 2Confirm'), 'Instrument 2')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_AbaxisConfirm'), 'Abaxis')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_PiccoloGroupConfirm'), 'Piccolo')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_PiccoloModelConfirm'), 'Piccolo')

'Click on the Utilities tab '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/div_Utilities'))

WebUI.delay(1)

'Click onthe change instrument tab '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/a_Change Instrument'))

WebUI.delay(1)

'Double click on the configured QC test for CA 125 '
WebUI.doubleClick(findTestObject('Gillian Objects/Change Instrument/td_CA 125'))

WebUI.delay(1)

'Ensure the correct page is being displayed by the heading '
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/h3_Select New Instrument'), 'Select New Instrument')

WebUI.delay(1)

'Click on the drop down arrow within the Instrument entry box '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/span_Select Instrument_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select Instrument 2 '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/li_Instrument 2'))

WebUI.delay(1)

'Click on the clear button to ensure that the field is cleared '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/button_Clear'))

WebUI.delay(1)

'Click on the drop down list within the Instrument drop down '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/span_Select Instrument_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select Instrument 2'
WebUI.click(findTestObject('Gillian Objects/Change Instrument/li_Instrument 2'))

WebUI.delay(1)

'Click on the Change Instrument button '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/button_Change Instrument'))

WebUI.delay(1)

'Verify the success banner is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Instrument/div_Confirmation Change Instrument process has been completed'), 
    0)

'Verify the text within the success banner'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/div_Confirmation Change Instrument process has been completed'), 
    'Confirmation Change Instrument process has been completed.')

'Verify the successful heading is displayed'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Change Instrument/h4_Successful'), 0)

'Verify that the heading says "Successful"'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/h4_Successful'), 'Successful')

'Verify the text within the gridview shows the new Instrument as being configured'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_CA 125 (1)'), 'CA 125')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_Chemiluminescent Immunoassay (CLIA)'), 'Chemiluminescent Immunoassay (CLIA)')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_Uml'), 'U/ml')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/b_Instrument 2'), 'Instrument 2')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_1304EC'), '1304EC')

WebUI.delay(2)

'Click on the configuration tab '
WebUI.click(findTestObject('Gillian Objects/Change Control Set/div_Configuration'))

WebUI.delay(1)

'Click on the QC Test tab '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/a_QC Test'))

'Verify the text within the QC test gridview to ensur eth enew test is being displayed'
WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_CA 125qcConfirm'), 'CA 125')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_ImmunoassayQcConfirm'), 'Immunoassay')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_Chemiluminescent Immunoassay (CLIA)qcConfirm'), 
    'Chemiluminescent Immunoassay (CLIA)')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_Instrument 2QcConfirm'), 'Instrument 2')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_UmlqcConfirm'), 'U/ml')

WebUI.verifyElementText(findTestObject('Gillian Objects/Change Instrument/td_1304ECqcConfirm'), '1304EC')

'Delete the qc test created within change instrument to clean up after automation test'
WebUI.delay(3)

'Get current Date and yime'
Date now = new Date()

'Call the date Format class and specify how you want the date to be formatted'
SimpleDateFormat dateFormatter = new SimpleDateFormat('dd-MMM-yyyy')

'Print the date in the formatted style'
System.out.println('Format 1:   ' + dateFormatter.format(now))

currentDate = dateFormatter.format(now)

int thirteen = 13

'Declare an array which will be used to save the row contents in'
String[] celltext = new String[thirteen]

'Declare 3 arrays of what you expect to see within the rows'
String[] expected1 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'Instrument 1', 'U/ml', '1304EC', ''
    , '', currentDate, '', '', '', '']

String[] expected2 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'Instrument 2', 'U/ml', '1304EC', ''
    , '', currentDate, '', '', '', '']

String[] expected3 = ['Calcium', 'Clinical Chemistry', 'Arsenazo', 'Instrument 1', 'g/l', '00125-PL', '', '', currentDate
    , '', '', '', '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    String delimiter = '| '

    println('Row text: ' + String.join(delimiter, celltext))

    println('Expected Text: ' + String.join(delimiter, expected1))

    if ((celltext == expected1) || (celltext == expected3)) {
        println('This configuration will not be deleted' //The following code resets the count so that the loop restarts checking each row from the top of the table
            )
    } else {
        Columns_row.get(12).findElement(By.tagName('a')).click()

        WebUI.delay(2)

        'Ensure that the pop up box appears to ask if you are sure you want to delete the QC test'
        WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'), 
            0)

        'Ensure the pop up box text is correct'
        WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'), 
            'Are you sure you want to delete this record?')

        'Click on the No button '
        WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_No'))

        WebUI.delay(2)

        Columns_row.get(12).findElement(By.tagName('a')).click()

        'Ensure the pop up box text is correct'
        WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'), 
            'Are you sure you want to delete this record?')

        'Click on the yes button'
        WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Yes'))

        WebUI.delay(1)

        'Ensure the success message appears to say the QC test has been successfully deleted '
        WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration deleted.'), 
            'Success! QC Test configuration deleted.')

        rows_table = Table.findElements(By.tagName('tr'))

        row = -1

        rows_count = rows_table.size()
    }
}

WebUI.delay(5)

'Click on the instrument tab '
WebUI.click(findTestObject('Gillian Objects/Change Instrument/a_Instrument'))

WebUI.delay(3)

int six = 6

'Declare 4 arrays. The first one ,"celltext", is going to be the array in which we save the values from each row into'
celltext = new String[six]

expected1 = ['Instrument 1', 'Roche', 'Cobas', 'Cobas c501', '', '']

expected2 = ['Instrument 2', 'Abaxis', 'Piccolo', 'Piccolo', '', '']

'To locate table'
Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    columns_count = Columns_row.size()

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    String delimiter = '| '

    println(String.join(delimiter, celltext))

    println(String.join(delimiter, expected2))

    if (celltext == expected1) {
        println('This configuration will not be deleted as it is to be used throughout the test suite' //The following code resets the count so that the loop restarts checking each row from the top of the table
            )
    } else {
        Columns_row.get(5).findElement(By.tagName('a')).click()

        WebUI.delay(2)

        WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_NoPopUP'))

        WebUI.delay(2)

        Columns_row.get(5).findElement(By.tagName('a')).click()

        'Click "Yes" on the Pop Up modal to delete Instrument 3'
        WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_YesPopUP'))

        WebUI.delay(1)

        WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'), 'Success! Instrument configuration deleted', 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'))

        rows_table = Table.findElements(By.tagName('tr'))

        row = -1

        rows_count = rows_table.size()
    }
    
    WebUI.delay(5)

    WebUI.closeBrowser()
}

