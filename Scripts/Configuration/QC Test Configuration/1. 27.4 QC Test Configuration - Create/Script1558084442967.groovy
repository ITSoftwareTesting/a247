import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the QC test tab'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/a_QC Test'))

WebUI.delay(2)

'Click on the Add New button'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/div_Add New'))

WebUI.delay(2)

'Click on the instrument drop down list '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _2'))

'Selecft Instrument 1'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_Instrument 1'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Select on the assay drop down list '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configuration/span_Select an item..._k-icon k-i-arrow-60-down'))

WebUI.delay(2)

'Select CA 125 | Immunoassay | Chemiluminescence | U/ml | Roche'
WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_CA 125  Immunoassay  Chemil'))

WebUI.delay(2)

'Select the Lot 1 drop down list '
WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _1'))

WebUI.delay(2)

'Select lot 1304EC '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_1304EC'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Save'))

'Check that the success message appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), 
    0)

WebUI.waitForElementVisible(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), 
    0)

WebUI.closeBrowser()

