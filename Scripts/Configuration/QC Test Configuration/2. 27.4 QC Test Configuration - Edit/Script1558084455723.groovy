import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.SimpleDateFormat as SimpleDateFormat
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password '
WebUI.setMaskedText(findTestObject('Gillian Objects/Log in/input_Use your account details_1'), password)

'Click "Log in"'
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the QC test tab'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/a_QC Test'))

WebUI.delay(2)

'Click on the Add New button'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/div_Add New'))

WebUI.delay(2)

'Click on the instrument drop down list '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _2'))

'Select Instrument 1'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_Instrument 1'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Select on the assay drop down list '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configuration/span_Select an item..._k-icon k-i-arrow-60-down'))

WebUI.delay(2)

'Select CA 125 | Immunoassay | Chemiluminescence | U/ml | Roche'
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_CA 125  Immunoassay  Chemil'))

WebUI.delay(2)

'Select the Lot 1 drop down list '
WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _1'))

WebUI.delay(2)

'Select lot 1305EC '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_1305EC'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Save'))

'Check that the success message appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), 
    2)

WebUI.click(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), FailureHandling.STOP_ON_FAILURE)

WebUI.refresh()

WebUI.delay(2)

'Double click on the newly created QC Test '
WebUI.doubleClick(findTestObject('Gillian Objects/QC Test Config/td_CA 125'))

WebUI.delay(2)

'Click on the Assay drop down button'
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configuration/span_Select an item..._k-icon k-i-arrow-60-down'))

WebUI.delay(2)

'Select "Calcium | Clinical Chemistry | Arsenazo | g/l | Roche"'
WebUI.click(findTestObject('Page_Acusera 24.7 - QC Test Configuration/li_Calcium  Clinical Chemistry  Arsenazo  gl  Roche'))

WebUI.delay(2)

'Click on the Lot 1 dropdown arrow'
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _1'))

WebUI.delay(2)

'Click Lot 00125-PL'
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_00125-PL'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Save'))

WebUI.delay(2)

'Check that the success message appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration created.'), 
    0)

WebUI.delay(2)

WebUI.refresh()

WebUI.delay(2)

Date now = new Date()

SimpleDateFormat dateFormatter = new SimpleDateFormat('dd-MMM-yyyy')

System.out.println('Format 1:   ' + dateFormatter.format(now))

currentDate = dateFormatter.format(now)

int thirteen = 13

String[] celltext = new String[thirteen]

String[] expected1 = ['Calcium', 'Clinical Chemistry', 'Arsenazo', 'Instrument 1', 'g/l', '00125-PL', '', '', currentDate
    , '', '', '', '']

String[] expected2 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'Instrument 1', 'U/ml', '1304EC', ''
    , '', currentDate, '', '', '', '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    String delimiter = '| '

    println('Row text: ' + String.join(delimiter, celltext))

    println('Expected Text: ' + String.join(delimiter, expected1))

    if (celltext == expected1 || celltext == expected2) {
		
		println('This Configuration is part of the automation suite.')
        
		
    }else {
        println('This configuration is not part of the automation suite, it will be deleted')
		
		Columns_row.get(12).findElement(By.tagName('a')).click()
		
				WebUI.delay(2)
		
		'Ensure that the pop up box appears to ask if you are sure you want to delete the QC test'
		WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'),
			0)

		'Ensure the pop up box text is correct'
		WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'),
			'Are you sure you want to delete this record?')

		WebUI.delay(2)

		'Click on the yes button'
		WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Yes'))

		WebUI.delay(2)

		'Ensure the success message appears to say the QC test has been successfully deleted '
		WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration deleted.'),
			30)
    }
}

WebUI.closeBrowser()

