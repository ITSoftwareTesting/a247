import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat as SimpleDateFormat

import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password '
WebUI.setMaskedText(findTestObject('Gillian Objects/Log in/input_Use your account details_1'), password)

'Click "Log in"'
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the QC test tab'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/a_QC Test'))

WebUI.delay(2)

'Click on the Add New button'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/div_Add New'))

WebUI.delay(2)

'Select on the instrument drop down list '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _2'))

'Selecft Instrument 1'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/li_Instrument 1'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the assay drop down list '
WebUI.click(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configuration/span_Select an item..._k-icon k-i-arrow-60-down'))

WebUI.delay(2)

'Select the Assay CA 125 | Immunoassay | Chemiluminescence | U/ml | Roche'
WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_CA 125  Immunoassay  Chemil'))

WebUI.delay(2)

'Select the drop down list for Lot 1 '
WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/span_Select an item..._k-icon _1'))

WebUI.delay(2)

'Select Lot 1064EC'
WebUI.click(findTestObject('Object Repository/Gillian Objects/Page_Acusera 24.7 - QC Test Configu/li_1064EC'))

WebUI.delay(2)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Save'))

'Ensure the success message is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Page_Acusera 24.7 - QC Test Configuration/div_Information QC Test configuration already exists'), 
    0)

WebUI.delay(2)

WebUI.refresh()

WebUI.delay(2)

'Get current Date and yime'
Date now = new Date()

'Call the date Format class and specify how you want the date to be formatted'
SimpleDateFormat dateFormatter = new SimpleDateFormat('dd-MMM-yyyy')

'Print the date in the formatted style'
System.out.println('Format 1:   ' + dateFormatter.format(now))

currentDate = dateFormatter.format(now)

int thirteen = 13

'Declare an array which will be used to save the row contents in'
String[] celltext = new String[thirteen]

'Declare 3 arrays of what you expect to see within the rows'
String[] expected1 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'Instrument 1', 'U/ml', '1064EC', ''
    , '', currentDate, '', '', '', '']

String[] expected2 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'Instrument 1', 'U/ml', '1304EC', ''
    , '', currentDate, '', '', '', '']

String[] expected3 = ['Calcium', 'Clinical Chemistry', 'Arsenazo', 'Instrument 1', 'g/l', '00125-PL', '', '', currentDate
    , '', '', '', '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    String delimiter = '| '

    println('Row text: ' + String.join(delimiter, celltext))

    println('Expected Text: ' + String.join(delimiter, expected1))

    if (celltext==expected2 || celltext==expected3) {
		
		println('This configuration will not be deleted')
		
    }else{
        		
		Columns_row.get(12).findElement(By.tagName('a')).click()
	
		WebUI.delay(2)

		'Ensure that the pop up box appears to ask if you are sure you want to delete the QC test'
		WebUI.verifyElementPresent(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'),
			0)

		'Ensure the pop up box text is correct'
		WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'),
			'Are you sure you want to delete this record?')

		'Click on the No button '
		WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_No'))

		WebUI.delay(2)

		Columns_row.get(12).findElement(By.tagName('a')).click()

		'Ensure the pop up box text is correct'
		WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/p_Are you sure you want to delete this record'),
			'Are you sure you want to delete this record?')

		'Click on the yes button'
		WebUI.click(findTestObject('Gillian Objects/QC Test Config/button_Yes'))

		WebUI.delay(1)

	   'Ensure the success message appears to say the QC test has been successfully deleted '
		WebUI.verifyElementText(findTestObject('Gillian Objects/QC Test Config/div_Success QC Test configuration deleted.'), 'Success! QC Test configuration deleted.')
		
		//The following code resets the count so that the loop restarts checking each row from the top of the table
		rows_table = Table.findElements(By.tagName('tr'))
		row= (-1)
		rows_count = rows_table.size()
    }
}

WebUI.closeBrowser()
