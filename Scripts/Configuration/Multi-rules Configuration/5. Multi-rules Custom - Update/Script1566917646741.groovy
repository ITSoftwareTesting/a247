import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(5)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the Mulit-rules tab'
WebUI.click(findTestObject('Gillian Objects/Multi-rules/div_Multi-rules'))

WebUI.delay(2)

'Double click on QC Test CA 125 '
WebUI.doubleClick(findTestObject('Gillian Objects/Multi-rules/td_CA 125'))

WebUI.delay(1)

'Click the next button '
WebUI.click(findTestObject('Gillian Objects/Multi-rules/a_Across_nextPage'))

WebUI.delay(3)

'Turn off the reject toggle for the new custom multi-rule'
WebUI.click(findTestObject('Gillian Objects/Multi-rules/span_-nx_reject'))

WebUI.delay(2)

'Click on the Alert for the nx multi rule '
WebUI.click(findTestObject('Gillian Objects/Multi-rules/span_-AlertToggle'))

'Click on the Save button'
WebUI.click(findTestObject('Gillian Objects/Multi-rules/div_Save'))

WebUI.verifyElementPresent(findTestObject('Gillian Objects/Multi-rules/div_Success Multirules have been applied to your selected tests'), 
    0)

WebUI.delay(1)

'Click on the down arrow at the end of the configured QC Test '
WebUI.click(findTestObject('Gillian Objects/Multi-rules/td_Closed_k-hierarchy-cell'))

WebUI.delay(1)

'Verify the abbreviation is displayed as 2:2s'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_22s'), '2:2s')

'Verify that the number of results is 2'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_2'), '2')

'Get the position that the toggle for alert is set at '
value = WebUI.getCSSValue(findTestObject('Gillian Objects/Multi-rules/td_alert22'), 'transform')

println(value)

'Checking that the value is correct and the toggle is switched on '
WebUI.verifyEqual(value, 'matrix(1, 0, 0, 1, 22, 0)')

'Verify that the Run Type is set to "Both" '
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_Both'), 'Both')

WebUI.delay(1)

'Verify the abbreviation is displayed as 7T'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_7T'), '7T')

'Verify that the number of results is 7'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_7'), '7')

'Verify that the Run Type is set to "Across Run" '
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_Across Run'), 'Across Run')

'Get the position that the toggle for reject is set at '
value = WebUI.getCSSValue(findTestObject('Gillian Objects/Multi-rules/td_Reject7T'), 'transform')

println(value)

'Checking that the value is correct and the toggle is switched on '
WebUI.verifyEqual(value, 'matrix(1, 0, 0, 1, 22, 0)')

WebUI.delay(2)

'Verify the abbreviation is displayed as nx'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_nxTable'), 'nx')

'Verify that the number of results is 12'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_12Table'), '12')

'Get the position that the toggle for reject is set at '
value = WebUI.getCSSValue(findTestObject('Gillian Objects/Multi-rules/span_NXalertToggle'), 'transform')

println(value)

'Checking that the value is correct and the toggle is switched on '
WebUI.verifyEqual(value, 'matrix(1, 0, 0, 1, 22, 0)')

'Verify that the Run Type is set to "Across Run" '
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_Across run (1)'), 'Across Run')

WebUI.delay(10)

WebUI.closeBrowser()

