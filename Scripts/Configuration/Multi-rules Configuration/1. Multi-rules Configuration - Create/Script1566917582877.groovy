import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(5)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the Mulit-rules tab'
WebUI.click(findTestObject('Gillian Objects/Multi-rules/div_Multi-rules'))

WebUI.delay(2)

'Double click on QC Test CA 125 '
WebUI.doubleClick(findTestObject('Gillian Objects/Multi-rules/td_CA 125'))

WebUI.delay(1)

'Click on the toggle for Reject for 10x'
WebUI.click(findTestObject('Gillian Objects/Multi-rules/span_-_k-switch-container km-switch-container'))

WebUI.delay(1)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/Multi-rules/div_Save'))

'Verify that the success banner is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Multi-rules/div_Success Multirules have been applied to your selected tests'), 
    0)

WebUI.delay(1)

'Click on the down arrow beside the configured QC test '
WebUI.click(findTestObject('Gillian Objects/Multi-rules/td_Closed_k-hierarchy-cell'))

'Verify the abbreviation test is displayed as 10x'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_10x'), '10x')

'Verify that the Number of Results is 10'
WebUI.verifyElementText(findTestObject('Gillian Objects/Multi-rules/td_10'), '10')

'Get the position that the toggle for reject is set at '
value = WebUI.getCSSValue(findTestObject('Gillian Objects/Multi-rules/span_---_k-switch-container km-switch-container_grid table'), 
    'transform')

println(value)

'Checking that the value is correct and the toggle is switched on '
WebUI.verifyEqual(value, 'matrix(1, 0, 0, 1, 22, 0)')

WebUI.closeBrowser()

