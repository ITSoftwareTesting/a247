import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the total allowable error tab '
WebUI.click(findTestObject('Gillian Objects/TAE Config/a_Total Allowable Error'))

WebUI.delay(3)

'Click on the drop down arrow'
WebUI.click(findTestObject('Gillian Objects/TAE Config/span_Biological Variation_k-icon k-i-arrow-60-down'))

WebUI.delay(1)

'Select Rilibak from the list'
WebUI.click(findTestObject('Gillian Objects/TAE Config/li_Rilibak'))

WebUI.delay(1)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/TAE Config/button_Save'))

WebUI.delay(1)

'Verify that the success banner is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/TAE Config/div_Success Total Allowable Error Type successfully changed'), 
    0)

'Verify the text within the success banner is correct '
WebUI.verifyElementText(findTestObject('Gillian Objects/TAE Config/div_Success Total Allowable Error Type successfully changed'), 
    'Success! Total Allowable Error Type successfully changed')

WebUI.delay(1)

'Click on the success banner so that it disappears'
WebUI.click(findTestObject('Gillian Objects/TAE Config/div_Success Total Allowable Error Type successfully changed'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the multi-rules tab '
WebUI.click(findTestObject('Gillian Objects/TAE Config/a_Multi-rules'))

WebUI.delay(2)

'Click on the TAE tab'
WebUI.click(findTestObject('Gillian Objects/TAE Config/a_Total Allowable Error'))

WebUI.delay(1)

'Verify the text is showing Rilibak as expected '
WebUI.verifyElementText(findTestObject('Gillian Objects/TAE Config/span_Rilibak'), 'Rilibak')

WebUI.delay(5)

WebUI.closeBrowser()

