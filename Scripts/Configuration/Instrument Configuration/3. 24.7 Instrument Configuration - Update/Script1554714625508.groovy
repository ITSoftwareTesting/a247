import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'Read the users email from the file and save it to the email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the user password from the file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Prereqs: Have a participant Registered'
WebUI.openBrowser('')

'Navigate to the STES url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.maximizeWindow()

'input the users email into the Username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

'Click on the Acusera 247 button on the application menu'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(2)

'Navigate to the "Instrument Configuration" page'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(2)

'Ensure you can UPDATE an Instrument Configuration'
WebUI.doubleClick(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow2'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 3')

'Click on the Manufacturer dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(1)

'Select "Abaxis" from the Manufacturer list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Abaxis'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Click on the "Group" dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(1)

'Select "Piccolo" from the Group list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Piccolo'))

WebUI.delay(1)

'Click on the Model dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(1)

'Select "Piccolo" from the Model list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Piccolo'))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

'Verify message text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigUpdated'), 'Success! Instrument configuration updated', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigUpdated'))

WebUI.delay(2)

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow2'), 'Instrument 3')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow2'), 'Abaxis')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow2'), 'Piccolo')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow2'), 'Piccolo')

WebUI.closeBrowser()

