import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

'Read the users email from the file and save it to the email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users password from the file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Prereqs: Have a participant Registered'
WebUI.openBrowser('')

'Navigate to the STES url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.maximizeWindow()

'Input the users email into the email field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password into the password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

'Click on the Acusera 247 button on the Application menu'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(2)

'Navigate to the "Instrument Configuration" page'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

int six = 6

'Declare 3 arrays. The first one ,"celltext", is going to be the array in which we save the values from each row into'
String[] celltext = new String[six]

String[] expected1 = ['Instrument 1', 'Roche', 'Cobas', 'Cobas c501', '', '']

String[] expected2 = ['Instrument 3', 'Abaxis', 'Piccolo', 'Piccolo', '', '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    String delimiter = '| '

    println(String.join(delimiter, celltext))

    println(String.join(delimiter, expected2))

    if (celltext == expected1) {
        println('This configuration will not be deleted as it is to be used throughout the test suite') //The following code resets the count so that the loop restarts checking each row from the top of the table
    } else {
        Columns_row.get(5).findElement(By.tagName('a')).click()

        WebUI.delay(2)

        WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_NoPopUP'))

        WebUI.delay(2)

        Columns_row.get(5).findElement(By.tagName('a')).click()

        'Click "Yes" on the Pop Up modal to delete Instrument 3'
        WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_YesPopUP'))

        WebUI.delay(1)

        WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'), 'Success! Instrument configuration deleted', 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'))

        rows_table = Table.findElements(By.tagName('tr'))

        row = -1

        rows_count = rows_table.size()
    }
}

WebUI.closeBrowser()

