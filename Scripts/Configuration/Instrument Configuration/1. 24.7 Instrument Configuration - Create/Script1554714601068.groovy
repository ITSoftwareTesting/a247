import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Prereqs: Have a participant Registered'
WebUI.openBrowser('')

'Navigate to STES url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.maximizeWindow()

'Input the users email into the Username field on login page'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the Login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

'Click on the Acusera 24.7 button within the Application menu'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(2)

'Navigate to the "Instrument Configuration" page'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(2)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

if (rows_count > 0) {
    'Loop will execute for all the rows of the table'
    Loop: for (int row = 0; row < rows_count; row++) {
        'Click on Delete Icon'
        WebUI.click(findTestObject('Acusera247/Instrument Configuration/delete_InstrumentRow1'))

        WebUI.delay(1)

        'Click "Yes" on the Pop Up modal to delete the Instrument'
        WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_YesPopUP'))

        WebUI.delay(1)

        WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'))

        WebUI.delay(1)
    }
}

WebUI.delay(1)

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Add New'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 1')

'Click on the Manufacturer dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(1)

'Filter out the list to show manufacturers containing "Roche" '
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_ManufacturerFilter'), 'roche')

WebUI.delay(1)

'Select "Roche" within the dropdown list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Roche'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Click on the "Group" dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(1)

'Select Cobas from the group'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Cobas'))

WebUI.delay(1)

'Click on the Model dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(1)

'Select Cobas 501 from the list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Cobas c501'))

WebUI.delay(1)

'Click the Save button to save the Configuration'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

'Verify Alert text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'), 'Success! Instrument configuration created', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'))

WebUI.delay(1)

'Click on the "Add New" button to Add an additional Instrument'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Add New'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 2')

WebUI.delay(1)

'Click on the Manufacturer dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(1)

'Filter out the list to show manufacturers containing "Roche" '
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_ManufacturerFilter'), 'roche')

WebUI.delay(1)

'Select "Roche" within the dropdown list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Roche'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Click on the "Group" dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(1)

'Select Cobas from the group'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Cobas'))

WebUI.delay(1)

'Click on the Model dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(1)

'Select Cobas 501 from the list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Cobas c501'))

WebUI.delay(1)

'Click the Save button to save the second Instrument Configuration'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'), 'Success! Instrument configuration created', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'))

WebUI.delay(1)

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow1'), 'Instrument 1')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow1'), 'Roche')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow1'), 'Cobas')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow1'), 'Cobas c501')

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow2'), 'Instrument 2')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow2'), 'Roche')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow2'), 'Cobas')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow2'), 'Cobas c501')

WebUI.closeBrowser()

