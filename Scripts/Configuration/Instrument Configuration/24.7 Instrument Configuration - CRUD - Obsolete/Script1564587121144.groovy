import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

'Prereqs: Have a participant Registered'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), 'daveau9@sharklasers.com')

WebUI.setEncryptedText(findTestObject('Acusera247/Login/input_PasswordField'), 'ZM/VR/a+KpRW0CGCfSq1yw==')

WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(2)

'Navigate to the "Instrument Configuration" page'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(2)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

KeywordLogger log = new KeywordLogger()

log.logInfo('Rows = ' + rows_count)

if (rows_count > 0) {
    'Loop will execute for all the rows of the table'
    Loop: for (int row = 0; row < rows_count; row++) {
        'Click on Delete Icon for Instrument 3'
        WebUI.click(findTestObject('Acusera247/Instrument Configuration/delete_InstrumentRow1'))

        WebUI.delay(2)

        'Click "Yes" on the Pop Up modal to delete Instrument 3'
        WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_YesPopUP'))

        WebUI.delay(1)

        WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'))

        WebUI.delay(1 //for
            //if
            )
    }
}

WebUI.delay(2)

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Add New'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 1')

'Click on the Manufacturer dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(2)

'Filter out the list to show manufacturers containing "Roche" '
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_ManufacturerFilter'), 'roche')

WebUI.delay(2)

'Select "Roche" within the dropdown list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Roche'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the "Group" dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(2)

'Select Cobas from the group'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Cobas'))

WebUI.delay(2)

'Click on the Model dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(2)

'Select Cobas 501 from the list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Cobas c501'))

WebUI.delay(2)

'Click the "Clear" button to clear the contents of all the fields'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Clear'))

WebUI.delay(2)

'Ensure the Name field has cleared'
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'value', '', 
    10)

'Ensure the Manufacturer dropdown has cleared'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'), 'Select an item...')

'Ensure the Group dropdown has cleared'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'), 'Select an item...')

'Ensure the Model dropdown has cleared'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'), 'Select an item...')

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(2)

WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 1')

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(2)

WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_ManufacturerFilter'), 'roche')

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Roche'))

WebUI.delay(2)

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Cobas'))

WebUI.delay(2)

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Cobas c501'))

WebUI.delay(2)

'Click the Save button to save the Configuration'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'), 'Success! Instrument configuration created', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'))

WebUI.delay(2)

'Attempt to add the same instrument (Duplication)'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Add New'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 1')

'Click on the Manufacturer dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(2)

'Filter out the list to show manufacturers containing "Roche" '
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_ManufacturerFilter'), 'roche')

WebUI.delay(2)

'Select "Roche" within the dropdown list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Roche'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the "Group" dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(2)

'Select Cobas from the group'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Cobas'))

WebUI.delay(2)

'Click on the Model dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(2)

'Select Cobas 501 from the list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Cobas c501'))

WebUI.delay(2)

'Click the Save button to attempt to save the duplicate configuration'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

'Ensure the validation message displays the correct error text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_ErrorDuplicate'), 'Error A configuration with the same name or model already exists. Please enter a different one and try again.')

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_ErrorDuplicate'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 2')

WebUI.delay(2)

'Click the Save button to attempt to save the duplicate configuration'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'), 'Success! Instrument configuration created', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigCreated'))

WebUI.delay(2)

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow1'), 'Instrument 1')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow1'), 'Roche')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow1'), 'Cobas')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow1'), 'Cobas c501')

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow2'), 'Instrument 2')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow2'), 'Roche')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow2'), 'Cobas')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow2'), 'Cobas c501')

'Ensure you can UPDATE an Instrument Configuration'
WebUI.doubleClick(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow2'))

WebUI.delay(2)

'Enter an Instrument Name'
WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 3')

'Click on the Manufacturer dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(2)

'Select "Abaxis" from the Manufacturer list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Abaxis'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the "Group" dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(2)

'Select "Piccolo" from the Group list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Piccolo'))

WebUI.delay(2)

'Click on the Model dropdown'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(2)

'Select "Piccolo" from the Model list'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Piccolo'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigUpdated'), 'Success! Instrument configuration updated', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigUpdated'))

WebUI.delay(2)

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow2'), 'Instrument 3')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow2'), 'Abaxis')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow2'), 'Piccolo')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow2'), 'Piccolo')

'Click on the Delete Icon within the second row'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/delete_InstrumentRow2'))

WebUI.delay(2)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_NoPopUP'))

WebUI.delay(2)

'Click on Delete Icon for Instrument 3'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/delete_InstrumentRow2'))

WebUI.delay(2)

'Click "Yes" on the Pop Up modal to delete Instrument 3'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_YesPopUP'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'), 'Success! Instrument configuration deleted', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_SuccessConfigDelete'))

WebUI.delay(2)

'verify that the Instrument Name is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_InstrumentNameRow1'), 'Instrument 1')

'verify that the Manufacturer is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ManufacturerRow1'), 'Roche')

'verify that the Group is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_GroupRow1'), 'Cobas')

'verify that the Model is saved to the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/td_ModelRow1'), 'Cobas c501')

rows_count = rows_table.size()

if (rows_count == 1) {
    WebUI.closeBrowser()
}

