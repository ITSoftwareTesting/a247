import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

'Read the users email from the file and save it to email variable'
String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Read the users password from the file and save it to the password variable'
String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Prereqs: Have a participant Registered'
WebUI.openBrowser('')

'Navigate to the STES Url'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.maximizeWindow()

'Input the email into the Email Field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the login button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(2)

'Click on the Acusera 247 button on the application menu'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(2)

'Navigate to the "Instrument Configuration" page'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(2)

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Add New'))

WebUI.delay(2)

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(1)

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(2)

WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'Instrument 1')

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(1)

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'))

WebUI.delay(1)

WebUI.setText(findTestObject('Acusera247/Instrument Configuration/input_ManufacturerFilter'), 'roche')

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Manufacturer_Roche'))

WebUI.delay(2)

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(1)

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Group_Cobas'))

WebUI.delay(1)

'Click Save to get the Validation Alert Message'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(1)

'Verify that the validation message displays the correct text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'), 'Information Please complete all configuration details before saving.', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_InformationCompleteFields'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'))

WebUI.delay(1)

WebUI.click(findTestObject('Acusera247/Instrument Configuration/list_Model_Cobas c501'))

WebUI.delay(1)

'Click the Save button to save the Configuration'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_SaveNEW'))

WebUI.delay(2)

'Ensure the validation message displays the correct error text'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/alert_ErrorDuplicate'), 'Error A configuration with the same name or model already exists. Please enter a different one and try again.')

WebUI.click(findTestObject('Acusera247/Instrument Configuration/alert_ErrorDuplicate'))

WebUI.delay(1)

'Click the "Clear" button to clear the contents of all the fields'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Clear'))

WebUI.delay(1)

'Ensure the Name field has cleared'
WebUI.verifyElementAttributeValue(findTestObject('Acusera247/Instrument Configuration/input_InstrumentName'), 'value', '', 
    10)

'Ensure the Manufacturer dropdown has cleared'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/dropdown_Manufacturer'), 'Select an item...')

'Ensure the Group dropdown has cleared'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/dropdown_Group'), 'Select an item...')

'Ensure the Model dropdown has cleared'
WebUI.verifyElementText(findTestObject('Acusera247/Instrument Configuration/dropdown_Model'), 'Select an item...')

WebUI.closeBrowser()

