import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

'Input the users email address'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the "Log in" button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the Configuration tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Panel" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_PanelConfiguration'))

WebUI.delay(1)

'Ensure the "Automated Panel 1" appears within the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/td_Automated Panel 1 Test'), 'Automated Panel 1')

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Add New'))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Save'))

WebUI.delay(1)

'Verify the Panel name validation message is displayed'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_ValidationInformationMessage'), 'Information Please enter a panel name.')

WebUI.waitForElementNotVisible(findTestObject('Acusera247/Panel Configuration/div_ValidationInformationMessage'), 0)

'Input "Automated Panel 1" into the Panel Name field'
WebUI.setText(findTestObject('Acusera247/Panel Configuration/input_PanelNameTextbox'), 'Automated Panel 1')

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Save'))

WebUI.delay(1)

'Verify the Panel QC Test selection validation message appears'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_ValidationInformationMessage'), 'Information Please select at least one QC test')

WebUI.waitForElementNotVisible(findTestObject('Acusera247/Panel Configuration/div_ValidationInformationMessage'), 0)

WebUI.delay(1)

'Click on the CA 125 row to highlight it'
WebUI.click(findTestObject('Acusera247/Panel Configuration/td_CA 125'))

'Click on the Calcium row to highlight it too'
WebUI.click(findTestObject('Acusera247/Panel Configuration/td_Calcium'))

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Save'))

WebUI.delay(1)

'Verify a duplication information message is displayed'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_ValidationInformationMessage'), 'Information Panel configuration already exists with the same name. Please make changes and try again.')

WebUI.closeBrowser()

