import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

'Enter the Email address within the Email field\r\n'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Enter the password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the "Login" button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Click on the Configuration tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Panel" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_PanelConfiguration'))

WebUI.delay(1)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

if (rows_count > 0) {
    'Loop will execute for all the rows of the table'
    Loop: for (int row = 0; row < rows_count; row++) {
        'Click on Delete Icon'
        WebUI.click(findTestObject('Acusera247/Panel Configuration/span_Delete'))

        WebUI.delay(1)

        WebUI.verifyElementVisible(findTestObject('Acusera247/Panel Configuration/p_DeleteModalText'))

        'Click "No" on the Pop Up modal to delete the Instrument'
        WebUI.click(findTestObject('Acusera247/Panel Configuration/button_No'))

        WebUI.delay(1)

		'Verify the Pop Up Modal is no longer visible'
        WebUI.verifyElementNotVisible(findTestObject('Acusera247/Panel Configuration/p_DeleteModalText'))

        'Click on Delete Icon'
        WebUI.click(findTestObject('Acusera247/Panel Configuration/span_Delete'))

        WebUI.delay(1)

        'Click "Yes" on the Pop Up modal to delete the Instrument'
        WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Yes'))

        WebUI.delay(1)
		
		'Verify a message informing the user the Panel has been deleted appears'
        WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_MessageBanner'), 'Success! Panel configuration deleted')
    }
}

'Ensure No Panels is displayed in the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_EmptyGridView'), 'No panels')

WebUI.closeBrowser()

'Call the Panel Configuration - Add, so we can use the panel further on in the Test Suite'
WebUI.callTestCase(findTestCase('Configuration/Panel Configuration/1. 24.7 Panel Configuration - Add'), [:], FailureHandling.STOP_ON_FAILURE)

