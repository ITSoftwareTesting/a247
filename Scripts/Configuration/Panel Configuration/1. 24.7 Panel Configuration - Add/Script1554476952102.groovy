import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

'Input the Users Email Address'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the Users Password'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the "Login" button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Panel" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_PanelConfiguration'))

WebUI.delay(2)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

if (rows_count > 0) {
    'Loop will execute for all the rows of the table'
    Loop: for (int row = 0; row < rows_count; row++) {
        'Click on Delete Icon'
        WebUI.click(findTestObject('Acusera247/Panel Configuration/span_Delete'))

        WebUI.delay(1)

        'Click "Yes" on the Pop Up modal to delete the Instrument'
        WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Yes'))

        WebUI.delay(1)
    }
}

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Add New'))

WebUI.delay(1)

'Input "Automated Panel 1 Test" into the Panel Name text field'
WebUI.setText(findTestObject('Acusera247/Panel Configuration/input_PanelNameTextbox'), 'Automated Panel 1')

'Click on the CA 125 row'
WebUI.click(findTestObject('Acusera247/Panel Configuration/td_CA 125'))

'Click on the Calcium row'
WebUI.click(findTestObject('Acusera247/Panel Configuration/td_Calcium'))

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Save'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_MessageBanner'), 'Success! Panel configuration has been created.')

'Refresh the page'
WebUI.refresh()

WebUI.delay(1)

'Ensure the panel appears within the grid view and its name has been saved correctly'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/td_Automated Panel 1 Test'), 'Automated Panel 1')

'Ensure the panel appears within the grid view and the nmber of qc tests within the panel is displayed correctly'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/td_NumberOfTestsCell'), '2')

WebUI.closeBrowser()

