import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

'Enter the Email address within the Email field\r\n'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Enter the password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the "Login" button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'), FailureHandling.STOP_ON_FAILURE)

'Click on the Configuration tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Panel" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_PanelConfiguration'))

WebUI.delay(1)

'Double click on the Panel'
WebUI.doubleClick(findTestObject('Acusera247/Panel Configuration/td_Automated Panel 1 Test'))

WebUI.delay(1)

'Click on the CA 125 QC Test to unselect it within the panel'
WebUI.click(findTestObject('Acusera247/Panel Configuration/td_CA 125'))

WebUI.delay(1)

'Edit the Panel name'
WebUI.setText(findTestObject('Acusera247/Panel Configuration/input_PanelNameTextbox'), 'Automated Panel 1 Test Edited')

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Panel Configuration/button_Save'))

WebUI.delay(2)

'Ensure the message displays to say the Panel has been updated.'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/div_MessageBanner'), 'Success! Panel configuration has been updated.')

'Ensure the Edited panel displays in the gridview under its new name'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/td_Automated Panel 1 Test'), 'Automated Panel 1 Test Edited')

'Ensure the panel appears within the grid view and the nmber of qc tests within the panel is displayed correctly'
WebUI.verifyElementText(findTestObject('Acusera247/Panel Configuration/td_NumberOfTestsCell'), '1')

WebUI.closeBrowser()

