import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(5)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the Stats Comparison tab'
WebUI.click(findTestObject('Gillian Objects/Stats Comparison/a_Stats Comparison'))

WebUI.delay(1)

'Double click on the preconfigured QC test '
WebUI.doubleClick(findTestObject('Gillian Objects/Stats Comparison/td_CA 125'))

WebUI.delay(1)

'Select the Method /Instrument Group toggle '
WebUI.click(findTestObject('Gillian Objects/Stats Comparison/span_Method Instrument Group_k-switch-handle km-switch-handle'))

WebUI.delay(1)

'Click on the Save button '
WebUI.click(findTestObject('Gillian Objects/Stats Comparison/button_Save'))

WebUI.delay(3)

'Verify the success banner is displayed when the Stats comparison setting is saved '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Stats Comparison/div_Success Stats comparison saved'), 0)

'Verify the success banner is displayed when the Stats comparison setting is saved '
WebUI.verifyElementText(findTestObject('Gillian Objects/Stats Comparison/div_Success Stats comparison saved'), 'Success! Stats comparison saved')

WebUI.delay(1)

'Click on the expand drop down to show which Stats comparison is saved'
WebUI.click(findTestObject('Gillian Objects/Stats Comparison/a_Closed_k-icon k-i-expand'))

WebUI.delay(1)

'Verify the text displaying the chosen Stats comparison'
WebUI.verifyTextPresent('Method/ Instrument Group', false)

' '
WebUI.delay(5)

WebUI.closeBrowser()

