import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

'Input the users email address into the Username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the "Log in" button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Performace Limit" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_Performance Limit'))

WebUI.delay(1)

'Click on the "Calcium" row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/td_AssayCell'))

WebUI.delay(1)

'Click on the "Next" button'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/button_Next'))

WebUI.delay(1)

'Click on the arrow beside the Assay name to dropdown the child row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/a_expandArrowPLEditPage'))

WebUI.delay(1)

'Double click on the Lot row within the child row'
WebUI.doubleClick(findTestObject('Acusera247/Performance Limit Configuration/td_LotCell'))

WebUI.delay(3)

'Double Click on the PL Type field to open the dropdown list'
WebUI.doubleClick(findTestObject('Acusera247/Performance Limit Configuration/td_Biological variation'))

WebUI.delay(1)

'Select User Defined from the list'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/li_User defined'))

WebUI.delay(1)

'Click on the Concentration High text box to make it editable.'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/td_concentrationHigh'))

WebUI.delay(2)

'Double click on the up arrow within the Concentration high numeric field to input the value "2"'
WebUI.doubleClick(findTestObject('Acusera247/Performance Limit Configuration/span_highConcentrationArrowUp'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Double Click on the "Status" field to open the drop down list'
WebUI.doubleClick(findTestObject('Acusera247/Performance Limit Configuration/td_Inactive'))

WebUI.delay(1)

'Select "Alert" from the dropdown list'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/li_Alert'))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/button_Save'))

WebUI.delay(1)

'Verify the Success message is displayed'
WebUI.verifyTextPresent('Success! Performance limit updated', false)

'Refresh the page'
WebUI.refresh()

WebUI.delay(1)

'Click on the expand arrow within the Calcium row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/a_expand2ndRowPLConfigPage'))

WebUI.delay(1)

'Click on the Performance Limit tab within the child row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/span_PerformanceLimitChildTab'))

WebUI.delay(1)

//'Creates the date format we want to use, this can be changed to different formats'
//SimpleDateFormat dateFormatter = new SimpleDateFormat('dd-MMM-yyyy hh:mm:ss')
//'Saves the formatted date to a variable'
//currentDate = dateFormatter.format(now)

int arrayLength = 10
'Declare an array "celltext", this is going to be the array in which we save the values from each row into'
String[] celltext = new String[arrayLength]

'Declare an array which hold the expected text from the grid view'
String[] expected1 = ['00125-PL', 'User defined', 'g/l', '--', '--', '2', '--', '--', '--', '--']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//*[@id="QCTestGrid"]/table/tbody/tr[3]/td/div/div/div/div/div/table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size() - 1

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    println(Arrays.toString(celltext))

    println(Arrays.toString(expected1))
}

'Verify that the cell values equal what you expect'
WebUI.verifyEqual(Arrays.toString(celltext), Arrays.toString(celltext))

'Close the browser'
WebUI.closeBrowser()

