import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/login?RememberMe=False&SelectionIsEmpty=True')

'Input the users email address into the Username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the users password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the "Log in" button'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Performace Limit" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_Performance Limit'))

WebUI.delay(1)

'Click on the "Calcium" row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/td_AssayCell'))

WebUI.delay(1)

'Click on the "Next" button'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/button_Next'))

WebUI.delay(1)

'Click on the arrow beside the Assay name to dropdown the child row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/a_expandArrowPLEditPage'))

WebUI.delay(1)

'Double click on the Lot row within the child row'
WebUI.doubleClick(findTestObject('Acusera247/Performance Limit Configuration/td_LotCell'))

WebUI.delay(1)

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//*[@id="pfHistoryGrid"]/table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    Columns_row.get(11).findElement(By.tagName('a')).click()

    WebUI.delay(1)

    'Click on the "No" button within the Pop up Modal'
    WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/button_No'))

    WebUI.delay(1)

    Columns_row.get(11).findElement(By.tagName('a')).click()

    'Click on the "Yes" button within the Pop up Modal'
    WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/button_Yes'))

    WebUI.delay(1)

    //The following code resets the count so that the loop restarts checking from the first row in the table
    rows_table = Table.findElements(By.tagName('tr'))

    row = -1

    rows_count = rows_table.size()
}

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/button_Save'))

WebUI.delay(1)

'Verify the "Success" message is displayed'
WebUI.verifyTextPresent('Success! Performance limit updated', false)

WebUI.delay(1)

'Refresh the page'
WebUI.refresh()

WebUI.delay(1)

'Click on the expand arrow within the Calcium row'
WebUI.click(findTestObject('Acusera247/Performance Limit Configuration/a_expand2ndRowPLConfigPage'))

WebUI.delay(1)

'Verify the Performance limit tab within the child row is not visible, this confirms that a performance limit is not present on the QC Test'
WebUI.verifyElementNotVisible(findTestObject('Acusera247/Performance Limit Configuration/span_PerformanceLimitChildTab'))

WebUI.closeBrowser()

