import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Navigate to stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

'Input the Participants email into the username field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the participants password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(2)

'Click on the "Assay" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

WebUI.delay(2)

'Declare an integer which will hold the value of the arrays length'
int eight = 8

'Declare 4 arrays. The first one ,"celltext", is going to be the array in which we save the values from each row into'
String[] celltext = new String[eight]

String[] expected1 = ['Calcium', 'Clinical Chemistry', 'Arsenazo', 'g/l', 'Roche', '', '', '']

String[] expected2 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'U/ml', 'Roche', '', '', '']

String[] expected3 = ['Chloride', 'Blood Gas', 'Colorimetric', 'mmol/l', 'Randox Laboratories', '', '', '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        (celltext[column]) = Columns_row.get(column).getText()
    }
    
    String delimiter = '| '

    println(String.join(delimiter, celltext))

    println(String.join(delimiter, expected3))

    if ((celltext == expected1) || (celltext == expected2)) {
        println('This configuration will not be deleted as it is to be used in the QC Test Configuration') 
    } else {
        Columns_row.get(7).findElement(By.tagName('a')).click()

        WebUI.delay(1)

        'Click on the "Yes" button within the Pop up Modal'
        WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Yes'))

        WebUI.delay(1)

        'Verify that the Delete Success message is displayed'
        WebUI.verifyTextPresent('Success! Assay configuration deleted.', false)

        rows_table = Table.findElements(By.tagName('tr'))

		//The following code resets the count so that the loop restarts checking each row from the top of the table
        row = -1

        rows_count = rows_table.size()
    }
}

WebUI.closeBrowser()

