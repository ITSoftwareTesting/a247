import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.maximizeWindow()

'Navigate to Stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

'Enter the users email address'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Enter the users password'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Assay" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

WebUI.delay(1)

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Add New'))

WebUI.delay(1)

'Click on the "Save" button, this will promt the validation message to show'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Validation alert message is displayed'
WebUI.verifyTextPresent('Information Please complete all fields', false)

WebUI.delay(1)

'Click on the validation message to close it'
WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

WebUI.delay(1)

'Click on the Analyte dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Analyte'))

WebUI.delay(1)

//Call the driverfactory library'
WebDriver driver = DriverFactory.getWebDriver()

List analyteList

List chemList

WebUI.setText(findTestObject('Acusera247/Assay Configuration/input_AnalyteFilter'), 'cal')

WebUI.delay(1)

'Save the list within the Analyte dropdown into the analyteList array'
analyteList = driver.findElements(By.xpath('//ul[@id=\'analyte_listbox\']/li'))

'Create a foreach loop to check each item in the list'
for (def item : analyteList) {
    'create an if to check if any of the items in the list equal, in this case "Albumin", you can change this to any list it you want.'
    if (item.getText().toString() == 'Calcium') {
        'Modify the text property of the Test Object so that it is pointing towards the item you selected from the list'
        analyteObject = WebUI.modifyObjectProperty(findTestObject('Acusera247/Assay Configuration/list_Analyte_CA 125'), 
            'text', 'equals', item.getText().toString(), true)

        'Click on modified object'
        WebUI.click(analyteObject)

        'break out of the loop'
        break
    }
}

WebUI.delay(1)

'Click on the Chemistry Type dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_ChemistryType'))

WebUI.delay(1)

'Select Clinical Chemistry from the dropdown list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_ChemType_ClinChem'))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Validation alert message is displayed'
WebUI.verifyTextPresent('Information Please complete all fields', false)

WebUI.delay(1)

'Click on the validation message to close it'
WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

WebUI.delay(1)

'Click on the Method dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Method'))

WebUI.delay(1)

'Click on "Arsenazo" within the method list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Method_Arsenazo'))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Validation alert message is displayed'
WebUI.verifyTextPresent('Information Please complete all fields', false)

WebUI.delay(1)

'Click on the validation message to close it'
WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

WebUI.delay(1)

'Click on the "Unit" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Unit'))

WebUI.delay(1)

'Select "g/l" from dropdown list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Unit_gl'))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Validation alert message is displayed'
WebUI.verifyTextPresent('Information Please complete all fields', false)

WebUI.delay(1)

'Click on the validation message to close it'
WebUI.click(findTestObject('Acusera247/Assay Configuration/alert_Validation'))

WebUI.delay(1)

WebUI.closeBrowser()

