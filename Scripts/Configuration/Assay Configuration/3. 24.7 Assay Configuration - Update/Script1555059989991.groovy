import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Assay" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

WebUI.delay(2)

'Double click on the third row within the Assay table, in this case it is the Albumin Row'
WebUI.doubleClick(findTestObject('Acusera247/Assay Configuration/td_AnalyteRow3'))

WebUI.delay(1)

'Click on the Dropdown to get the list to appear'

'Click on the Anayte Dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Analyte'))

WebUI.delay(1)

'Call the driverfactory library'
WebDriver driver = DriverFactory.getWebDriver()

List analyteList

'Save the list within the Analyte dropdown into the analyteList array'
analyteList = driver.findElements(By.xpath('//ul[@id=\'analyte_listbox\']/li'))

'Create a foreach loop to check each item in the list'
for (def item : analyteList) {
    println(item.getText().toString())

    'create an if to check if any of the items in the list equal, in this case "Albumin", you can change this to any list it you want.'
    if (item.getText().toString() == 'Chloride') {
        'Modify the text property of the Test Object so that it is pointing towards the item you selected from the list'
        analyteObject = WebUI.modifyObjectProperty(findTestObject('Acusera247/Assay Configuration/list_Analyte_CA 125'), 
            'text', 'equals', item.getText().toString(), true)

        'Click on modified object'
        WebUI.click(analyteObject)

        'break out of the loop'
        break
    }
}

WebUI.delay(1)

'Click on the "Chemistry Type" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_ChemistryType'))

WebUI.delay(1)

'Select "Blood Gas" from the list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_Blood Gas'))

WebUI.delay(1)

'Click on the "Method" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Method'))

WebUI.delay(1)

'Select "Colormetric" from the list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_Colorimetric'))

WebUI.delay(1)

'Click on the "Unit" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Unit'))

WebUI.delay(1)

'Select "mmol/l" from the list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_mmoll'))

WebUI.delay(1)

'Click on the "Reagent Supplier" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Reagent Supplier'))

WebUI.delay(1)

'Select "Randox Laboratories" from the list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_Randox Laboratories'))

WebUI.delay(1)

'Click on "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify that the Update Success message is displayed'
WebUI.verifyTextPresent('Success! Assay configuration has been updated.', false)

WebUI.delay(1)

'Refresh the page, then verify that chloride is in the grid view\r\n'
WebUI.refresh()

WebUI.delay(1)

'Ensure Chloride in now displayed as the 3rd Analyte'
WebUI.verifyElementText(findTestObject('Acusera247/Assay Configuration/td_AnalyteRow3'), 'Chloride')

WebUI.closeBrowser()

