import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

WebUI.maximizeWindow()

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Navigate to stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "Assay" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

WebUI.delay(1)

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Add New'))

WebUI.delay(1)

'Click on the "Analyte" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Analyte'))

WebUI.delay(1)

//Call the driverfactory library'
WebDriver driver = DriverFactory.getWebDriver()

List analyteList

WebUI.setText(findTestObject('Acusera247/Assay Configuration/input_AnalyteFilter'), 'cal')

WebUI.delay(1)

'Save the list within the Analyte dropdown into the analyteList array'
analyteList = driver.findElements(By.xpath('//ul[@id=\'analyte_listbox\']/li'))

'Create a foreach loop to check each item in the list'
for (def item : analyteList) {
    'create an if to check if any of the items in the list equal, in this case "Calcium", you can change this to any list it you want.'
    if (item.getText().toString() == 'Calcium') {
        'Modify the text property of the Test Object so that it is pointing towards the item you selected from the list'
        analyteObject = WebUI.modifyObjectProperty(findTestObject('Acusera247/Assay Configuration/list_Analyte_CA 125'), 
            'text', 'equals', item.getText().toString(), true)

        'Click on modified object'
        WebUI.click(analyteObject)

        'break out of the loop'
        break
    }
}

WebUI.delay(1)

'Click on the "Chemistry Type" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_ChemistryType'))

WebUI.delay(1)

'Select "Clinical Chemistry" in the Chemistry Type list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_ChemType_ClinChem'))

WebUI.delay(1)

'Click on the Method Dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Method'))

WebUI.delay(1)

'Select "Arsenazo" from the Method list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Method_Arsenazo'))

WebUI.delay(1)

'Click on the "Unit" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Unit'))

WebUI.delay(1)

'Select "g/l" from the Unit list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Unit_gl'))

WebUI.delay(1)

'Click on the "Reagent Supplier" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Reagent Supplier'))

WebUI.delay(1)

'Type "ro" into the Reagent Supplier input field'
WebUI.setText(findTestObject('Acusera247/Assay Configuration/input_ReagentSupplier1'), 'ro')

WebUI.delay(2)

'Select "Roche" from the filtered out Reagent Supplier list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_ReagentSupplier_Roche'))

WebUI.delay(2)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Success message is displayed'
WebUI.verifyTextPresent('Success! Assay configuration created.', false)

WebUI.delay(1)

'Refresh the page'
WebUI.refresh()

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Add New'))

WebUI.delay(1)

'Click on the Dropdown to get the list to appear'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Analyte'))

WebUI.delay(1)

'Save the list within the Analyte dropdown into the analyteList array'
analyteList = driver.findElements(By.xpath('//ul[@id=\'analyte_listbox\']/li'))

'Create a foreach loop to check each item in the list'
for (def item : analyteList) {
    'create an if to check if any of the items in the list equal, in this case "CA 125", you can change this to any list it you want.'
    if (item.getText().toString() == 'CA 125') {
        'Modify the text property of the Test Object so that it is pointing towards the item you selected from the list'
        analyteObject = WebUI.modifyObjectProperty(findTestObject('Acusera247/Assay Configuration/list_Analyte_CA 125'), 
            'text', 'equals', item.getText().toString(), true)

        'Click on modified object'
        WebUI.click(analyteObject)

        'break out of the loop'
        break
    }
}

WebUI.delay(1)

'Click on the "Chemistry Type" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_ChemistryType'))

WebUI.delay(1)

'Select "Immunoassay" from the Chemistry Type list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Method_Immunoassay'))

WebUI.delay(1)

'Click on the "Method" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Method'))

WebUI.delay(1)

'Select "CLIA" from the Method list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Method_CLIA'))

WebUI.delay(1)

'Click on "Unit" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Unit'))

WebUI.delay(1)

'Select "U/ml" from the Unit list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_Unit_Uml'))

WebUI.delay(1)

'Click on the "Reagent Supplier" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Reagent Supplier'))

WebUI.delay(1)

'Input "Roche" into the Reagent Supplier filter'
WebUI.setText(findTestObject('Acusera247/Assay Configuration/input_ReagentSupplier2'), 'roche')

WebUI.delay(1)

'Select "Roche" from the filtered list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_ReagentSupplier_Roche - Copy'))

WebUI.delay(1)

'Click the "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Success message is displayed'
WebUI.verifyTextPresent('Success! Assay configuration created.', false)

WebUI.delay(1)

'Refresh the page - ADD A THIRD ASSAY - This will be the one we update and delete'
WebUI.refresh()

'Click on the "Add New" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Add New'))

WebUI.delay(1)

'Click on the Dropdown to get the list to appear'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Analyte'))

WebUI.delay(1)

'Save the list within the Analyte dropdown into the analyteList array'
analyteList = driver.findElements(By.xpath('//ul[@id=\'analyte_listbox\']/li'))

'Create a foreach loop to check each item in the list'
for (def item : analyteList) {
    'create an if to check if any of the items in the list equal, in this case "Albumin", you can change this to any list it you want.'
    if (item.getText().toString() == 'Albumin') {
        'Modify the text property of the Test Object so that it is pointing towards the item you selected from the list'
        analyteObject = WebUI.modifyObjectProperty(findTestObject('Acusera247/Assay Configuration/list_Analyte_CA 125'), 
            'text', 'equals', item.getText().toString(), true)

        'Click on modified object'
        WebUI.click(analyteObject)

        'break out of the loop'
        break
    }
}

WebUI.delay(1)

'Click on the "Chemistry Type" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_ChemistryType'))

WebUI.delay(1)

'Select "Clinical Chemistry" from the Chemistry Type list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_Clinical Chemistry'))

WebUI.delay(1)

'Click on the "Method" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Method'))

WebUI.delay(1)

'Select "Bromocresol Purple" from the Method list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_Bromocresol Purple'))

WebUI.delay(1)

'Click on "Unit" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Unit'))

WebUI.delay(1)

'Select "U/ml" from the Unit list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/li_gdl'))

WebUI.delay(1)

'Click on the "Reagent Supplier" dropdown'
WebUI.click(findTestObject('Acusera247/Assay Configuration/dropdown_Reagent Supplier'))

WebUI.delay(1)

'Input "Roche" into the Reagent Supplier filter'
WebUI.setText(findTestObject('Acusera247/Assay Configuration/input_ReagentSupplier2'), 'roche')

WebUI.delay(1)

'Select "Roche" from the filtered list'
WebUI.click(findTestObject('Acusera247/Assay Configuration/list_ReagentSupplier_Roche - Copy'))

WebUI.delay(1)

'Click the "Save" button'
WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Save'))

WebUI.delay(1)

'Verify Success message is displayed'
WebUI.verifyTextPresent('Success! Assay configuration created.', false)

'Verify that "Calcium" is displayed as the first Analyte in the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Assay Configuration/td_AnalyteRow1'), 'Calcium')

'Verify that "CA 125" is displayed as the second Analyte in the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Assay Configuration/td_AnalyteRow2'), 'CA 125')

'Verify that "Albumin" is displayed as the third Analyte in the grid view'
WebUI.verifyElementText(findTestObject('Acusera247/Assay Configuration/td_AnalyteRow3'), 'Albumin')

WebUI.closeBrowser()

