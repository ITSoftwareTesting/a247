import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(5)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the targets tab'
WebUI.click(findTestObject('Gillian Objects/Targets/a_Targets'))

WebUI.delay(1)

'Double click on the QC Test'
WebUI.doubleClick(findTestObject('Gillian Objects/Targets/td_CA 125'))

WebUI.delay(1)

'Click on the expand button'
WebUI.click(findTestObject('Gillian Objects/Targets/a_Next_k-icon k-i-expand'))

WebUI.delay(1)

'Double click on the lot 1304'
WebUI.doubleClick(findTestObject('Gillian Objects/Targets/td_1304EC'))

WebUI.delay(1)

'Click on the Add new target button'
WebUI.click(findTestObject('Gillian Objects/Targets/a_Add New Target'))

WebUI.delay(1)

'Click on the Type drop down '
WebUI.click(findTestObject('Gillian Objects/Targets/td_Cumulative'))

WebUI.delay(2)

'Click on the drop down arrow'
WebUI.click(findTestObject('Gillian Objects/Targets/span_Cumulative_k-icon k-i-arrow-60-down_1'))

WebUI.delay(1)

'Select Fixed'
WebUI.click(findTestObject('Gillian Objects/Targets/li_Fixed'))

WebUI.delay(1)

'Click on the Mean entry box '
WebUI.click(findTestObject('Gillian Objects/Targets/td_--'))

WebUI.delay(2)

'Input the number 22 within the Mean entry box '
WebUI.sendKeys(findTestObject('Gillian Objects/Targets/input_Fixed_Mean'), '22')

WebUI.delay(1)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/Targets/button_Save'))

WebUI.delay(1)

'Verify the success message is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Targets/div_Information Please insert a valid SD for fixed target(s)'), 
    0)

WebUI.delay(1)

'Click on the SD entry box '
WebUI.click(findTestObject('Gillian Objects/Targets/td_2--SD'))

WebUI.delay(2)

'Input 3 '
WebUI.sendKeys(findTestObject('Gillian Objects/Targets/input_Fixed_SD'), '3')

WebUI.delay(1)

'Click on the save button '
WebUI.click(findTestObject('Gillian Objects/Targets/button_Save'))

WebUI.delay(2)

'Verify that the success message is displayed '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Targets/div_Information Target assigned'), 0)

WebUI.delay(2)

'Click on the drop down arrow beside the QC Test'
WebUI.click(findTestObject('Gillian Objects/Targets/a_Next_k-icon k-i-expand'))

'Verify the lot number 1304EC'
not_run: WebUI.verifyElementText(findTestObject('Gillian Objects/Targets/td_1304EC'), '1304EC')

'Verify the Type is Fixed '
WebUI.verifyElementText(findTestObject('Gillian Objects/Targets/td_Fixed'), 'Fixed')

'Verify the mean is 22'
WebUI.verifyElementText(findTestObject('Gillian Objects/Targets/td_22'), '22')

'Veridy the SD is 3'
WebUI.verifyElementText(findTestObject('Gillian Objects/Targets/td_3'), '3')

WebUI.delay(5)

WebUI.closeBrowser()

