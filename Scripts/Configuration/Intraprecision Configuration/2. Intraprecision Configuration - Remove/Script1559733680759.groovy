import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.openBrowser('')

WebUI.maximizeWindow()

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Navigate to stes'
WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

WebUI.delay(1)

'Input the participants email address into the email field'
WebUI.setText(findTestObject('Acusera247/Login/input_UserNameField'), email)

'Input the participants password into the Password field'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Log in to the application as a participant'
WebUI.click(findTestObject('Acusera247/Login/button_Log in'))

WebUI.delay(1)

'Click on the Acusera 24.7 application'
WebUI.click(findTestObject('Acusera247/Home Page/button_Acusera24.7'))

WebUI.delay(1)

'Click on the "Configuration" tile'
WebUI.click(findTestObject('Acusera247/Home Page/button_ConfgurationTile'))

WebUI.delay(1)

'Click on the "IntraPrecision" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/tab_Intraprecision'))

WebUI.delay(1)

int twelve = 12

'Declare 4 arrays. The first one , "celltext", is going to be the array in which we save the values from each row into'
String[] celltext = new String[twelve]

String[] expected1 = ['', '', 'Calcium', 'Clinical Chemistry', 'Arsenazo', 'g/l', 'Instrument 1', '00125-PL', '', '', ''
	, '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//*[@id="QCTestGrid"]/table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
	'To locate columns(cells) of that specific row'
	List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

	'To calculate no of columns(cells) In that specific row'
	int columns_count = Columns_row.size()

	'Loop will execute till the last cell of that specific row'
	for (int column = 0; column < columns_count; column++) {
		'It will retrieve text from each cell'
		(celltext[column]) = Columns_row.get(column).getText()
	}
	
	String delimiter = '| '

	println(String.join(delimiter, celltext))

	println(String.join(delimiter, expected1))

	if (celltext == expected1) {
		println('This configuration we will update its intraprecision')

		Columns_row.get(3).click()
	}
}

WebUI.delay(1)

'Click on the "Next" button'
WebUI.click(findTestObject('Acusera247/Intraprecision Configuration/button_Next'))

WebUI.delay(1)

'Double click on the Type field to open the dropdown list'
WebUI.doubleClick(findTestObject('Acusera247/Intraprecision Configuration/td_Type'))

WebUI.delay(1)

'Select "SD" within the dropdown'
WebUI.click(findTestObject('Acusera247/Intraprecision Configuration/li_SD'))

'Double Click on the Intraprecision input field to make it editable'
WebUI.doubleClick(findTestObject('Acusera247/Intraprecision Configuration/td_InstraprecisionValueField'))

WebUI.delay(1)

'Type "0" into the Intraprecision value field'
WebUI.sendKeys(findTestObject('Acusera247/Intraprecision Configuration/input_InstraprecisionValue'), Keys.chord(Keys.NUMPAD0))

WebUI.delay(1)

'Click on the "Save" button'
WebUI.click(findTestObject('Acusera247/Intraprecision Configuration/button_Save'))

WebUI.delay(1)

'Verify the Success message has displayed after saving the new configuration'
WebUI.verifyTextPresent('Success! Intra Precision values have been saved.', false)

WebUI.delay(1)

'Refresh the page'
WebUI.refresh()

WebUI.delay(2)

'Click on the child row dropdown arrow on the calcium row'
WebUI.click(findTestObject('Acusera247/Intraprecision Configuration/a_ChildRowDropdownArrow'))

WebUI.delay(1)

'Verify that "No Intraprecisions" appears within the calcium tests child row'
WebUI.verifyElementVisible(findTestObject('Acusera247/Intraprecision Configuration/div_No Intraprecisions'))

WebUI.closeBrowser()