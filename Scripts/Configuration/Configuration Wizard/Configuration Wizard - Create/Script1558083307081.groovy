import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the Configuration Wizard tab'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/a_Configuration Wizard'))

WebUI.delay(1)

'Click on the Manufacturer drop down entry box'
WebUI.click(findTestObject('Gillian Objects/Events/span_Instrument Configuration_k-select'))

WebUI.delay(1)

'Enter Manufacturer Abaxis '
WebUI.setText(findTestObject('Gillian Objects/Events/input_Instrument Configuration_k-input'), 'Abaxis')

'Send the enter key '
WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.delay(2)

'Click on the Instrument Group dropdown list '
WebUI.click(findTestObject('Gillian Objects/Events/span_Instrument Configuration_k-icon k-i-arrow-60-down_1'))

'Enter the Group Piccolo'
WebUI.setText(findTestObject('Gillian Objects/Configuration Wizard/input_InstrumentGroup'), 'Piccolo')

'Send the enter keys'
WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.delay(2)

'Click on the Instrument Model drop down list '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_InstrumentModelDropdown'))

'Enter the model Piccolo'
WebUI.setText(findTestObject('Gillian Objects/Configuration Wizard/input_InstrumentModelInput'), 'Piccolo')

'Send the enter key'
WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.delay(2)

'Set the Text within the Instrument name field to "Config Wizard Instrument" '
WebUI.setText(findTestObject('Gillian Objects/Events/input_Instrument Configuration_InstrumentInput'), 'Config Wizard Instrument')

WebUI.delay(2)

'Click on the Toggle to activate ALT (GPT)'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_--_k-switch-handle km-switch-handle'))

'Click on the Chemistry type entry box '
WebUI.click(findTestObject('Gillian Objects/Events/td_--'))

WebUI.delay(2)

'Click on Clinical Chemistry '
WebUI.click(findTestObject('Gillian Objects/Events/li_Clinical Chemistry'))

WebUI.delay(2)

'Click on the Method cell'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Method'))

WebUI.delay(2)

'Select Colorimetric '
WebUI.setText(findTestObject('Gillian Objects/Configuration Wizard/li_Colorimetric'), 'Colorimetric')

'Send the enter key'
WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.delay(2)

'Select the unit dropdown '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_UnitDropdown'))

WebUI.delay(2)

'Select mU/ml'
WebUI.click(findTestObject('Gillian Objects/Events/li_gdl'))

WebUI.delay(2)

'Select the temperature drop down '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_TempCell'))

WebUI.delay(1)

'Select the value 37'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_37'))

WebUI.delay(1)

'Select the Reagent supplier cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_SupplierDropdown'))

WebUI.delay(2)

'Select Abaxis from the drop down'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Abaxis'))

WebUI.delay(1)

'Select Lot 1 cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Lot 1'))

WebUI.delay(1)

'Select 1006UN'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1006UN'))

WebUI.delay(1)

'Select Lot 2 cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Lot2'))

WebUI.delay(1)

'Select 1007UN from the drop down list'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1007UN'))

WebUI.delay(5)

'Click on the Toggle to activate Albumin'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_--_k-switch-handleAlbumin'))

WebUI.delay(1)

'Click on the Chemistry type entry box '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_CTalb'))

WebUI.delay(2)

'Click on Clinical Chemistry '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Clinical ChemistryALB'))

WebUI.delay(2)

'Click on the Method cell'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_MethodAlb'))

WebUI.delay(2)

'Select Bromocresol Purple'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Bromocresol Purple'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

'Select the unit dropdown '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_UnitAlb'))

WebUI.delay(2)

'Select g/l'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_gl_alb'))

WebUI.delay(2)

'Select the Reagent supplier cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_RSalb'))

WebUI.delay(2)

'Select Abaxis from the drop down'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_AbaxisAlb'))

WebUI.delay(1)

'Select Lot 1 cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Lot1alb'))

WebUI.delay(1)

'Select 1008UN'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1008UNalb'))

WebUI.delay(1)

'Select Lot 2 cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Lot2alb'))

WebUI.delay(1)

'Select 1010UN from the drop down list'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1010UNalb'))

WebUI.delay(1)

'Select Lot 3 cell to make it active'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Lot3alb'))

WebUI.delay(1)

'Select 1026UN from the drop down list'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1026UNalb'))

WebUI.delay(2)

'Click on the next button '
WebUI.click(findTestObject('Gillian Objects/Events/button_Next'))

'END OF QC TEST CONFIGURATION - STEP 1'
WebUI.delay(3)

'Click on the drop down arrow beside the Instrument information '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_Uses Reagent Supplier_k-switch-handle km-switch-handle'))

WebUI.delay(1)

'Select the "Uses Reagent Supplier" toggle to make it active '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Step 2 Stats Comparison_k-hierarchy-cell'))

WebUI.delay(5)

'Click on the drop down arrow beside the Instrument information for Albumin'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_StatsAlbuminDropdown'))

WebUI.delay(1)

'Select the "Uses Instrument Group" toggle to make it active '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_Stats_Instrument Group_Albumin'))

WebUI.delay(5)

'Click on the Next button '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/button_Next'))

'END OF STATS COMPARISON CONFIGURATION - STEP 2'
WebUI.delay(3)

'Select the dropdown arrow beside the configured QC Test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Step 3 Performance LimitsDropdown'))

WebUI.delay(1)

'Select the Type cell for Lot 1006UN'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1006UNtype'))

WebUI.delay(1)

'Select Biological variation'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Biological variation1006UN'))

WebUI.delay(1)

'Verify that the Low percentage value is 11.48 as expected'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1006low'), '11.48')

'Verify that the High percentage value is 14.9 as expected'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1006high'), '27.48')

WebUI.delay(2)

'Click on the Type cell for 1007UN to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1007UNtype'))

WebUI.delay(2)

'Select Riqas'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Clia1007un'))

WebUI.delay(1)

'Verify that the Low percentage value is -- as expected'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1007low'), '--')

'Verify that the high percentage value is 14.9 as expected'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1007high'), '14.9')

WebUI.delay(2)

'Select the dropdown arrow beside the configured QC Test for Albumin'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_-PLalb_dropdown'))

WebUI.delay(1)

'Select the Type cell for Lot 1008UN'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1008Type'))

WebUI.delay(1)

'Select Biological variation'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Biological variation1008'))

WebUI.delay(1)

'Verify that the Low percentage value is 1.43 as expected'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1008low'), '1.43')

'Verify that the High percentage value is 4.07 as expected'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1008high'), '4.07')

WebUI.delay(2)

'Click on the next button '
WebUI.click(findTestObject('Gillian Objects/Events/button_Next'))

'END OF PERFORMANCE LIMITS CONFIGURATION - STEP 3'
WebUI.delay(5)

'Click on the drop down arrow beside the configured QC test '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Step 4 Targets_dropdown'))

WebUI.delay(1)

'Click on Type cell for 1006un'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1006TargetType'))

WebUI.delay(1)

'Select Cumulative'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1006UNCumulative'))

'Verify that the Mean is displayed as --'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1006MeanValue--'), '--')

'Verify that the SD is displayed as --'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1006UNSDvalue--'), '--')

WebUI.delay(1)

'Click on Type cell for 1007un'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1007UNTargetType'))

WebUI.delay(1)

'Select Fixed from the drop down list '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_1007UNFixed'))

WebUI.delay(1)

'Click on the Mean cell for 1007UN to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1006UNMean'))

WebUI.delay(1)

'Click on the up arrow repeatedly so the value is 12'
WebUI.doubleClick(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSDuparrow-60-up'))

WebUI.doubleClick(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSDuparrow-60-up'))

WebUI.doubleClick(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSDuparrow-60-up'))

WebUI.doubleClick(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSDuparrow-60-up'))

WebUI.doubleClick(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSDuparrow-60-up'))

WebUI.doubleClick(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSDuparrow-60-up'))

WebUI.delay(1)

'Click on the SD cell for 1007UN to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1007UNSD'))

WebUI.delay(1)

'Click on the Up arrow 3 times '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSD1007un_k-icon k-i-arrow-60-up'))

WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSD1007un_k-icon k-i-arrow-60-up'))

WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/span_FixedSD1007un_k-icon k-i-arrow-60-up'))

WebUI.delay(3)

'Click on the drop down arrow beside the configured QC test '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_-TargetsAlb_dropdown'))

WebUI.delay(1)

'Click on Type cell for 1010un'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_1010UNType'))

WebUI.delay(1)

'Select Cumulative'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Cumulative1010UN'))

'Verify that the Mean is displayed as --'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1006MeanValue--'), '--')

'Verify that the SD is displayed as --'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/td_1006UNSDvalue--'), '--')

WebUI.delay(1)

'Click on the next button '
WebUI.click(findTestObject('Gillian Objects/Events/button_Next'))

'END OF TARGETS CONFIGURATION - STEP 4'
WebUI.delay(5)

'Click on the arrow beside the configured QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_Final Step Multi-rules_k-hierarchy-cell'))

WebUI.delay(2)

'Click on the multirule cell to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_multiruleCell'))

WebUI.delay(1)

'Select 10X'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_10x'))

WebUI.delay(1)

'Click on the Run Type cell to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_RunTypeCell'))

WebUI.delay(1)

'Select "Across Run"'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Across Run'))

WebUI.delay(1)

'Click on the Status cell to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_StatusCell'))

WebUI.delay(1)

'Select "Reject"'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_Reject'))

WebUI.delay(2)

'Click on the arrow beside the configured QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_-MRalb_dropdown'))

WebUI.delay(2)

'Click on the multirule cell to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_albMultirulecell'))

WebUI.delay(1)

'Select 2:2s'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_22sAlb'))

WebUI.delay(1)

'Click on the Run Type cell to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_RunTypeAlb'))

WebUI.delay(1)

'Select "Both"'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_BothAlb'))

WebUI.delay(1)

'Click on the Status cell to activate it '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/td_InactiveAlb'))

WebUI.delay(1)

'Select "Alert"'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/li_AlertAlb'))

WebUI.delay(5)

'Click on the Finish button '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/button_Finish'))

WebUI.delay(2)

'Click No on the pop up box which asks if you wish to delete'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/button_No'))

'Verify banner appears to say Save cancelled'
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Configuration Wizard/div_Information Save Cancelled'), 0)

'Verify the text within the banner '
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/div_Information Save Cancelled'), 'Information Save Cancelled')

WebUI.delay(5)

'Click on the Finish button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/button_Finish'))

WebUI.delay(2)

'Click on the Yes button within the drop down banner '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/button_Yes'))

'Verify that the success banner appears '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Configuration Wizard/div_Success Test(s) saved'), 0)

'Verify the text within the success banner '
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/div_Success Test(s) saved'), 'Success! Test(s) saved')

WebUI.closeBrowser()

