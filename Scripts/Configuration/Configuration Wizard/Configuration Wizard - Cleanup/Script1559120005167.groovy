import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String email = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 3, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

String password = CustomKeywords.'readAndWriteToExcel.ReadFromExcel.readFromExcel'(0, 4, 1, 'G:\\Acusera247\\Testing\\Katalon Automation Suites\\Katalon Studio\\Acusera 247\\Data Files\\User Login Data.xlsx')

'Needs to be ran within the recorder as not working just when click run\r\n'
WebUI.openBrowser('')

WebUI.navigateToUrl('http://randoxqcv2-stes.azurewebsites.net/Account/Login?RememberMe=False&SelectionIsEmpty=True')

'Maximise window'
WebUI.maximizeWindow()

WebUI.delay(2)

'Enter username'
WebUI.setText(findTestObject('Gillian Objects/Log in/input_Use your account details'), email)

'Enter password from file'
WebUI.setMaskedText(findTestObject('Acusera247/Login/input_PasswordField'), password)

'Click on the log in button '
WebUI.click(findTestObject('Gillian Objects/Log in/button_Log in'))

WebUI.delay(2)

'Click on the configuration tile '
WebUI.click(findTestObject('Gillian Objects/QC Test Config/h3_Configuration'))

WebUI.delay(2)

'Click on the QC test tab'
WebUI.click(findTestObject('Gillian Objects/QC Test Config/a_QC Test'))

WebUI.delay(2)

'Verify that the text for the QC test is correct and it is the one created on the previous test case (Configuration Wizard - Create) '
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Albumin'), 'Albumin')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Clinical ChemistryAlb'), 'Clinical Chemistry')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Bromocresol Purple'), 'Bromocresol Purple')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Config Wizard InstrumentAlb'), 'Config Wizard Instrument')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_glunitAlb'), 'g/l')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_1008UNalb'), '1008UN')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_1010UNalb'), '1010UN')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_1026UNalb'), '1026UN')

WebUI.delay(2)

'Verify that the text for the QC test is correct and it is the one created on the previous test case (Configuration Wizard - Create)'
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_ALT (GPT)'), 'ALT (GPT)')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Clinical ChemistryALT'), 'Clinical Chemistry')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_ColorimetricALT'), 'Colorimetric')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Config Wizard InstrumentALT'), 'Config Wizard Instrument')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_mUmlALT'), 'mU/ml')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_1006UNalt'), '1006UN')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_1007UNalt'), '1007UN')

WebUI.delay(2)

'Click on the bin icon beside the ALT QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/span_trashALT'))

WebUI.delay(1)

'Click on the No button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/button_No'))

WebUI.delay(1)

'Click on the bin icon beside the ALT QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/span_trashALT'))

WebUI.delay(1)

'Click on the Yes button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/button_Yes'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the QC test has been deleted successfully '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success QC Test configuration deleted'), 
    0)

'Verify the text within the success banner which is displayed to say the QC test has been deleted successfully '
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success QC Test configuration deleted'), 
    'Success! QC Test configuration deleted.')

WebUI.delay(2)

'Click on the success banner so that it disappears '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success QC Test configuration deleted'))

WebUI.delay(2)

'Click on the bin icon beside the Albumin QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/span_trashAlb'))

WebUI.delay(1)

'Click on the No button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/button_No'))

WebUI.delay(1)

'Click on the bin icon beside the Albumin QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/span_trashAlb'))

WebUI.delay(1)

'Click on the Yes button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/button_Yes'))

WebUI.delay(1)

'Verify that the success banner is displayed to say the QC test has been deleted successfully '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success QC Test configuration deleted'), 
    0)

'Verify the text within the success banner which is displayed to say the QC test has been deleted successfully '
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success QC Test configuration deleted'), 
    'Success! QC Test configuration deleted.')

WebUI.delay(2)

'Click on the success banner so that it disappears '
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success QC Test configuration deleted'))

WebUI.delay(2)

'Click on the "Assay" tab'
WebUI.click(findTestObject('Acusera247/Instrument Configuration/button_Tab_Assay'))

WebUI.delay(2)

//'Verify that the text for the QC test is correct and it is the one created on the previous test case (Configuration Wizard - Create) '
//WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_ALT (GPT)assay'), 'ALT (GPT)')
//
//WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Clinical ChemistryALTassay'), 'Clinical Chemistry')
//
//WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_ColorimetricALTassay'), 'Colorimetric')
//
//WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_mUmlALTassay'), 'mU/ml')
//
//WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_AbaxisALTassay'), 'Abaxis')
//
//WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_37altAssay'), '37')

'Declare an integer which will hold the value of the arrays length'
int eight = 8

'Declare 4 arrays. The first one ,"celltext", is going to be the array in which we save the values from each row into'
String[] celltext = new String[eight]

String[] expected1 = ['Calcium', 'Clinical Chemistry', 'Arsenazo', 'g/l', 'Roche', '', '', '']

String[] expected2 = ['CA 125', 'Immunoassay', 'Chemiluminescent Immunoassay (CLIA)', 'U/ml', 'Roche', '', '', '']

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

println('Rows = ' + rows_count)

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
	'To locate columns(cells) of that specific row'
	List Columns_row = rows_table.get(row).findElements(By.tagName('td'))

	'To calculate no of columns(cells) In that specific row'
	int columns_count = Columns_row.size()

	'Loop will execute till the last cell of that specific row'
	for (int column = 0; column < columns_count; column++) {
		'It will retrieve text from each cell'
		(celltext[column]) = Columns_row.get(column).getText()
	}
	
	String delimiter = '| '

	println(String.join(delimiter, celltext))

	if ((celltext == expected1) || (celltext == expected2)) {
		println('This configuration will not be deleted as it is to be used in the QC Test Configuration')
	} else {
		Columns_row.get(7).findElement(By.tagName('a')).click()

		WebUI.delay(1)

		'Click on the "Yes" button within the Pop up Modal'
		WebUI.click(findTestObject('Acusera247/Assay Configuration/button_Yes'))

		WebUI.delay(1)

		'Verify that the Delete Success message is displayed'
		WebUI.verifyTextPresent('Success! Assay configuration deleted.', false)

		rows_table = Table.findElements(By.tagName('tr'))

		//The following code resets the count so that the loop restarts checking each row from the top of the table
		row = -1

		rows_count = rows_table.size()
	}
}

WebUI.delay(2)

WebUI.delay(2)

WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/a_Instrument'))

WebUI.delay(2)

'Verify that the text for the QC test is correct and it is the one created on the previous test case (Configuration Wizard - Create) '
WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_Config Wizard Instrument'), 'Config Wizard Instrument')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_AbaxisInstrument'), 'Abaxis')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_PiccoloInstrument'), 'Piccolo')

WebUI.verifyElementText(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/td_PiccoloModel'), 'Piccolo')

WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/span_Piccolo_Delete'))

WebUI.delay(1)

'Click on the No button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/button_No'))

WebUI.delay(1)

'Click on the bin icon beside the Albumin QC test'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/span_Piccolo_Delete'))

WebUI.delay(1)

'Click on the Yes button'
WebUI.click(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/button_Yes'))

'Verify that the success banner is displayed to say the QC test has been deleted successfully '
WebUI.verifyElementPresent(findTestObject('Gillian Objects/Configuration Wizard/Cleanup/div_Success Instrument configuration deleted'), 
    0)

WebUI.delay(5)

WebUI.closeBrowser()

