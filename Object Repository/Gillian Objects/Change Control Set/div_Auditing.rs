<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Auditing</name>
   <tag></tag>
   <elementGuidId>41648088-b03c-4b0b-8cf1-dd5655522830</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main_content_wrapper']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>main_content_wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-fluid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

        
                        
                            Auditing
                        
                        
                            
                                Advisor
                            
                        
                    
                        Change Control Set
                    
                    
                        Change Instrument
                    
                    
                        Data Review
                    
                    
                        Data Export
                    
                    
                        Schedule Fixed Cumulative
                    
                    
                        Copy Targets
                    
        


    .userGuide {
        color: black;
        display: inline-block;
        vertical-align: middle;
        padding-left: 16px;
        padding-right: 16px;
        font-size: 18px;
        cursor: pointer
    }



    $('#UserGuideButton').click(function (e) {
        e.preventDefault();
        var fileName = GetFileNameBasedOnTranslations();
        var azureBlobURL = 'https://randoxqcstorage.blob.core.windows.net/userguide/' + fileName + '.pdf';
        window.open(azureBlobURL, '_blank');

    });

    function GetFileNameBasedOnTranslations() {
        var region = &quot;&quot;;
        if (Translations.UserGuide[&quot;Guide_Region&quot;] != null) {
            region = Translations.UserGuide[&quot;Guide_Region&quot;];
        }
        else {
            region = 'en';

        }
        var translationFileName = &quot;UserGuide&quot; + &quot;-&quot; + region;
        return translationFileName;
    }


        


    .userGuide {
        color: black;
        display: inline-block;
        vertical-align: middle;
        padding-left: 16px;
        padding-right: 16px;
        font-size: 18px;
        cursor: pointer
    }



    $('#UserGuideButton').click(function (e) {
        e.preventDefault();
        var fileName = GetFileNameBasedOnTranslations();
        var azureBlobURL = 'https://randoxqcstorage.blob.core.windows.net/userguide/' + fileName + '.pdf';
        window.open(azureBlobURL, '_blank');

    });

    function GetFileNameBasedOnTranslations() {
        var region = &quot;&quot;;
        if (Translations.UserGuide[&quot;Guide_Region&quot;] != null) {
            region = Translations.UserGuide[&quot;Guide_Region&quot;];
        }
        else {
            region = 'en';

        }
        var translationFileName = &quot;UserGuide&quot; + &quot;-&quot; + region;
        return translationFileName;
    }

            
            
            
             Confirmation Change control set process has been completed. Please see the report grid below for more details.

            
            

            
            










    
        
        
    



    

        Select Instrument
        Select Lot 1
        Select Lot 2
        Select Lot 3
        Select All
        Clear All
    

    

                
                    


                        QC Test:

                                                                                                                                       


                    
                        QC Panel
                    
                        
                            Clearkendo.syncReady(function(){jQuery(&quot;#clear_selected_tests_button&quot;).kendoButton({});});

                        

                        
                            
                            
                        

                    
                
                Drag a column header and drop it here to group by that column IDAnalyteChemistry TypeMethodUnitInstrumentLot 1Lot 2Lot 3Closed  ParticipantDetails.ParticipantNameNo tests0010510152050items per pageNo items to display
                Drag a column header and drop it here to group by that column IDPanel NameNumber of TestsParticipantDetails.ParticipantName1872Automated Panel 11Dave Automation 251110510152050items per page1 - 1 of 1 items
            
                
                    Nextkendo.syncReady(function(){jQuery(&quot;#forwardButton&quot;).kendoButton({});});



                
            
    



            &lt;div class=&quot;TestDetailGrid_SingleSelection&quot;>&lt;/div>


    &lt;div class=&quot;QCPanelTestsGrid&quot;>&lt;/div>


    // Send the test selection option model to javascript to build necessary components (in nested detail row)
    _TestSelectionOptions = '{&quot;SingleOrMultipleSelection&quot;:0,&quot;RequiresFixedTargets&quot;:false,&quot;RequiresDropdown&quot;:true,&quot;RequiresMetaTargets&quot;:false,&quot;RequiresPerformanceLimits&quot;:false,&quot;RequiresMultirules&quot;:true,&quot;RequiresComparisonGroup&quot;:false,&quot;MultipleItemsRequired&quot;:false,&quot;IsMultirule&quot;:false,&quot;DisableClosedTest&quot;:false,&quot;HideClosedTest&quot;:false,&quot;AllowLotsMultipleSelect&quot;:false,&quot;RequiresIntraPrecision&quot;:false}';




    .category-label {
        vertical-align: middle;
        padding-right: .5em;
    }

    #category {
        vertical-align: middle;
    }

    .toolbar {
        float: right;
        margin-right: 19px;
    }
    .searchIcon {
        margin-right: 3px;
        margin-top: -2px;
        font-size: 19px;
        vertical-align: middle;
        color: #9DA2A4;
    }



    

        Select new control set
        
            
                
                    Lot 1:
                
                
                    1306EC
                
            
            
                
                    Lot 2:
                
                
                    Select a lot...
                
            
            
                
                    Lot 3:
                
                
                    Select a lot...
                
            
        
        Close Test(s)?
        
            
                
            
        
        
            
                
            
            
                Change Control Set
            
            
                Clearkendo.syncReady(function(){jQuery(&quot;#clear_selected_lots&quot;).kendoButton({&quot;icon&quot;:&quot;trash&quot;});});
            
        
    


    
        Result
    
    
        
            
                
                     Successful
                
                1 / 1
            
            
                
                    The following tests have been updated successfully and their control set has been changed.
                
                Drag a column header and drop it here to group by that column
                AnalyteMethodUnitInstrumentLot 1Lot 2Lot 3CA 125Chemiluminescent Immunoassay (CLIA)U/ml                                              Instrument 1 1306EC  
            
        
        
            
                
                    Successful (with warnings)
                
                0 / 1
            
            
                
                    The following tests were partially updated due to an incompatibility with the analyte and the lots selected in the new control set.
                
                
                
            
        
        
            
                
                     Unsuccessful
                
                0 / 1
            
            
                
                    The following test(s) could not be updated due to an incompatible chemistry type with the lots selected.
                
                
                
                
                    The following test(s) could not be updated due to an analyte that is incompatible with any of the lots in the new control set.
                
                
                
            
        
    

        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main_content_wrapper&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='main_content_wrapper']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page_body_wrapper']/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Utilities'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
