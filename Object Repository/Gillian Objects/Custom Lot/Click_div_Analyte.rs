<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Click_div_Analyte</name>
   <tag></tag>
   <elementGuidId>2e94dd81-e24f-4725-ba89-7a132e3bdc68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main_content_wrapper']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>main_content_wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-fluid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            
                        Analyte
                        Method
                        Lot
                        Unit
                        Temperature
                        Instrument
                        Reagent Supplier
                        Mapping
            


    .userGuide {
        color: black;
        display: inline-block;
        vertical-align: middle;
        padding-left: 16px;
        padding-right: 16px;
        font-size: 18px;
        cursor: pointer
    }



    $('#UserGuideButton').click(function (e) {
        e.preventDefault();
        var fileName = GetFileNameBasedOnTranslations();
        var azureBlobURL = 'https://randoxqcstorage.blob.core.windows.net/userguide/' + fileName + '.pdf';
        window.open(azureBlobURL, '_blank');

    });

    function GetFileNameBasedOnTranslations() {
        var region = &quot;&quot;;
        if (Translations.UserGuide[&quot;Guide_Region&quot;] != null) {
            region = Translations.UserGuide[&quot;Guide_Region&quot;];
        }
        else {
            region = 'en';

        }
        var translationFileName = &quot;UserGuide&quot; + &quot;-&quot; + region;
        return translationFileName;
    }

        
        
        
        

        
        

        
        



    
    
    
        Drag a column header and drop it here to group by that column Lot NumberLot NameChemistry TypeExpiry DateApprove0010510152050items per pageNo items to displaykendo.syncReady(function(){jQuery(&quot;#LotGrid&quot;).kendoGrid({&quot;groupable&quot;:{&quot;enabled&quot;:true},&quot;navigatable&quot;:true,&quot;sortable&quot;:true,&quot;columns&quot;:[{&quot;title&quot;:&quot;Lot Number&quot;,&quot;width&quot;:&quot;20%&quot;,&quot;field&quot;:&quot;LotNumber&quot;,&quot;filterable&quot;:{&quot;messages&quot;:{&quot;selectedItemsFormat&quot;:&quot;{0} selected items&quot;},&quot;operators&quot;:{&quot;date&quot;:{&quot;eq&quot;:&quot;Is equal to&quot;,&quot;neq&quot;:&quot;Is not equal to&quot;,&quot;gte&quot;:&quot;Is after or equal to&quot;,&quot;gt&quot;:&quot;Is after&quot;,&quot;lte&quot;:&quot;Is before or equal to&quot;,&quot;lt&quot;:&quot;Is before&quot;,&quot;isnotnull&quot;:&quot;Is not null&quot;}},&quot;checkAll&quot;:false},&quot;encoded&quot;:true,&quot;editor&quot;:&quot;\u003cinput class=\&quot;text-box single-line\&quot; id=\&quot;LotNumber\&quot; name=\&quot;LotNumber\&quot; type=\&quot;text\&quot; value=\&quot;\&quot; /\u003e\u003cspan class=\&quot;field-validation-valid\&quot; data-valmsg-for=\&quot;LotNumber\&quot; data-valmsg-replace=\&quot;true\&quot;\u003e\u003c/span\u003e&quot;},{&quot;title&quot;:&quot;Lot Name&quot;,&quot;width&quot;:&quot;20%&quot;,&quot;field&quot;:&quot;LotName&quot;,&quot;filterable&quot;:{&quot;messages&quot;:{&quot;selectedItemsFormat&quot;:&quot;{0} selected items&quot;},&quot;operators&quot;:{&quot;date&quot;:{&quot;eq&quot;:&quot;Is equal to&quot;,&quot;neq&quot;:&quot;Is not equal to&quot;,&quot;gte&quot;:&quot;Is after or equal to&quot;,&quot;gt&quot;:&quot;Is after&quot;,&quot;lte&quot;:&quot;Is before or equal to&quot;,&quot;lt&quot;:&quot;Is before&quot;,&quot;isnotnull&quot;:&quot;Is not null&quot;}},&quot;checkAll&quot;:false},&quot;encoded&quot;:true,&quot;editor&quot;:&quot;\u003cinput class=\&quot;text-box single-line\&quot; id=\&quot;LotName\&quot; name=\&quot;LotName\&quot; type=\&quot;text\&quot; value=\&quot;\&quot; /\u003e\u003cspan class=\&quot;field-validation-valid\&quot; data-valmsg-for=\&quot;LotName\&quot; data-valmsg-replace=\&quot;true\&quot;\u003e\u003c/span\u003e&quot;},{&quot;title&quot;:&quot;Chemistry Type&quot;,&quot;width&quot;:&quot;20%&quot;,&quot;field&quot;:&quot;ChemistryType&quot;,&quot;filterable&quot;:{&quot;messages&quot;:{&quot;selectedItemsFormat&quot;:&quot;{0} selected items&quot;},&quot;operators&quot;:{&quot;date&quot;:{&quot;eq&quot;:&quot;Is equal to&quot;,&quot;neq&quot;:&quot;Is not equal to&quot;,&quot;gte&quot;:&quot;Is after or equal to&quot;,&quot;gt&quot;:&quot;Is after&quot;,&quot;lte&quot;:&quot;Is before or equal to&quot;,&quot;lt&quot;:&quot;Is before&quot;,&quot;isnotnull&quot;:&quot;Is not null&quot;}},&quot;checkAll&quot;:false},&quot;encoded&quot;:true,&quot;editor&quot;:&quot;\u003cinput class=\&quot;text-box single-line\&quot; id=\&quot;ChemistryType\&quot; name=\&quot;ChemistryType\&quot; type=\&quot;text\&quot; value=\&quot;\&quot; /\u003e\u003cspan class=\&quot;field-validation-valid\&quot; data-valmsg-for=\&quot;ChemistryType\&quot; data-valmsg-replace=\&quot;true\&quot;\u003e\u003c/span\u003e&quot;},{&quot;title&quot;:&quot;Expiry Date&quot;,&quot;width&quot;:&quot;25%&quot;,&quot;template&quot;:&quot;#= kendo.toString(kendo.parseDate(ExpiryDate), \u0027dd/MMM/yyyy HH:mm:ss\u0027) #&quot;,&quot;field&quot;:&quot;ExpiryDate&quot;,&quot;format&quot;:&quot;{0:dd/MM/yyyy hh:mm:ss}&quot;,&quot;filterable&quot;:{&quot;messages&quot;:{&quot;selectedItemsFormat&quot;:&quot;{0} selected items&quot;},&quot;operators&quot;:{&quot;date&quot;:{&quot;eq&quot;:&quot;Is equal to&quot;,&quot;neq&quot;:&quot;Is not equal to&quot;,&quot;gte&quot;:&quot;Is after or equal to&quot;,&quot;gt&quot;:&quot;Is after&quot;,&quot;lte&quot;:&quot;Is before or equal to&quot;,&quot;lt&quot;:&quot;Is before&quot;,&quot;isnotnull&quot;:&quot;Is not null&quot;}},&quot;checkAll&quot;:false},&quot;encoded&quot;:true,&quot;editor&quot;:&quot;\u003cinput class=\&quot;text-box single-line\&quot; data-val=\&quot;true\&quot; data-val-required=\&quot;Pick a date\&quot; id=\&quot;ExpiryDate\&quot; name=\&quot;ExpiryDate\&quot; type=\&quot;date\&quot; value=\&quot;\&quot; /\u003e\u003cspan class=\&quot;field-validation-valid\&quot; data-valmsg-for=\&quot;ExpiryDate\&quot; data-valmsg-replace=\&quot;true\&quot;\u003e\u003c/span\u003e&quot;},{&quot;title&quot;:&quot;Approve&quot;,&quot;width&quot;:&quot;6%&quot;,&quot;template&quot;:&quot;#=DisplayAllApprovalStatus(ApprovalStatus)#&quot;,&quot;field&quot;:&quot;ApprovalStatus&quot;,&quot;filterable&quot;:{&quot;messages&quot;:{&quot;selectedItemsFormat&quot;:&quot;{0} selected items&quot;},&quot;operators&quot;:{&quot;date&quot;:{&quot;eq&quot;:&quot;Is equal to&quot;,&quot;neq&quot;:&quot;Is not equal to&quot;,&quot;gte&quot;:&quot;Is after or equal to&quot;,&quot;gt&quot;:&quot;Is after&quot;,&quot;lte&quot;:&quot;Is before or equal to&quot;,&quot;lt&quot;:&quot;Is before&quot;,&quot;isnotnull&quot;:&quot;Is not null&quot;}},&quot;checkAll&quot;:false},&quot;encoded&quot;:true,&quot;editor&quot;:&quot;\u003cinput class=\&quot;text-box single-line\&quot; data-val=\&quot;true\&quot; data-val-required=\&quot;The Approval Status field is required.\&quot; id=\&quot;ApprovalStatus\&quot; name=\&quot;ApprovalStatus\&quot; type=\&quot;number\&quot; value=\&quot;\&quot; /\u003e\u003cspan class=\&quot;field-validation-valid\&quot; data-valmsg-for=\&quot;ApprovalStatus\&quot; data-valmsg-replace=\&quot;true\&quot;\u003e\u003c/span\u003e&quot;},{&quot;title&quot;:&quot;Delete&quot;,&quot;headerAttributes&quot;:{&quot;style&quot;:&quot;padding-left: 1%&quot;},&quot;width&quot;:&quot;3%&quot;,&quot;template&quot;:&quot;\u003ca class=\u0027k-button k-button-icontext k-grid-Delete\u0027\u003e\u003cspan onclick=\u0027ConfirmLotDelete(event)\u0027 class=\u0027glyphicon glyphicon-trash\u0027\u003e\u003c/span\u003e\u003c/a\u003e&quot;,&quot;headerTemplate&quot;:&quot;\u003cspan class=\u0027glyphicon glyphicon-menu-hamburger waffle\u0027\u003e\u003c/span\u003e&quot;}],&quot;pageable&quot;:{&quot;pageSizes&quot;:[5,10,15,20,50],&quot;buttonCount&quot;:10},&quot;height&quot;:&quot;auto&quot;,&quot;editable&quot;:{&quot;confirmation&quot;:&quot;Are you sure you want to delete this record?&quot;,&quot;confirmDelete&quot;:&quot;Delete&quot;,&quot;cancelDelete&quot;:&quot;Cancel&quot;,&quot;mode&quot;:&quot;inline&quot;,&quot;create&quot;:true,&quot;update&quot;:true,&quot;destroy&quot;:true},&quot;dataSource&quot;:{&quot;type&quot;:(function(){if(kendo.data.transports['aspnetmvc-ajax']){return 'aspnetmvc-ajax';} else{throw new Error('The kendo.aspnetmvc.min.js script is not included.');}})(),&quot;transport&quot;:{&quot;read&quot;:{&quot;url&quot;:&quot;/CustomLotConfiguration/GetAllParticipantLotsAsync&quot;,&quot;type&quot;:&quot;POST&quot;},&quot;prefix&quot;:&quot;&quot;},&quot;pageSize&quot;:10,&quot;page&quot;:1,&quot;total&quot;:0,&quot;schema&quot;:{&quot;data&quot;:&quot;Data&quot;,&quot;total&quot;:&quot;Total&quot;,&quot;errors&quot;:&quot;Errors&quot;,&quot;model&quot;:{&quot;id&quot;:&quot;ID&quot;,&quot;fields&quot;:{&quot;ID&quot;:{&quot;type&quot;:&quot;number&quot;,&quot;defaultValue&quot;:null},&quot;QCTestID&quot;:{&quot;type&quot;:&quot;number&quot;,&quot;defaultValue&quot;:null},&quot;MappingID&quot;:{&quot;type&quot;:&quot;string&quot;},&quot;LotNumber&quot;:{&quot;type&quot;:&quot;string&quot;},&quot;LotName&quot;:{&quot;type&quot;:&quot;string&quot;},&quot;ExpiryDate&quot;:{&quot;type&quot;:&quot;date&quot;},&quot;Result&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;ParticipantID&quot;:{&quot;type&quot;:&quot;number&quot;,&quot;defaultValue&quot;:null},&quot;ApprovalStatus&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;LotAnalytes&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;LotInstruments&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;IsInFirst19Results&quot;:{&quot;type&quot;:&quot;boolean&quot;},&quot;TestDetails&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;PerformanceLimitConfiguration&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;TargetConfiguration&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;UncertaintyMeasurement&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;DeactivationRanges&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;SummarisedCount&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;SummarisedMean&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;SummarisedSD&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;IsUnassayed&quot;:{&quot;type&quot;:&quot;boolean&quot;},&quot;isApproved&quot;:{&quot;type&quot;:&quot;boolean&quot;},&quot;Rilibak&quot;:{&quot;type&quot;:&quot;number&quot;,&quot;defaultValue&quot;:null},&quot;SlideGen&quot;:{&quot;type&quot;:&quot;boolean&quot;},&quot;Category&quot;:{&quot;type&quot;:&quot;string&quot;},&quot;Size&quot;:{&quot;type&quot;:&quot;string&quot;},&quot;IsCalibrator&quot;:{&quot;type&quot;:&quot;boolean&quot;},&quot;ChemistryType&quot;:{&quot;type&quot;:&quot;string&quot;},&quot;ChemistryTypeID&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;SetGloballyAvailable&quot;:{&quot;type&quot;:&quot;boolean&quot;},&quot;LotChangedDetails&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;UpdateType&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;ExtendedResultDetails&quot;:{&quot;type&quot;:&quot;object&quot;},&quot;GroupID&quot;:{&quot;type&quot;:&quot;number&quot;},&quot;SelectedLotID&quot;:{&quot;type&quot;:&quot;number&quot;}}}}},&quot;detailTemplate&quot;:kendo.template(jQuery('#template').html())});});
    
    
        
            Add New
        
    


    
        
            
                
                    Lot Number: 
                
                
                    
                        
                    
                
            
            
                
                    Lot Name: 
                

                
                    
                
            
            
                
                    Expiry Date: 
                
                
                    kendo.syncReady(function(){jQuery(&quot;#lotExpiryText&quot;).kendoDateTimePicker({&quot;format&quot;:&quot;dd/MMM/yyyy HH:mm:ss&quot;});});
                
            
            
                
                    Chemistry Type: 
                
                
                    Clinical Chemistry
                
            
            
                
                    Is Unassayed: 
                
                
                                                                                                                                   
                
            
            
                
                    Slide Gen: 
                
                
                                                                                                                                   
                
            
        
        

        
            Analyte Selection Add All 
                       Clear All 
            
                
                    1 analyte(s) selected25-OH-Vitamin DACTHAlkaline PhosphataseALT (GPT)Amylase PancreaticAmylase TotalAndrostenedioneAnti-TGAnti-TPOAnti-TSHR as a ratioAST (GOT)Basophils %Beta-CrossLapsBilirubin Total CalcitoninCalciumCEAChlorideCholesterol Total CortisolC-PeptideCreatinineCRPDHEA-SEosinophils %FSHGGTGHGlucoseHaematocrit (HCT)Haemoglobin (HGB)HbA1c - DCCT / NGSP alignedHDL CholesterolIGF-1IGFBP-3InsulinIronLarge Unstained Cells %LDL CholesterolLHLipaseLymphocyte %MagnesiumMean Cell Haemoglobin (MCH) Mean Cell Haemoglobin Conc (MCHC)Mean Cell Volume (MCV)Mean Platelet Volume (MPV)MicroalbuminMonocytes %MyoglobinNeutrophil %OestradiolOsteocalcinPhosphate InorganicPlatelet Count Imp (PLT)PotassiumProgesteroneProlactin Protein TotalPTHRed Blood Cell Count Imp (RBC)Red Blood Cell Count Op (RBC)Red Cell Distribution Width CVSHBGSodiumT3 FreeT4 FreeTestosteroneThyroglobulinTotal P1NP TransferrinTriglyceridesTSHUreaUric AcidWhite Blood Cell Count Imp (WBC)White Blood Cell Count Op (WBC)CRP hs1-25-(OH)2 Vitamin D17-OH-Progesterone5-HIAAACEAcid Phosphatase ProstaticAcid Phosphatase TotalAFPAlbuminAlbumin CSFAlcoholAldolaseAldosterone Alpha-1-Acid GlycoproteinAlpha-1-AntitrypsinAlpha-1-GlobulinAlpha-2-GlobulinAlpha-2-MacroglobulinAmikacinAmmoniaAmphetaminesAmylase UrineAnti Streptolysin OAnti-CCPAntithrombin IIIAntithrombin III activityAnti-TSHRApolipoprotein A1Apolipoprotein A2Apolipoprotein BApolipoprotein C2 Apolipoprotein C3 Apolipoprotein EaPTT as a ratioaPTT in secondsBarbituratesBasophil CountBenzodiazepinesBeta-2-MicroglobulinBeta-GlobulinBile AcidsBilirubin Direct BNP CA 125CA 15-3CA 19-9CA 72-4 CaeruloplasminCaffeineCalcium IonisedCannabinoidsCarbamazepineCholinesterase Ciclosporin Creatine KinaseCK-MB CK-MB ActivityCK-MB MassCO2 (Bicarbonate)CO2 (Partial)Cocaine Metabolites Complement C3 Complement C4 Copper Creatinine Urine CYFRA 21-1Cystatin C D-3-HydroxybutyrateD-Dimer DHEA unconjugatedDigoxin Dopamine Electrophoresis Albumin (%)Electrophoresis Albumin (conc)Electrophoresis Alpha-1-Globulin (%)Electrophoresis Alpha-1-Globulin (conc)Electrophoresis Alpha-2-Globulin (%)Electrophoresis Alpha-2-Globulin (conc)Electrophoresis Beta-Globulin (%)Electrophoresis Beta-Globulin (conc)Electrophoresis Gamma-Globulin (%)Electrophoresis Gamma-Globulin (conc)Eosinophils Count EpinephrineErythropoietinEstriol totalEstriol unconjugatedEthanol (Alcohol) EthosuximideFactor II activityFactor IX activity Factor V activityFactor VII activityFactor VIII activity Factor X activity Factor XI activity Factor XII activityFerritinFibrinogenFolateFructosamine Gamma-GlobulinGastrin GentamicinGLDH Growth HormoneGrowth Hormone - RecombinantHaptoglobinHbA1c - IFCC standardiseda - HBDHhCG  hCG free betahCG IntacthCG TotalHomocysteineIgAIgEIgG IgG CSFIgG Subclass 1IgG Subclass 2IgG Subclass 3IgG Subclass 4IgMImmature Granulocytes %Immature Granulocyte CountInhibin AIPF Kappa Light Chain Free Kappa Light Chain Total LactateLambda Light Chain FreeLambda Light Chain TotalLAPLD (LDH)Lipoprotein (a)LithiumLymphocyte Count MetanephrineMethadoneMethotrexateMonocyte Count NEFANeutrophil CountNorepinephrineNormetanephrineNSENT-Pro BNP O2 (Partial)OpiatesOsmolalityOxalatePacked Cell VolumePAPP-A ParacetamolpHPhencyclidine PhenobarbitalPhenytoinPhospholipidsPlasma Renin ActivityPlasminogen activityPlatelet Count OptPlatelet Distribution Width (PDW)Platelet Larger Cell Ratio (PLCR) PlateletcritPrealbuminPrimidoneProcalcitonin Protein C activityProtein S activityPSA FreePSA TotalProthrombin Time activityProthrombin Time as ratioProthrombin Time as INRProthrombin Time in secondsRed Cell Distribution Width SDRenin DirectRetinol Binding ProteinRheumatoid FactorS100SalicylateSalicylic Acid Specific Gravity T UptakeT3 TotalT4 TotalTestosterone FreeTheophyllineThrombin Time in secondsThrombin Time as a RatioThyroxine Binding GlobulinTIBCTobramycinTotal Antioxidant StatusTotal Lipids Troponin I Troponin TTroponin T hsUIBCValproic AcidVancomycin Vanillylmandelic acidVitamin B12 Whole Blood FolateZincTroponin I hsAcid Phosphatase non-ProstaticCA 27-29SYPHTSIHbA2HbFHbSCarboxy HaemoglobinMethanemoglobin Anti HEPCAnti HIVRubellaBACTCASTEC in UrineRBC in UrineWBC in UrineAnalyteTestCustom Analyte Test 101Custom Analyte Lot 2 OnlyCustom Analyte Tri Level Lot TestingCustom Analyte TestingAnalyte TriAnalyte Dual 1 &amp; 2Analyte Dual 1 &amp; 3Analyte Dual 2 &amp; 3Analyte Uni 1Analyte Uni 2Analyte Uni 3387 AnalyteAnalyte RFC0384Aidan Analyte Test 4Aidan Analyte 10Custom Analyte RFC386 Test 1Custom Analyte RFC386 Test 2Custom ANalyte RFC386 Test ACustom Analyte RFC386 Test BCustom Analyte RFC386 Test CCustom Analyte RFC386 Test DCustom Analyte PF Test 1Custom Analyte Delete 1Custom Analyte Mapping Delete 1Custom Analyte Mapping Delete 2Custom Analyte Mapping Delete 3Custom Analyte Mapping Delete 4Custom Analyte RFC386 Test 1ACustom Analyte RFC386 Test 2ACus Analyte AD 2Cus Analyte PG T1Custom Analyte 0611Custom Analyte 0611Custom Analyte 11Steve AnalyteCustom Analyte 2811ST Analyte 2811ST Analyte 100Custom Analyte TA11Custom Analyte 3011Custom Analyte 3015Custom Analyte 0312SRT Analyte 0312Custom Analyte 0412Testing Analyte CustomAnalyte Custom TestingCustom Analyte 0512ATAnalyteName10Alt Analyte 0512CAnalyte11AName Analyte TestCAnalyteA21CAnalyteA24CAnalyteA25CAnalyteA26 CAnalyteA26a5 hydroxyindoleacetic acidcda101 Analyte02 Analyte03 Analyte10 Analyte99 Analyte30 Analyte35 Analyte40 AnalyteBlaine Custom Analyte50 Analyte60 Analyte70 Analyte80 Analyte90 Analyte88 analyte0402 Analyteaimeeacaimee test
                
            
        

        
            Instrument Model Selection Add All
                          Clear All
            
                
                    1 instrument(s) selected1032483003484504805007047057177367377478508558608659029049119129176000705071507600103AP1100 Series120FR1600DR1940 Plus2300 Plus2300GL2900M2950D30 Plus30R400/405550 Express600 Series925/940A15A25Abacus 380ABL 5ABL 500ABL 700ABL 77ABL 80ABL 800ABX Pentra 400ABX Pentra 80ABX Pentra C200AC-4ACAAccent 200Accent 300AccessAccess 2Accute TBA-40FRACE AleraACE AxcelACFACL 100ACL 1000ACL 10000ACL 200ACL 2000ACL 300ACL 3000ACL 5000ACL 6000ACL 7000ACL 8000ACL 9000ACL AdvanceACL EliteACL Elite ProACL FuturaACL MCL2Cobas ISE ModuleACL TOP 300 CTSACL TOP 350 CTSACL TOP 500 CTSACL TOP 550 CTSACL TOP 700ACL TOP 700 CTSACL TOP 750ACL TOP 750 CTSACL TOP 750 LASACQUITY ionKey/MSACQUITY TQDACQUITY UPC2 SystemACQUITY UPLC H-Class SystemACQUITY UPLC I-Class SystemACQUITY UPLC M-ClassACQUITY UPLC SystemACS 180ActiveAdvanced 2020Advanced 3250Advanced 3300Advanced 3320Advanced 3MOADVIA 120ADVIA 1200ADVIA 1650ADVIA 1800ADVIA 2120iADVIA 2400ADVIA Centaur CPADVIA Centaur XPADVIA IMSAE-1000AE-3000AerosetAFT 300AFT 500AIA 2000AIA 360AIA 600IIAIA 900Alcyon 300AMAX  AMAX DestinyAQT 90 FLEXArchitect c16000Architect c4000Architect c8000Architect i1000Architect i2000Array 360ATAC 8000AU5400AU5800AU1000AU2700AU400AU480AU500AU5000AU600AU640AU680AU800Aution Max AX-4280AutoDELFIAAutoLumo A2000AutolyserAvivaAviva ExpertAviva NanoAVL 9110AVL 9120AVL 9130AVL 9140AVL 9180AVL 9181AVL 940AVL 980AVL 990AVL Compact 1AVL Compact 2AxSYMBA400BC-2300BC-2800BC-3000 PlusBC-5200BC-5500BC-5600BC-5800BC-6600BC-6800BCSBCS XPBCTBD FACSCaliburBD FACSCanto IIBICOBIOBAS 10BIOBAS 1000BIOBAS 20/40Biolyte 2000Biolyte VBioPhotometer D30Bio-Plex 2200BioProfile BasicLIASYS 450BioProfile FlexBioProfile Flex 2BioProfile pHOXBio-Rad D-10Biosen C-Line ClinicBiosen C-Line GP+BN IIBN ProSpecBS-200BS-300BS-400BS-800BSA 3000BT3000 PlusBT4500 PlusBTS350CA-1500CA-50CA-560CA-5000CA-6000CA-620CA-660CA-7000CAPILLARYS 2Cardiac ReaderCareLyteCareSens NCareSens N NFCCareSens N POPCareSens N PremierCareSens N VoiceCareSens PROCB 350iCELL-DYN 1800Celly 70CHEM 200CHEM 300CHEM 400Chem 5CHEM 500Chem 7Chemwell 2910ChorusCLC 385CLINITEK 500Clot 1Clot 2SClot SPClover A1cCoaDATA 2001CoaDATA 4001Coag-A-Mate MTXCoagulometer CC-3003Coagulometer CL4Coagulometer K-3002 OpticCoasys Plus CCoat-A-CountCoatron A4Coatron A6Coatron IVCoatron M1Coatron M2Coatron M4Coatron XCobas b121Cobas b123Cobas b221Cobas BioCobas c111Cobas c311Cobas c501Cobas Core EIACobas e411Cobas e601Cobas h232Cobas MiraCobas u411CoL 1/2/4Combi LineCombilyzer 13Combilyzer VACompact PlusContour LINK MeterCS -2000iCS-2100iCS-400CS-800Synchron CX3D360DCA 2000DELFIADELFIA XpressDemandDIAcheckDiaMed CD-XDimension ARDimension EXL 200Dimension RXLDimension Vista 1500Dimension Vista 500Dimension XpandDimension Xpand PlusDri-Chem 3500iDri-Chem 4000iDri-Chem 800DS2DS5EasyBloodGasEasyLyteEasyRAEasyStatEBIOEcho PlusELANELAN DRCElecsysELITEE-LizaMat 8882/4E-LizaMat GSEML 100EML 105EnzymunEon 100ErisETI-MAX 3000EvidenceEvolutionF360F560FA 200Falcor 300Fibrintimer BFAFibrintimer BFTFibrintimer BFT IIFalcor 600FLM 3FP20G300G400G450GASTAT 1800GASTAT 700GASTAT naviGEM OPLGEM Premier 3000GEM Premier 3500GEM Premier 4000Global 300Glucocard ExpressionGlucose 201+HA-8121HA-8140HA-8160HA-8180HandUReaderHERAHLC 723G7HLC 723G8HLC 723GXHPLC 300HPLC 400HPLC 500HumaClot DuoHumaClot JuniorHumaClot ProHumaLyte Plus 3HumaLyte Plus 5HumaLyzer 2000HumaLyzer 3000HumaLyzer 900HumaLyzer PrimusHumaReader HSHumaReader Single PlusHumaStar 100HumaStar 180HumaStar 200HumaStar 300SRHumaStar 600HumaStar 80HYDRASYS 2iCHROMAIDS-iSYSIL 1700IL 243IL 743IL 943iLab 300iLab 600iLab 650iLab TaurusiLyteIMMAGEImmulite 1Immulite 1000Immulite 2000Immulite 2000 XPiImmulite 2500Immuno 1ImmunoCAP 100ImmunoCAP 250IMXInCCAInCCA BitInCCA Counter 18scInfinity 1200INTEGRA 400 PlusINTEGRA 800InvestigatorIRMA CountIRMA TruPointISE 4000ISE 6000i-Seriesi-STATIV PlusJCA-BM6010/CKC1KC4Kenza 240TXKNAUER K-7000Konelab 20Konelab 30Konelab 60Konelab DeltaKonelab MicrolyteKonelab ProgressKonelab SpecificKonelab UltraKRYPTOR Compact PlusKX-21NLabUMatLAMBDA 1050LAMBDA 265LAMBDA 365LAMBDA 465LAMBDA 650LAMBDA 750LAMBDA 850LAMBDA 950LAURALAURA MLAURA SmartLIAISONLIAISON XLLIASYS 330Libra S21LIDA 500Lisa 200LUMIPULSE G1200LUMIPULSE G600 IILWE60Maglumi 1000Maglumi 2000Maglumi 2000 PlusMaglumi 4000Maglumi 600Maglumi 800MAGO PlusMap Lab PlusMAXMAT PLMediaMEGAMGC 240MicrolabMicro-Osmometer 210mini VIDASMINICAPMININEPHMispa NeoMission U120Miura 1Modular DModular e170Modular ISEModular PMONOMT1/4CNephelometer DELTANEPHYSNexera Method Scouting SystemNexera MPNexera MXNexera QuarternaryNexera SRNexera UCNexera X2Nexera XRNycoCard ReaderNycoCard Reader IIOMNI COMNI SOption 2/4 OsmoStation OM-6050ParallelPATHFASTPDQPerspectivePhadia 2500PiccoloPictus 400Pictus 700Plus Lyte IIPointe 180Precil C2000Premier Hb9210Prestige 24iProLyteProminenceProminence NanoQuikRead 101QuikRead GoRAMP 200RAMP Reader 720RAPIDChem 744RAPIDLab 1265RAPIDLab 248RAPIDLab 278RAPIDLab 348EXRAPIDPoint 350RapidPoint 400Refloton PlusReplyRIASTARRT-200C PlusRX AltonaRX DaytonaRX Daytona PlusRX ImolaRX ModenaRX MonacoRX MonzaRX SuzukaRX-600Sapphire 120Sapphire 200Sapphire 350Sapphire 500Sapphire 600Sapphire 800SAS-1 SAS-2SAS-3SAT 450Saturno 300Saturno 300 PlusSavantAASavantAA ZeemanSavantAA ΣScreen LyteScreen Master TouchSMA CSmartLyteSolea 2/4SPA PlusSpectronic 20Spectrophotometer XLSpectrophotometer XSSpectrum CCxSpheraSphera DuoSPINLAB 100SPINLAB 180SPINLAB XLSpotchem EZSpotlyteST-200CLSTA Compact CTSTA SatelliteStago ST2Stago STA CompactSTA-RStarDust MC15Starlyte IIISTart 4STart 8Stat Fax 1904 PlusStat Fax 303 PlusStat Fax 3300 PlusStat Fax 4200Stat Fax 4700StatProfile CCXStatProfile pHOXStatProfile pHOX UltraStatProfile PrimeStatStripStatStrip XpressStratus CSStratus CS 200SunriseSuper GSuper GL2Synchron CX4Synchron CX5Synchron CX7Synchron CX9Synchron LX20Synchron LX20 PROSynchron LXi725TC-MatrixTDxTechnicon opeRATechnicon RA1000Technicon RA2000Technicon RA50Technicon RA500Technicon RA-XTThrombolyzer Compact XThrombolyzer XRCThrombolyzer XRMThromboscreen 1000Thromboscreen 200Thromboscreen 400CThrombostatThrombotimerTRACE 1300 GCTRACE 1310 GCTriage MeterProTri-statTRITURUSTurbi-QuickTurbitimerTurboxUA-66UF-1000iUF-500iUniCel DxC 600UniCel DxC 800UniCel DxI 600UniCel DxI 800URiSCAN Optima IIURiSCAN SuperURiSCANPro IIUrisys 1100Urocheck 120V8 E-ClassV8 NexusVAPRO 5600VARIANT Express VARIANT IVARIANT IIVARIANT TurboVetMateVetScan VS2Viatron SPSVIDASVIDAS 3VIKIA 1/2VITALAB EclipseVITALAB FlexorVITALAB SelectraVITROS 250VITROS 350VITROS 3600VITROS 4600VITROS 5.1 FSVITROS 500VITROS 5600VITROS 700VITROS 950VITROS DT60VITROS ECiQViva-EViva-JrViva-ProEV-TwinWizardXE-2100XL-1000XL-180XL-200XL-300XL-600XL-640XN-1000XN-2000XS-1000iXT-1800iXT-2000iCobas c702Cobas c701Cobas c502Cobas e602UniCel DxC 880iUniCel DxC 600iUniCel DxC 660iUniCel DxC 680iUniCel DxC 860iAVL OptiLAMBDA BioLAMBDA Bio PlusLAMBDA XLSLAMBDA XLS PlusRAPIDPoint 340RAPIDPoint 4056000Cobas c711CA 500CA 540ADVIA 60CUBE 30ABX Micros 60ADVIA Chemistry XPTBC-5380BS380MINICUBEtest modelSiemens Rapidlab 348Rapidpoint 500Cobas e411COBAS e.411PENTRA 60 PENTRA 60 HYBRID XL DRGSysmex XT 2000isysmex cerradoOptiliteVitros 5600 (UD)PDQTromboelastografo HaemascopeTEGCobas c513A-15TERMOMETRO  01TERMOHIGOMETRO  0341TERMOHIGOMETRO  0344RA 50 ACCUTREND  PLUS EM Destiny - 180EM - 200Micross Ultima Platinum LC-MS/MS        Immunodiagnostic Systems iSYS           Griffols Triturus                       Packard Cobra                           AB Sciex API 4000                       Chlorochek 3400                         Varian LCMS                             Varian Spectra AA                       Varian ProStar HPLC                     Micromass Quattro Ultima                BD FACSCanto II (Flow)                  Diasorin Liaison XL                     Uvikon XL                               Binding Site Optilite                   D-10                                    XT 1800i CerradoSiemens BN proSpec                      sysmex UF 500i                          WHIRPOOLBD FACSCanto                            Agilent 1200                            Molecular Devices Plate Reader          Sherwood 926s                           Wescor 3120                             EGH-Wescor 3120                         EGH-Sherwood 926s                       STH-Wescor 3120                         STH-Sherwood 926s                       Biostat bsd570                          Biochrom 30                             AutoDELPHIA                             Sciex MS/MS                             Abbott Architect ci4100                 Thermo Solaar M6 Atomic Absorption SpectWallac MDS Sciex System MS/MS           Wescor Sweat Chek 3120                  Quattro Micro API Micromass             Advanced Micro Osmometer 3320           Analyser                                HPLC Thermo                             Thermo UHPLC Dionex UltiMate 3000       Thermo TMS                              E.S.R.                                  ESR                                     Thermo iCAPQ                            Biochrom 30+                            BD FACSCanto                            Agilent 1200                            Molecular Devices Plate Reader          Sherwood 926s                           Wescor 3120                             EGH-Wescor 3120                         EGH-Sherwood 926s                       STH-Wescor 3120                         STH-Sherwood 926s                       BC-5380Sysmex XE series                        Egg Collection                          Frozen Embryo Transfer                  IVF Fertilisation                       ICSI Embryology                         Interlab Genio S Electrophoresis        Siemens immulite 2000                   COBAS INFECCIOSAS                       COBAS 6000 E11                          COBAS 6000 E 12                         COBAS 6000 E 21                         COBAS 6000 E22                          Química A1-A                            Química A2-A                            Química A1-B                            Phadia 250                              Manual ELISA                            BioTek ELx800                           SPAplus                                 ADVIA 60                                PREMIER HB 9210                         VES MATIC CUBE 200                      PHADIA                                  VESCUBE30                               Abbott Architect i2000                  Sysmex XN-SERIES                        Thrombotimer 1                          Pharmacia ImmunoCAP 100/UniCAP          ElectaLab                               Stago Start 4                           Sysmex XN-2000-R                        Ortho Vitros 250                        COL 2                                   ABL 800                                 INCUBADORA MICROBIOLOGICABAÑO SEROLOGICODIESTRO 103                             Mindray BS 380                          Ruby                                    ABX Micros ES60                         ABX MICROS ES 60                        ABX MICROS ES60.                        ABX MICROS ES60..                       DIASORIN LIAISON                        EQUIPOS VARIOSCOBAS  6000 HIBRIDO 1Manual method                           AB Sciex API 3200MD LC/MS/MS System     AB Sciex API 4000 LC/MS/MS System       Agilent GC 5975B MSD                    Agilent GC 5973 MSD                     Labsystems Fluoroskan Ascent FL         Perkin Elmer 2021 GSP                   Waters Acquity LC/MS/MS System          Cary 100 UV Visible Spectrophotometer   Training Instrument                     Bio-Rad Luminex XYP                     Manual Elisa                            Manual method                           Leimat LB9507 (Berthold Technologies)   Phadia ImmunoCap 250/Unicap             BD FACS Lyric                           CAPILLARYS 2HA-22 TouchURIT-8021ASiemen Centaur XP                       Roche Cobas 9180                        Mindray BC 5500                         Sysmex XN 350                           Erba ECL105                             Beckman Coulter Act 5Diff               Erba EM 200                             Agappe Mispa i                          CA-50                                   Vesmatic Cube 80                        Abbott Cell Dyn Ruby                    Biorad Reader 680                       FacsCalibur                             Mispa i2Roche Cobas b 121                       Sysmex XS 800i                          CA-50 - Transasia                       Micro SED-10- ESR                       CS-2500Manual                                  Roche COBAS 4800                        Cepheid                                 Vitech Osmometer 3300                   Vitech Osmometer 3301                   Ammonia Checker                         IDS ISys                                Thermo-Fischer                          Thermo Fisher X1                        Thermo Fisher X2                        X 2 Series                              X1 Series                               Thermo Fisher iCAP Qc                   Vesmatic                                XP-300Beckman NXp Plate System                Beckman AU680 1014                      ADVIA                                   ADVIA 60                                SYSMEX KX 21                            BFT2                                    PATHFAST CTN I                          COBAS C 111                             AVL 9140                                AVL 9100 Series                         Roche AVL 9100 Series                   SYSMEX KX21                             XP SERIES                               XP 300                                  Roche/Sysmex XN 1000                    cobas b123                              Tromboelastografo Haemoscope            Orgentec                                Stago STA Compact Max                   STA Compact Max 2                       Alifax Test 1                           Alifax Microtest                        Alegria                                 Roche Cobas 6000/8000-2                 Roche/Sysmex XN 1000 R                  Alegria-2                               Roche b221-2                            Roche b221-3                            Roche b221-4                            Roche b221-6                            IL/Beckman ACL TOP-2                    AggRAM                                  Trinity/Menarini Premier Hb9210 (2)     URIT-8021A                              URIT-3000 Plus                          HB 9210                                 HA-22 TOUCH                             Rayto 7600                              BS-200E MINDRAY                         BA-88A                                  BA-88A MINDRAY                          SYSMEX XT 1800i                         COBAS e 411                             Dynex Ds2                               PREMIER                                 SYSMEX 1 XT 1800i                       SYSMEX 2 XT-1800i                       CS-2100I Siemens                        HYBRID XL DRG                           IMMULITE 1000                           SYSMEX XT-1800i M.AB                    SYSMEX 2 XT-1800i M.AB                  XXXXXX                                  COBAS E 602-Serie 6000                  Roche, Cobas e 602                      Roche cobas e602                        Modular Cobas e 601                     Roche Cobas E-601 Serie 6000-8000       Modular Cobas E-601                     Cobas E-601                             Rohe Cobas e-601                        Roche, Cobas C501                       Thunderbolt                             Siemens viva-E                          Viva-E, Siemens                         Rayto RAC-050                           STA Compact Max                         Sysmex XN 1000                          Sysmex XN 1000MA                        ROCHEM ACL 300                          Abx Micros ES 60                        ABX Micross 60                          Imola 2                                 Mindray 5380                            MINICUBE                                EQUIPO HEMATOLOGIA PRUEBA               Roche XT2000i                           Roche XT2000i Abierto                   Sysmex XT-2000i C                       Trinity/Meranini Premier HB9210 (2)     Epoc Blood Analysis                     Siemens Rapidlab 348                    UF1000i                                 VITROS 5600                             VES MATIC EASY                          IL/Beckman ACL Top 500                  ABX Micros ES60                         ABX MICROS ES 60                        ABX MICROS ES60.                        ABX MICROS ES60..                       DIASORIN LIAISON                        HA 22 TOUCH                             DIASORIN LIAISON                        ABX PENTRA 60                           ABX MICROS ES60                         BS 200 SAN MARCOS                       ACL 2000                                CELLDYN RUBY                            Lumatic                                 Lumistat                                Aldir                                   Maglumi                                 Roche Elecsys                           HUMASTAR 300                            OLYMPUS AU 400                          LIAISON XL                              Sysmex KX21                             Sysmex XT1800                           Sysmex XT 2000 DIA                      Sysmex XT 2000 NOCHE                    Sysmex XT1800 DIA                       Sysmex XT 1800 Noche                    Sysmex XE                               Roche Sysmex XT2000i                    Roche Sysmex XT4000i                    Cobas c501                              STA Compact MAX                         Siemens Rapidpoint 500 series           Sysmex CA2500                           Helena Spife 3000                       Urised                                  Sysmex XN 1000                          Alcor Ised                              Sysmex XN 1000/2000/3000/9000           Cobas e801CS-5100EMERALD 22Stago STA compac                        Grifols Q                               CL-2000iCL-1000iHA 22 TOUCHDrew-3 (hematología)                    D-10 BIO RAD                            ISED ALCOR                              FACS CALIBUR                            Abbott Celldyn Ruby                     926S Chloride AnalyserBiotek ELx800                           Celltac F                               Cobas e 411                             Tosoh Gx/G8                             Vitros ECi                              Cell Dyn Ruby                           Stago compact Max                       Abbott Ruby                             XN-3000CS 2100i                                Beckman Coulter ACT-5 Diff CP           Architect i 1000                        Thrombo STAT                            UDI Chem 810                            Indiko Plus Themo Scientific            9180 Electrolyte Analyzer               Caretium XI-921                         SYSMEX N 21                             COBAS INTEGRA 400                       STAGO DIAGNOSTICA                       Mindray BS-380                          ERMA PCE-210                            Seimens Dade Behring BFTII              ERMA-ANC-PCE-110                        Cobas c111                              Acctren plusOptiliteCLIMA MCXI-921CustomMod1CustomModXS-500iIndigoModelCustomModel2GediminasTestTestModelMossTestdfszdfsdfmodeltest1stevietest1stevietest2andytest1andytest2ConorModelCustom Model Test 101CustomModelForRFCCustom Model Lot 2 OnlyCustom Group Tri Level Lot TestingCustom Model Tri Level Lot TestingCustom Model TestingModel TriModel Dual 1 &amp; 2Model Dual 1 &amp; 3Model Dual 2 &amp; 3Model Uni 1Model Uni 2Model Uni 3Custom Model 387Custom Mod RFC0384Model RFC0384TestModelTestModel2Aidan Model 1Aidan Model Test 4Aidan Model 10Aidan Model 11Custom Model Test A2CustomModel RFC386 2Custom Model Test 2 RFC386Custom Sysmex Model 1Custom Model ATAC 1Custom SensaCore ST Model 1Custom Model Test 1 RFC386Custom Model Test 3 RFC386Custom Model Test 4 RFC386Custom Model Test A RFC386Custom Model Test B RFC386Custom Model RFC386 Test CCustom Model RFC386 Test DCustom Model Test ECustom Model Delete 1Custom Model Mapping Delete 2Custom Model Mapping Delete 3Custom Model Mapping Delete 4Dave ModelCustom Model Test 1 RFC386Custom Model Test 2 RFC386Cus Mod AD 2Delete Model Test 2Cus Mod PG T1Custom Model 0910c1600aCustom model 1610Testing 1Simon Inst ModelAidan Ins ModDP1 Reportable Limits ModelReportable Limits Model 1Reportable Limits Model 2Reportable Limits 1Dave M1 G1 Model 1Dave M2G1 Model 1Dave M2G1 Model 2DP3 M1G1 Model 4DP3DP3 M1 Model 3Sample ModelAidan Ins Mod 1H2DP33Instrument Mod ID #79Aidan Ins Mod 3DP3 New ModelDP4 M2G1 Model 1Aidan Ins Mod 4Panel Model 0511novmodelnovcusmodCustom Model 0511 v2IS M1G1 Model 1Aidan Ins Mod 6Custom Model 0711DFGDFGAidan Ins Mod 7Ins No Gro ModPiccolo 1Custom ModelRPOC Result ModelDesign Model 1Design Model 2Scenario 1 M1G1 Model 1Scenario 1 M1G1 Model 2Scenario 1 M1G1 Model 3Scenario 1 M1G1 Model 4pIC1Custom Model 2811ST Model 100aCustom Model 3011Custom Model 3012Custom Model 3015Custom Model 117Custom Group 0312Custom Mod TA12ST Model 1234Custom Model Map 0312Custom Mod TA17test1Mod1acSRT Model 0312Custom Mod TA18Ins Mod A1Reg Test no 23 MCustom RPOC Model 0312Custom model 0412Cobas c711PDQTesting Model CustomCustom RPOC Model 0412Model Test CustomCustom Model 0512CMod10AMODTModel10TestAlt Model 0512Name Model TestCustom RPOC Model 0512CMod11ACMod21ACModA24CModA26Instrument Model 1712Dave M4 G1 Model 1Dave M4 G1 Model 2Inst Model ST17121 IT SW Test ModelJordgubbeMan GlucoseMan CalciumInstrument Model 08Panel 0801 ModelPanel 0801 Model 2Panel 0801 Model3Panel 0801 Model 22 IT SW Test ModelPanel 0901 ModelPanel 0901 Model 2Panel 0901 Model 3Panel 0901 Model 2RPOC Model 1101RPOC Model 1102RPOC Model 1103RPOC Model 11021401 RPOC Modelcdmod11501 res test model1501 R Test Instrument RPOC Model 16011401 RPOC Model 23Inst Model 1801Test2CL1049 Model Test 1Instrument Setup 2 L1 ModelCL1001 Model2301GW2301 Test Model 2Multi Inst ModelRPOC Model 25011202gw1234Inst Model 209101 Model02 Model03 Model10 Model99 Model30 Model35 Model40 ModelBlaine Custom ModelInst Model 30011202199350 Model60 Model70 Model80 Model90 ModelInst Model 3101Gillian Model 121288 ModelFeb 1 Model 0402 ModelInst Model 0402CountingModelTest ModelCustom Model 1403aimeeCustom ST Model 1803DXC700Instrument Model 02040204 Model 1 
                
            
        
    
    
    
        
            
                
                    
                    Save
                
            
        
    



    &lt;div class=&quot;k-tabstrip-wrapper&quot;>&lt;div class=&quot;k-widget k-tabstrip k-header&quot; id=&quot;tabStrip_#=ID#&quot;>&lt;ul class=&quot;k-reset k-tabstrip-items&quot;>&lt;li class=&quot;k-item k-state-default k-state-active&quot;>&lt;span class=&quot;k-link&quot; unselectable=&quot;on&quot;>Analyte&lt;/span>&lt;/li>&lt;li class=&quot;k-item k-state-default&quot;>&lt;span class=&quot;k-link&quot; unselectable=&quot;on&quot;>Instrument&lt;/span>&lt;/li>&lt;/ul>&lt;div class=&quot;k-content k-state-active&quot; id=&quot;tabStrip_#=ID#-1&quot; style=&quot;display:block&quot;>&lt;div class='AnalytesGrid'>&lt;/div>&lt;/div>&lt;div class=&quot;k-content&quot; id=&quot;tabStrip_#=ID#-2&quot;>&lt;div class='InstrumentsGrid'>&lt;/div>&lt;/div>&lt;/div>&lt;/div>&lt;script>kendo.syncReady(function(){jQuery(&quot;\#tabStrip_#=ID#&quot;).kendoTabStrip({});});&lt;\/script>







    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main_content_wrapper&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//div[@id='main_content_wrapper']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='page_body_wrapper']/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Utilities'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[3]/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
