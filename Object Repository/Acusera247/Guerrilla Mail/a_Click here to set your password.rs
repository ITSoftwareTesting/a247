<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Click here to set your password</name>
   <tag></tag>
   <elementGuidId>73220ab1-ba3b-4512-9b79-037c535a88be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='display_email']/div/div[2]/div/p[4]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>randoxqcv2-stes.azurewebsites.net/Account/SetPassword?Guid=g8NtpJTCx9gv7145pqeqFw1MuXRADKSpbG9eOLvEnP8oEiwKyCWGmV034Sld3bwqRBBhAM%2FBDIGGjDMpj44+4S7HQbbrZtRkC105mgWRTeN3puUW1nMwPqt6d1x1L9L1eu2DJwa0bqLzcXvFlU+%2FJaImKIaEPJ6afeISXsi1U0w%3D</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Click here to set your password</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;display_email&quot;)/div[@class=&quot;email_page&quot;]/div[@class=&quot;email&quot;]/div[@class=&quot;email_body&quot;]/p[4]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='display_email']/div/div[2]/div/p[4]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <value>//a[contains(text(),'Click here to set your password')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username:'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name:'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Best Regards'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Control Department'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <value>//a[contains(@href, 'randoxqcv2-stes.azurewebsites.net/Account/SetPassword?Guid=g8NtpJTCx9gv7145pqeqFw1MuXRADKSpbG9eOLvEnP8oEiwKyCWGmV034Sld3bwqRBBhAM%2FBDIGGjDMpj44+4S7HQbbrZtRkC105mgWRTeN3puUW1nMwPqt6d1x1L9L1eu2DJwa0bqLzcXvFlU+%2FJaImKIaEPJ6afeISXsi1U0w%3D')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//p[4]/a</value>
   </webElementXpaths>
</WebElementEntity>
