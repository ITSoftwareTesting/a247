<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>24.7 Performance Limit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>baf7303b-ebd1-4572-b148-fbc552b4f6ec</testSuiteGuid>
   <testCaseLink>
      <guid>dbbd8a80-04cf-422a-b874-01479bb47614</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Performance Limit Configuration/1. Performance Limit Configuration - Create</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1fbec718-b0f1-4586-a821-9ad52584489c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Performance Limit Configuration/2. Performance Limit Configuration - Edit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a96a15a2-7c98-4dfe-9faa-74db2a3ef81c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Performance Limit Configuration/3. Performance Limit Configuration - Delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
