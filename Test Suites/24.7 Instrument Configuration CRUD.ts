<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>24.7 Instrument Configuration CRUD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>35196659-6499-4fcd-884a-de71284e0f54</testSuiteGuid>
   <testCaseLink>
      <guid>0f67ba4f-5e06-4aae-b343-b3535dbaa736</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Instrument Configuration/1. 24.7 Instrument Configuration - Create</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d9c4b76-9f6f-41a3-8154-14f50726fa05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Instrument Configuration/2. 24.7 Instrument Configuration - Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80652cc7-3648-405e-8ded-5998c7dba463</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Instrument Configuration/3. 24.7 Instrument Configuration - Update</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed42f4e0-e254-484b-9baa-3efab2c644d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Instrument Configuration/4. 24.7 Instrument Configuration - Delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
