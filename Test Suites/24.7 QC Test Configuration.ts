<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>24.7 QC Test Configuration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>917d9e88-6fd2-44b2-8387-9f7e2413493e</testSuiteGuid>
   <testCaseLink>
      <guid>526068d2-ad7c-4177-9ce2-7479c96569d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/QC Test Configuration/1. 27.4 QC Test Configuration - Create</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85bf0f2e-719c-4bdd-860c-1e82c81aa4c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/QC Test Configuration/2. 27.4 QC Test Configuration - Edit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b40f5bec-dd69-494d-a6dd-9ad18c9db9a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/QC Test Configuration/3. 27.4 QC Test Configuration - Delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
