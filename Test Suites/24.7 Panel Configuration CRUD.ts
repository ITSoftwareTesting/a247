<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>24.7 Panel Configuration CRUD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2bfece1d-2345-4db9-bd61-4055b548eac9</testSuiteGuid>
   <testCaseLink>
      <guid>d6152421-a257-4e6f-98f7-d4520d8a2ef9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Panel Configuration/1. 24.7 Panel Configuration - Add</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fe370f4-eae1-4c2c-a15b-13b0519e4c66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Panel Configuration/2. 24.7 Panel Configuration - Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75535428-0d44-4d91-8f81-b8b52e4061eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Panel Configuration/3. 24.7 Panel Configuration - Edit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48666b8d-c886-4226-a061-f48bafc123ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Panel Configuration/4. 24.7 Panel Configuration - Delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
