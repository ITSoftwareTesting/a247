<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>24.7 Intraprecision Configuration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>05732838-abad-41ba-ae20-2fac90ebfd72</testSuiteGuid>
   <testCaseLink>
      <guid>00ad5294-cf47-47b9-97ed-e3a0bf14445d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Intraprecision Configuration/1. Intraprecision Configuration - Update</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6e5e0dd-3045-46fd-baaf-598e47ddcda3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Intraprecision Configuration/2. Intraprecision Configuration - Remove</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
