<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>24.7 Assay Configuration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d8db2701-1c47-40c1-891d-23272624a1f5</testSuiteGuid>
   <testCaseLink>
      <guid>99998e5d-3782-4322-939c-48e12131e09c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Assay Configuration/1. 24.7 Assay Configuration - Create</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4e794f4-b9d0-46ee-be6c-805c88fd93fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Assay Configuration/2. 24.7 Assay Configuration - Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a4ae3a8-40f0-424d-82f1-2d1a931f381c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Assay Configuration/3. 24.7 Assay Configuration - Update</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5e83c5a-be2f-4cf2-b7b2-a23fab92f2a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Configuration/Assay Configuration/4. 24.7 Assay Configuration - Delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
